﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AIGeneral : MonoBehaviour
{

    //-----------------------------------
    //Constants
    //-----------------------------------

    //Enums for AI Types
    public enum AITypes
    {
        Hotball,Survival,Machines
    };
    //-----------------------------------
    // Public Variables
    //-----------------------------------
    //Is AI ACTIVE?
    public bool isAIActive;
    //Current AI Type
    private AITypes currentAIType;

    public AITypes CurrentAIType
    {
        get
        {
            return currentAIType;
        }

        set
        {
            currentAIType = value;
        }
    }
    //-----------------------------------
    // Private Variables
    //-----------------------------------
    //Player Bowl Movement of the player
    protected PlayerBowlMovement playerBowlMovement;
    //PLayer Status
    protected PlayerStatus playerStatus;
    //Transform of the player
    protected Transform transform;
    //-----------------------------------
    // Methods
    //-----------------------------------

   //Method to initialize AI Values
   protected void initAI()
    {
        playerBowlMovement = GetComponent<PlayerBowlMovement>();
        playerStatus = GetComponentInChildren<PlayerStatus>();
        transform = GetComponent<Transform>();
    }

    //Abstract Method
    public abstract void setAIState(bool newState);  
   
}
