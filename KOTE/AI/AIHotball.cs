﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIHotball : AIGeneral
{
    //-----------------------------------
    //Constants
    //-----------------------------------

    //-----------------------------------
    // Public Variables
    //-----------------------------------
    //The hotball
    public Transform hotball;
    //The Team of the AI
    public bool isTeamA;

    //-----------------------------------
    // Private Variables
    //-----------------------------------

    //-----------------------------------
    // Methods
    //-----------------------------------
    void Start()
    {
        initAI();
        if (isAIActive)
        {   //Set AI Type
            CurrentAIType = AITypes.Hotball;
        }
    }

    /**
     * Checks the ball state and acts if it must
     */
    private void FixedUpdate()
    {
        if (isAIActive)
        {
            if (hotball.gameObject.GetComponent<SpriteRenderer>().enabled == true)
            {
                int direction = 1;
                if (isTeamA)
                    direction = -1;

                //Is the hotball is on the score position?
                if (hotball.position.x * direction >= 0)
                {
                    //It's not on the score position.. Push it!
                    float directionx = hotball.position.x - transform.position.x;
                    float directiony = hotball.position.y - transform.position.y;
                    float tempdirectionx = directionx;
                    float tempdirectiony = directiony;
                    //Normalize
                    if (directionx > 0)
                        directionx = 1;
                    else if (directionx < 0)
                        directionx = -1;
                    if (directiony > 0)
                        directiony = 1;
                    else if (directiony < 0)
                        directiony = -1;
                    //Is right on the x axis?
                    if (tempdirectionx * direction < 5 && tempdirectionx * direction >= 0)
                    {
                        directionx = direction;                       
                    }
                    else
                        playerBowlMovement.dashPlayer();
                    playerBowlMovement.movePlayer(directionx, directiony);
                }
                else
                {
                    //Try to block on y
                    float directionx = 0;
                    float directiony = hotball.position.y - transform.position.y;
                    //Normalize
                    if (directiony > 0)
                        directiony = 1;
                    else if (directiony < 0)
                        directiony = -1;
                    playerBowlMovement.movePlayer(directionx, directiony);
                }
            }
        }
    }
    /**
     * Initialize AI for hotball
     */
    public override void setAIState(bool newState)
    {
        isAIActive = newState;
        //Debug.Log("AI is "+newState);
    }

    /**
     * Set team 
     */
    public void setTeam0(bool team)
    {
        isTeamA = team;
    }
}
