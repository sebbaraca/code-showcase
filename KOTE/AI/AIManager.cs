﻿/**
* This Script takes care of the AI that the player will have
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIManager : MonoBehaviour
{
    //----------------------------------
    //Public Variables
    //----------------------------------

    // Current Player ID
    public GameManager.PlayerId playerId;

    // AI HotBall
    public AIHotball AIHotBallScript;

    // AI Survival IceBreakers
    public AISurvivalIceBreakers AISurvivalIceScript;

    // AI Survival Smarty Plant
    public AISurvivalSmarty AISurvivalSmartyScript;

    // AI King of the Hill
    public AIKoth AIKingOfTheHillScript;

    //----------------------------------
    // Methods
    //----------------------------------

    // Use this for initialization - Checks the current game mode and activates/deactivates the ai scripts
    public void setUpAI()
    {
        //Checks current mode
        GameMode currentMode = GameManager.instance.CurrentGameMode;
        bool currentAIPlayer = GameManager.instance.AIPlayers[(int)playerId];
        switch (currentMode.TypeOfGame)
        {
            case GameMode.TypeOfGameMode.HotBall:
                AIHotBallScript.enabled = true;
                setUPHotBallAI(currentAIPlayer);
                break;
            case GameMode.TypeOfGameMode.Survival:
                switch (currentMode.GameKingdom)
                {
                    case GameMode.Kingdom.IceBreakers:
                        AISurvivalIceScript.enabled = true;
                        setUpSurvival(currentMode.GameKingdom, currentAIPlayer);
                        break;
                    case GameMode.Kingdom.SmartyPlants:
                        AISurvivalSmartyScript.enabled = true;
                        setUpSurvival(currentMode.GameKingdom, currentAIPlayer);
                        break;

                }
                break;
            case GameMode.TypeOfGameMode.KingOfTheHill:
                //AIKingOfTheHillScript.enabled = true;
                setUPKOTHAI(currentAIPlayer);
                break;
        }
    }

    // Method that set up the ai state of the player according of his state in the GameManager
    public void setUPHotBallAI(bool valueP)
    {
        if(playerId != GameManager.PlayerId.Player1)
        {
            AIHotBallScript.enabled = valueP;
        }
    }

    // Method that set up the ai state of the player according of his state in the GameManager
    public void setUPKOTHAI(bool valueP)
    {
        if (playerId != GameManager.PlayerId.Player1)
        {
            //Debug.Log("PlayerID: " + playerId);
            AIKingOfTheHillScript.enabled = valueP;
        }
    }

    /* Method that set up the ai state of the player according of his state in the GameManager
     * @param pKingdom - Survival kingdom
     */
    public void setUpSurvival(GameMode.Kingdom pKingdom, bool valueP)
    {
        if (playerId != GameManager.PlayerId.Player1)
        {
            switch (pKingdom)
            {
                case GameMode.Kingdom.IceBreakers:
                    //AISurvivalIceScript.enabled = valueP;
                    AISurvivalIceScript.setAIState(valueP);
                    break;
                case GameMode.Kingdom.SmartyPlants:
                    //AISurvivalSmartyScript.enabled = valueP;
                    AISurvivalSmartyScript.setAIState(valueP);
                    break;
            }
        }
    }

}
