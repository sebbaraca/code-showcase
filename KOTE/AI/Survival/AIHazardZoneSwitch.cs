﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/**
*This class activates or deactivates the hazards when the switcher passes by
*/
public class AIHazardZoneSwitch : MonoBehaviour
{


    //------------------------------------
    // Public Variables
    //------------------------------------
    public bool debugOn;
    //The collider to be turned on/off
    public GameObject myCollider;
    //------------------------------------
    // Private Variables
    //------------------------------------
    
    //----------------------------------
    // Methods
    //----------------------------------
    // Use this for initialization
    void Start ()
    {
        //myCollider = GetComponentInChildren<Collider2D>();
        myCollider.SetActive( false);
	}

    /**
   *Turn On this hazard zone for the AI
   */
    void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.tag.Contains("Hazard"))
        {
            myCollider.SetActive(true);
            if (debugOn) Debug.Log("Trigger On");
        }
    }

    /**
    *Turn off this hazard zone for the AI
    */
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag.Contains("Hazard"))
        {
            myCollider.SetActive(false);
            if (debugOn) Debug.Log("Trigger Off");
        }

    }

}
