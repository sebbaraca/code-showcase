﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/**
*This class is for the AI of Ice Breaker's Survival
***********************************************************************************
* DEVELOPER LOG
*+ <Sebbaraca> <18/08/2017 > < Format Change>
*/
public class AISurvivalIceBreakers : AIGeneral
{
    //-----------------------------------
    //Constants
    //-----------------------------------
    //Types of AI
     public enum SurvivalAIGameType { IceBreakers,SmartyPlants,Hotchicks};
    //-----------------------------------
    // Public Variables
    //-----------------------------------
    public SurvivalAIGameType currentSurvivalType;
    //-----------------------------------
    // Private Variables
    //-----------------------------------
    //Has the egg moved yet?
    bool hasMoved;
    //The direction to move
    Vector3 moveDirection;
    //-----------------------------------
    // Methods
    //-----------------------------------
    private void Awake()
    {
        isAIActive = false;
        hasMoved = false;
    }
    void Start()
    {      
        initAI();
        if (isAIActive)
        {   //Set AI Type
            CurrentAIType = AITypes.Survival;
        }
        
    }
    /**
      *Checks every 3 secs if havent move
      */
      void checkIddle()
    {
                   
        switch (currentSurvivalType)
        {
            case SurvivalAIGameType.IceBreakers:
                moveDirection = Vector3.zero - transform.position;
                break;
            case SurvivalAIGameType.SmartyPlants:
                moveDirection = Vector3.zero - transform.position;
                break;
            case SurvivalAIGameType.Hotchicks:
                //moveDirection = Vector3.zero - transform.position;
                break;
            default:
                moveDirection = Vector3.zero - transform.position;
                break;
        }
    }
    /**
     * Checks the hazzard places and acts if must
     */
    private void FixedUpdate()
    {
        if (isAIActive)
        {
            //Normalize
            if (moveDirection.x > 0)
                moveDirection.x = 1;
            else if (moveDirection.x < 0)
                moveDirection.x = -1;
            if (moveDirection.y > 0)
                moveDirection.y = 1;
            else if (moveDirection.y < 0)
                moveDirection.y = -1;
            playerBowlMovement.movePlayer(moveDirection.x, moveDirection.y);
            
            
        }
    }


    /**
    *The Egg is in danger!
    */
    void OnTriggerEnter2D(Collider2D other)
    {
        if(isAIActive)
        {
            
            if (other.gameObject.tag.Contains("Hazard"))
            {
                moveDirection = (transform.position - other.transform.position);
                if (Random.Range(0, 300) > 250||currentSurvivalType!=SurvivalAIGameType.IceBreakers)
                    playerBowlMovement.dashPlayer();
            }
            else if (other.gameObject.tag.Contains("Snow"))
            {
                moveDirection = (transform.position - other.transform.position);
                playerBowlMovement.dashPlayer();
            }
            else if (other.gameObject.tag.Contains("RunAway"))
            {
                moveDirection = (transform.position - other.transform.position);
                //playerBowlMovement.dashPlayer();
            }
            else if (other.gameObject.tag.Contains("Item")|| other.gameObject.tag.Contains("Player"))
            {
                //Debug.Log(this.gameObject.tag+" - "+other.gameObject.tag);
                moveDirection = ( other.transform.position- transform.position );
                playerBowlMovement.dashPlayer();
            }
        }
    }
  
    /**
    *OUT OF DANGER!
    */
    void OnTriggerExit2D(Collider2D other)
    {
        if(isAIActive)
        {
            if (other.gameObject.tag.Contains("Hazard"))
            {
                moveDirection = Vector3.zero;
            }
        }
    }

    /**
     * On collision enter
     */
    private void OnCollisionEnter2D(Collision2D other)
    {
        if(isAIActive)
        {
            if (other.gameObject.tag.Contains("Player"))
            {
                moveDirection = (other.transform.position - transform.position);
                playerBowlMovement.dashPlayer();
            }
        }
    }
    /**
     * Initialize AI for survival
     */
    public override void setAIState(bool newState)
    {
        isAIActive = newState;
        if (isAIActive)
            InvokeRepeating("checkIddle", 0f, 3f);
        //Debug.Log("AI is " + newState);
    }


   

}
