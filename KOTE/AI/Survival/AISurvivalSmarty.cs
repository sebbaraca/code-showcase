﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISurvivalSmarty : AIGeneral
{

    //-----------------------------------
    //Constants
    //-----------------------------------
    //Enums for the game states
    public enum GameStates
    {
       Laser,Iddle,FruitSpawn,FruitActive
    };
    //-----------------------------------
    // Public Variables
    //-----------------------------------


    //-----------------------------------
    // Private Variables
    //-----------------------------------
    //Indicator of the current state of the game
    GameStates currentState;
    //The objective to follow
    Transform objectiveTransform;
    //-----------------------------------
    // Methods
    //-----------------------------------
    void Start()
    {
        objectiveTransform = this.transform;
        currentState = GameStates.Iddle;
        initAI();
        if (isAIActive)
        {   //Set AI Type
            CurrentAIType = AITypes.Survival;
        }
    }

    /**
     * Checks the ball state and acts if it must
     */
    private void FixedUpdate()
    {
        if (isAIActive)
        {
            switch (currentState)
            {
                case GameStates.Laser:
                    break;
                case GameStates.Iddle:
                    break;
                case GameStates.FruitSpawn:
                    break;
                case GameStates.FruitActive:
                    break;
                default:
                    break;
            }
        }
    }
    /**
     * Initialize AI for survival
     */
    public override void setAIState(bool newState)
    {
        isAIActive = newState;
        //Debug.Log("AI is " + newState);
    }

    /**
     * Changes the current game state to one to chase the fruit
     */
     public void activeFruit(Transform fruitTransform)
    {
        currentState = GameStates.FruitActive;
        objectiveTransform = fruitTransform;
    }
}
