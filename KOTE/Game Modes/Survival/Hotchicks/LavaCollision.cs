﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/**
*This Class moves the collision point of the lava on SVHC
***********************************************************************************
* DEVELOPER LOG
*+ <Sebbaraca> <23/11/2017 > < Script created>
*/
public class LavaCollision : MonoBehaviour
{
    //----------------------------------
    //Public Variables
    //----------------------------------
   
    //Directionof the raycast;
    public Transform raycastDirection;
   
    //Sprite of the collision
    public Transform collisionSprite;
    //Distance of ray
    public float rayDistance;
    //----------------------------------
    //Private Variables
    //----------------------------------
    //Tag for collision
    private string wallTag = "MyWall";
    //Raycast variables
    RaycastHit2D hit;
    //Point of start of the raycast
    Transform raycastOrigin;
    //Layer
    int mylayerMask;
    //----------------------------------
    //Methods
    //----------------------------------
    private void Start()
    {
        mylayerMask= LayerMask.NameToLayer("Wall"); 
        raycastOrigin = this.transform;
        //Debug.Log(mylayerMask);
    }
    // Use this for initialization
    void FixedUpdate ()
    {        
        setLavaCollision();
	}
	
	/**
     * Detects the collision point and puts the sprite in place
     */ 
     void setLavaCollision()
    {
        hit = Physics2D.Raycast(raycastOrigin.position, (raycastDirection.position - raycastOrigin.position),rayDistance,1<<mylayerMask);
        if (hit&&hit.collider.tag==wallTag)
        {
            //Debug.Log("Hi there");
            collisionSprite.transform.position = hit.point;
        }
        Debug.DrawRay(raycastOrigin.position, (raycastDirection.position - raycastOrigin.position),Color.cyan);
    }
}
