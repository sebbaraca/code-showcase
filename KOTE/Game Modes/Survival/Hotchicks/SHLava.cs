﻿using UnityEngine;
using System.Collections;

public class SHLava : SurvivalHazard {

    //------------------------------------
    // Constants
    //------------------------------------


    //------------------------------------
    // Variables
    //------------------------------------
    //Hot & Cold duration
    public float hotDuration;
    public float coldDuration;
    public float itemTime;
    public int maxItems;

    //Spawn areas
    public Transform downSpawn;
    public Transform upSpawn;
    //Game objects of hot and cold
    public GameObject hotObject;
    public GameObject coldObject;
    //SurvivalPlayer of each player
    public SurvivalPlayer[] survivalPlayers;
	//------------------------------------
	// Methods
	//------------------------------------
	// Use this for initialization
	void Start () 
	{
        InvokeRepeating("spawnRandomItem", 6f, itemTime);
	}	

	//Restarts the hazards of the world
	public override void restartSurvivalHazards()
	{	
		
        foreach (SurvivalPlayer survivalPlayer in survivalPlayers)
        {

            survivalPlayer.restartBoilPoints();
        }
    }
    	
    //Spawns a random item in a random quantity
    void spawnRandomItem()
    {
        int quantity = Random.Range(1, maxItems);
        for (int i = 0; i < quantity; i++)
        {
            int isHot = Random.Range(0, 2);
            if (isHot == 0)
            {
                spawnObject(hotObject, 1, hotDuration);
            }
            else
            {
                spawnObject(coldObject, 1, coldDuration);
            }
        }
        
    }
    //Spawns a number of objects
    void spawnObject(GameObject item, int objectNumber, float duration)
    {
        Vector3 tempVector = new Vector3();

        for (int i = 0; i < objectNumber; i++)
        {
            //Gets a random position for the snowball
            tempVector.x = Random.Range(upSpawn.position.x, downSpawn.position.x);
            tempVector.y = Random.Range(upSpawn.position.y, downSpawn.position.y);
            //Instantiate and destroy the snowball
            GameObject.Destroy(GameObject.Instantiate(item, tempVector, this.transform.rotation), duration);
        }
    }

}
