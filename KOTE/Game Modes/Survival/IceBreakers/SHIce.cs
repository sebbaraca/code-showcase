﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/**
*This class is the Surival hazard for ICE BREAKERS
***********************************************************************************
* DEVELOPER LOG
*+ <Sebbaraca> <18/08/2017 > < Format Change>
*+ <Sebbaraca> <27/08/2017 > < Fixes de patrones & spawn de snowballs>
*+ <Sebbaraca> <02/09/2017 > < Fix spawn fantasma>
*+ <Sebbaraca> <07/09/2017 > < Fix Definitivo spawn fantasma>
*/
public class SHIce : SurvivalHazard
{

	//------------------------------------
	// Constants
	//------------------------------------
	//Constants for animation

	int icyOnTrigger;

	//------------------------------------
	//Public  Variables
	//------------------------------------
	//The spawn locations for the arrows
	public Transform[] arrowsSpawn;
	
	//Time for the next  state
	public float IceWaitFactor;
	
	//Transform constraints for the snowball
	public Transform upSnowBall;
	public Transform downSnowball;
	//Snowball prefab
	public GameObject snowball;
	//Duration of each snowball
	public float snowDuration;
	//Number of snowballs
	public int snowNumber;
	//Snow waiting time
	public float snowWait;
	//Time between shots
	public float shotTime;
    //The arrow to be spawned
    public GameObject arrowPrefab;
    //The time before an arrow gets destroyed
    public float arrowDestructionTime;
    //Max number of arrows per strike
    public int maxArrowsPerStrike;
    //Number of seconds to check if arrows are being spawned
    public float timeCheckArrows;
    
    //------------------------------------
    // Private Variables
    //------------------------------------
    //Bool to check if can spawn
    bool canSpawnArrows;
    //The last id used for spawnArrows
    int spawnId;
    //Arraylist of IDs of each arrow round
    ArrayList spawnIDs;
    //The list of spawned arrows
    ArrayList spawnedArrows;
    //The list of spawned snowballs
    ArrayList spawnedSnowballs;
    //Current round
    int currentRound;
    //current active size
	int active_ice_size;
    //Active icicles
	int [][] active_ice;
    //AUdio Source
	AudioSource audioSource;
    //Arrows per loop
    int arrowsPerLoop;
    //------------------------------------
    // Methods
    //------------------------------------
    // Use this for initialization
    void Start () 
	{
        canSpawnArrows = false;
        spawnId = 0;
        spawnIDs = new ArrayList();
        spawnedArrows = new ArrayList();
        spawnedSnowballs = new ArrayList();
        audioSource = GetComponent<AudioSource>();
		active_ice = new int[3][];
        for (int i = 0; i < active_ice.Length; i++)
        {
            active_ice[i] = new int[maxArrowsPerStrike];
        }
        currentRound = 0;
		//Create the array of animations
		icyOnTrigger = Animator.StringToHash("TurnOn");

		//restartSurvivalHazards();
		active_ice_size = 1;
		InvokeRepeating("spawnSnowball",10f,snowWait);
        //InvokeRepeating("checkArrowsStatus", 6f, timeCheckArrows);
        
	}

    /**
    *Check every t seconds if there are arrows spawned
    */
    void checkArrowsStatus()
    {
        Debug.Log("Check status " + spawnIDs.Count + " can spawn "+canSpawnArrows);
        //Are any arrows to spawn?
        if (spawnIDs.Count<=0&& canSpawnArrows)
        {
            Debug.Log("Restarting Mecanism");
            restartSurvivalHazards();
        }
        else if(spawnIDs.Count > 1&& canSpawnArrows)
        {
            Debug.Log("Restarting Mecanism");
            //restartSurvivalHazards();
            spawnIDs.Clear();
        }
    }

	/**
    *Removes al spawned arrows and snowballs and resets the arrays 
    */
	void resetSpawnedObjects()
	{
        foreach (GameObject item in spawnedArrows)
        {
            try
            {
                GameObject.Destroy(item);
            }
            catch (System.Exception e) { }
        }
                
        foreach (GameObject item in spawnedSnowballs)
        {
            try
            {
                GameObject.Destroy(item);
            }
            catch (System.Exception e) { }
        }
        spawnIDs.Clear();
        spawnedArrows.Clear();
        spawnedArrows.Clear();
    }

	/**
    *Restarts the hazards of the world
    */
	public override void restartSurvivalHazards()
	{
        canSpawnArrows = true;
        StopCoroutine("spawnArrowsInTSeconds");
        active_ice_size = 1;
		currentRound++;
        resetSpawnedObjects();
		manageArrowPattern (0,currentRound);
	}

	/**
    *Spawns arrows after t seconds
	*@param time the time for the new round
    *@param round the round of that shooting arrow
    @param numberOfParallelArrows the number of arrows to be shot in parallel
    *@param roundSpawnId the spawn id asigned to this round
    */
	IEnumerator spawnArrowsInTSeconds(float time,  int round,int roundSpawnId)
	{

		//Waits and then starts the behaviour
		yield return new WaitForSeconds (time);
		if (Time.timeScale == 1f&&currentRound==round)
		{
            
            //resetSpawnedArrows();
            for (int i = 0; i < active_ice_size&& spawnIDs.Contains(roundSpawnId); i++)
            {
                yield return new WaitForSeconds(shotTime);
                //src.Play();
                for (int j = 0; j < arrowsPerLoop && currentRound == round&& spawnIDs.Contains(roundSpawnId); j++)
                {
                    //Debug.Log("Arrows per loop " + arrowsPerLoop + " ***** i - j " + i + " - " + j + " --- " + active_ice[i][j] + " --- Round - current round  "+round+" - "+currentRound);
                    if (spawnIDs.Contains(roundSpawnId))
                    {
                        GameObject spawnedArrow = GameObject.Instantiate(arrowPrefab, arrowsSpawn[active_ice[i][j]]);
                        spawnedArrows.Add(spawnedArrow);
                        GameObject.Destroy(spawnedArrow, arrowDestructionTime);
                    }
                }

            }
            //Clear the Spawn Ids
            //spawnIDs.Clear();
            spawnIDs.Remove(roundSpawnId);
            //Debug.Log("---------------");
            //Increase level of difficulty
			if(active_ice_size<active_ice.Length)
				active_ice_size ++;
			manageArrowPattern (IceWaitFactor,round);
			
		}
	}

	/**
    *Manager for the arrow patterns
	*Sets the array of arrows to be spawned the next round
    *@param waitTime the time to wait for the next round of arrows
    *@param shotRound the Round of the round
    */
	void manageArrowPattern(float waitTime,int shotRound)
	{
        if (Time.timeScale < 1f||currentRound!=shotRound    )
        {
            return;
        }
        
        else
        {
            arrowsPerLoop = maxArrowsPerStrike;

            int j = 0;
            while (j < arrowsPerLoop)         
            {
               
                    int pattern = Random.Range(0, 4);

                    switch (pattern)
                    {
                        case 0: //Right
                            for (int i = 0; i < active_ice_size; i++)
                            {                                
                                if (i % 2 == 0)
                                {
                                    active_ice[i][j] = Random.Range(arrowsSpawn.Length / 4, arrowsSpawn.Length / 2);
                                }
                                else
                                {
                                    active_ice[i][j] = Random.Range(arrowsSpawn.Length / 2 + 1 + arrowsSpawn.Length / 4, arrowsSpawn.Length);
                                }                                
                            }
                            break;
                        case 1: //Left
                            for (int i = 0; i < active_ice_size; i++)
                            {
                                if (i % 2 == 0)
                                {
                                    active_ice[i][j] = Random.Range(0, arrowsSpawn.Length / 4);
                                }
                                else
                                {
                                    active_ice[i][j] = Random.Range(arrowsSpawn.Length / 2 + 1, arrowsSpawn.Length / 2 + arrowsSpawn.Length / 4);
                                }                               
                            }
                            break;
                        case 2: //Center & sides
                            for (int i = 0; i < active_ice_size; i++)
                            {
                                if (i % 6 == 0)//Left
                                {
                                    active_ice[i][j] = Random.Range(0, arrowsSpawn.Length / 4);
                                }
                                else if (i % 6 == 1)
                                {
                                    active_ice[i][j] = Random.Range(arrowsSpawn.Length / 2 + 1, arrowsSpawn.Length / 2 + arrowsSpawn.Length / 4);
                                }
                                else if (i % 6 == 2) //Right
                                {
                                    active_ice[i][j] = Random.Range(arrowsSpawn.Length / 4, arrowsSpawn.Length / 2);
                                }
                                else if (i % 6 == 3)
                                {
                                    active_ice[i][j] = Random.Range(arrowsSpawn.Length / 2 + 1 + arrowsSpawn.Length / 4, arrowsSpawn.Length);
                                }
                                else if (i % 6 == 4) //Center
                                {
                                    active_ice[i][j] = Random.Range(arrowsSpawn.Length / 6, arrowsSpawn.Length / 3);
                                }
                                else
                                {
                                    active_ice[i][j] = Random.Range(arrowsSpawn.Length / 2 + 1 + arrowsSpawn.Length / 6, arrowsSpawn.Length / 2 + arrowsSpawn.Length / 3);
                                }                                
                            }
                            break;
                        case 3: //Sides
                            for (int i = 0; i < active_ice_size; i++)
                            {
                                if (i % 4 == 0)//Left
                                {
                                    active_ice[i][j] = Random.Range(0, arrowsSpawn.Length / 5);
                                }
                                else if (i % 4 == 2)
                                {
                                    active_ice[i][j] = Random.Range(arrowsSpawn.Length / 2 + 1, arrowsSpawn.Length / 2 + arrowsSpawn.Length / 4);
                                }
                                else if (i % 4 == 3) //Right
                                {
                                    active_ice[i][j] = Random.Range(arrowsSpawn.Length / 5, arrowsSpawn.Length / 2);
                                }
                                else if (i % 4 == 1)
                                {
                                    active_ice[i][j] = Random.Range(arrowsSpawn.Length / 2 + 1 + arrowsSpawn.Length / 5, arrowsSpawn.Length);
                                }                                
                            }
                            break;
                        default:
                            break;
                     }

               
                j++;
        }
            spawnIDs.Add(spawnId);
            StartCoroutine( spawnArrowsInTSeconds(waitTime,shotRound,spawnId));            
            spawnId++;
            
		}
	}

	/**
    *Spawns a number of snowballs
    */
	void spawnSnowball()
	{
		Vector3 tempVector = new Vector3();

		for(int i =0;i<snowNumber;i++)
		{
			//Gets a random position for the snowball
			tempVector.x = Random.Range(upSnowBall.position.x,downSnowball.position.x);
			tempVector.y = Random.Range(upSnowBall.position.y,downSnowball.position.y);
            //Instantiate and destroy the snowball
            GameObject thisSnowball = GameObject.Instantiate(snowball, tempVector, this.transform.rotation);
            spawnedSnowballs.Add(thisSnowball);
            try
            {
                GameObject.Destroy(thisSnowball, snowDuration);
            }
            catch (System.Exception e) { }
            
		}
	}
}
