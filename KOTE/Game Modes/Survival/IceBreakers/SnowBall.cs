﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
* Class that controlls the sounds made by the snowball
***********************************************************************************
* DEVELOPER LOG
*+ Raul 10-08-2017 Se implemento los estandares de documentacion
*/


public class SnowBall : MonoBehaviour {

    //---------------------------
    // Private Variables
    //---------------------------

    //Reference to the audio source
    private AudioSource src;

    //-----------------------------
    //Methods
    //-----------------------------

    // Use this for initialization
    void Start ()
    {
        src = GetComponent<AudioSource>();
    }


    public void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("me choque con: " + collision.gameObject.tag);
        if(collision.gameObject.tag.Contains("Player"))
        {
            playFrezzingPlayerSound();
        }
    }

    // Method that plays the sound of the player being forzen
    // Ice Eviroment Effects [2] : Frezzing
    private void playFrezzingPlayerSound()
    {
        src.clip = EffectsManager.instance.iceEnviromentEffects[2];
        src.Play();
    }

    // Method that plays the sound of the snow ball smashing the ground
    // Ice Eviroment Effects [1] : Snow Drop
    public void playEffect(int index)
    {
        src.clip = EffectsManager.instance.iceEnviromentEffects[1];
        src.Play();
    }

}
