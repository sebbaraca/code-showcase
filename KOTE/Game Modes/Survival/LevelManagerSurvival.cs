﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System;
/**
*This class is the level manager for the survival game modes
***********************************************************************************
* DEVELOPER LOG
*+ <Sebbaraca> <18/08/2017 > < Format Change>
*/

public class LevelManagerSurvival : LevelManagerGeneral
{

    //----------------------------------
    // Constants
    //----------------------------------

    //-----------------------------------
    // Delagates
    //-----------------------------------

    //Delegates for the score
    public delegate void touchedAction(GameManager.PlayerId player, int score);
    public static event touchedAction OnTouched;

    //------------------------------------
    // Public Variables
    //------------------------------------

    //Variables used to know how many rounds are going to played
    public int roundsToBePlayed;

    //Time for the next round
    public float newGameWaitTime;
    //Time to end game
    public float endGameTime = 4f;
    //Round Winner Canvas
    public GameObject roundWinnerCanvas;
    //Round Winner spray on the winner canvas
    public Image roundWinnerImage;

    //UI element for winner/survivor text
    //public Text survivor_txt;

    // Winner Canvas
    public GameObject gameWinnerCanvas;

    // Game Winner Image
    public Image gameWinnerImage;


    //Player images of the Score Canvas
    public Image p1_Image;
    public Image p2_Image;
    public Image p3_Image;
    public Image p4_Image;


    //Player status
    public PlayerStatus p1_status;
    public PlayerStatus p2_status;
    public PlayerStatus p3_status;
    public PlayerStatus p4_status;

    //Can the players win points?
    public bool canWinPoints;

    //The Survival Hazards of the world
    public SurvivalHazard survivalHazard;


    //Showcase at the start of the level
    //SmartyPlants Showcase
    public SpriteRenderer faceGodRenderer;
    public SpriteRenderer fakeFaceGod;
    public SpriteRenderer glow;
    public Sprite[] faceGodSprites;
    public Sprite[] glowSprites;
    //HotChicks showcase
    public GameObject laser;
    public GameObject laserBase;
    public SpriteRenderer laserTell;
    public GameObject impact;
    public Transform impactPosition1;
    public Transform impactPosition2;
	
	//Score
	public Score score;
	
	//sprite for draw
	public Sprite drawSprite;
    //changed to theme B?
    public bool changedToB;
    //------------------------------------
    // Private  Variables
    //------------------------------------
    //Number of players alive
	int alive;
    //Array of player status i.e. Alive- Dead
	// false means Dead
	//true Means Alive
	bool[] isPlayerAlive;
    //----------------------------------------
    // Methods
    //----------------------------------------

    // Use this for initialization
    void Start ()
	{
        changedToB = false;
        //Initial state of variables
		Time.timeScale = 1;
        //survivor_txt.text = "Survivor";
		alive = 4;
		canWinPoints = true;
		//Initialize eggs
		initializeEggs();
		//Setup

		isPlayerAlive = new bool[4];

		for(int i =0;i<4;i++)
		{
			//Set all the players to alive status
			isPlayerAlive [i] = true;
		}
        // Deactivate round Canvas
        roundWinnerCanvas.SetActive(false);
        //Set the images of the canvas
        p1_Image.sprite = characterManager.getCharacterSpriteUni((int)GameManager.instance.PlayersData[0].CharacterId);
        p2_Image.sprite = characterManager.getCharacterSpriteUni((int)GameManager.instance.PlayersData[1].CharacterId);
        p3_Image.sprite = characterManager.getCharacterSpriteUni((int)GameManager.instance.PlayersData[2].CharacterId);
        p4_Image.sprite = characterManager.getCharacterSpriteUni((int)GameManager.instance.PlayersData[3].CharacterId);

        startCountDown();

        // Checks if the game is a last round. If it is only enable the finalists. If not continue as normally
        if(GameSession.instance.currentSessionType == GameSession.GameSessionType.LAST_ROUND)
        {
            leaveFinalists(GameSession.instance.tournamentFinalists);
            alive = GameSession.instance.tournamentFinalists.Length;
            roundsToBePlayed = 1;
        }
        else
        {
            roundsToBePlayed = 3;
        }

        StartCoroutine(showcaseMode());
        GameSession.instance.loadingScreenScript = loadScreenScript;
        
    }

    // Awake
    void Awake () 
	{
        GameMode currentGameMode = new GameMode(GameMode.TypeOfGameMode.Survival);
        currentGameMode.GameKingdom = kingdom;
        GameManager.instance.CurrentGameMode = currentGameMode;
    }

    //Method that showcases the mode
    private IEnumerator showcaseMode()
    {
        GameMode currentMode = GameManager.instance.CurrentGameMode;
        if (currentMode.GameKingdom.Equals(GameMode.Kingdom.SmartyPlants))
        {
            faceGodRenderer.enabled = false;
            Sprite tempFaceGodSprite = fakeFaceGod.sprite;
            yield return new WaitForSeconds(1f);
            int pos = 0;
            while (pos < faceGodSprites.Length)
            { 
                fakeFaceGod.sprite = faceGodSprites[pos];
                glow.sprite = glowSprites[pos];
                pos++;
                yield return new WaitForSeconds(1f);
            }
            fakeFaceGod.sprite = null;
            faceGodRenderer.enabled = true;
            glow.sprite = null;
        }
        if (currentMode.GameKingdom.Equals(GameMode.Kingdom.HotChicks))
        {
            int rotationPos = 0;
            while (rotationPos < 4)
            {
                yield return new WaitForSeconds(0.1f);
                float pos = 0;
                while (pos < 1)
                {
                    yield return new WaitForSeconds(0.1f);
                    pos += 0.3f;
                    laserTell.color = new Color(1, 1, 1, pos);
                }
                if (rotationPos == 0 || rotationPos == 2)
                {
                    impact.transform.position = impactPosition1.position;
                }
                else
                {
                    impact.transform.position = impactPosition2.position;
                }
                laserTell.color = new Color(1, 1, 1, 0);
                laser.SetActive(true);
                yield return new WaitForSeconds(0.45f);
                laser.SetActive(false);

                rotationPos++;
                laserBase.transform.eulerAngles = new Vector3(0, 0, 90*rotationPos);
            }
            laserBase.SetActive(false);
            laser.SetActive(false);
            laserTell.enabled = false;
            impact.SetActive(false);
         
        }
    }

    /**
	*This method starts a new round of survival, reseting values and if must it finishes the game
	*/

    void startNewRound()
	{
        
		//survivor_txt.text = "Survivor";
		Time.timeScale = 1;
		//Restarts the hazards
		survivalHazard.restartSurvivalHazards();
        bool allRoundsPlayed = false;
        
        if(GameSession.instance.currentSessionType == GameSession.GameSessionType.LAST_ROUND)
        {
            PlayerData[] finalistsPlayers = GameSession.instance.tournamentFinalists;
            for(int i = 0; i < finalistsPlayers.Length && !allRoundsPlayed; i++)
            {
                int tempID = (int) finalistsPlayers[i].PlayerId;
                int tempScore = score.currentScores[tempID] / 30;
                if(tempScore >= (roundsToBePlayed *2 /3))
                {
                    MusicManager.instance.changeTrackToThemeB();
                    changedToB = true;
                }
                if(tempScore >= roundsToBePlayed)
                {
                    allRoundsPlayed = true;
                }
            }
        }
        else
        {
            for (int i = 0; i < score.currentScores.Length && !allRoundsPlayed; i++)
            {
                int tempScore = score.currentScores[i] / 30;
                if (tempScore >= (roundsToBePlayed * 2 / 3))
                { MusicManager.instance.changeTrackToThemeB(); changedToB = true; }
                if (tempScore >= roundsToBePlayed)
                {
                    allRoundsPlayed = true;
                }
            }
        }

		if(allRoundsPlayed == true)
		{
            playEndGameTrumpets();
			//If all rounds are played then finish the game
			Time.timeScale = 0;
			//Shows the winner
			GameManager.PlayerId thewinnerID = score.giveWinner();
			if(thewinnerID == GameManager.PlayerId.Default)
			{
				roundWinnerImage.sprite = drawSprite;
				//survivor_txt.text = "DRAW";
			}
			else
			{
                roundWinnerCanvas.SetActive(false);

                gameWinnerCanvas.SetActive(true);
                gameWinnerImage.sprite = characterManager.getCharacterSpriteUni(((int)GameManager.instance.PlayersData[(int)thewinnerID].CharacterId));
                
                if (GameSession.instance.currentSessionType == GameSession.GameSessionType.TOURNAMENT || GameSession.instance.currentSessionType == GameSession.GameSessionType.LAST_ROUND)
                {
                    GameSession.instance.updatePlayerScore((int) thewinnerID);
                }                
            }
			StartCoroutine (endGame());

			return;
		}

        // Deactivate Round Canvas
        roundWinnerCanvas.SetActive(false);

        if(GameSession.instance.currentSessionType == GameSession.GameSessionType.LAST_ROUND)
        {
            PlayerData[] finalists = GameSession.instance.tournamentFinalists;
            foreach(PlayerData temp in finalists)
            {
                switch(temp.PlayerId)
                {
                    case GameManager.PlayerId.Player1:
                        p1_status.restartPlayer();
                        isPlayerAlive[0] = true;
                        break;
                    case GameManager.PlayerId.Player2:
                        p2_status.restartPlayer();
                        isPlayerAlive[1] = true;
                        break;
                    case GameManager.PlayerId.Player3:
                        p3_status.restartPlayer();
                        isPlayerAlive[2] = true;
                        break;
                    case GameManager.PlayerId.Player4:
                        p4_status.restartPlayer();
                        isPlayerAlive[3] = true;
                        break;
                }
            }
            alive = finalists.Length;
        }
        else
        {
            //Restarts all players
            p1_status.restartPlayer();
            p2_status.restartPlayer();
            p3_status.restartPlayer();
            p4_status.restartPlayer();
            for (int i = 0; i < 4; i++)
            {
                //Set all the players to alive status
                isPlayerAlive[i] = true;

            }

            alive = 4;
        }
		canWinPoints = true;
	}

	/**
    *This method ends the game
    */
	IEnumerator endGame()
	{
		yield return new WaitForSecondsRealtime (endGameTime);
        
        score.Unsuscribe();
        Time.timeScale = 1;
        StopAllCoroutines();
        GameSession.instance.LoadNextLevel();
    }

    /**
    *This Method Kills the player with certain ID
	*@param playerId id of the Player to be Killed
    */
    public void killPlayer(int playerId)
	{
		if(canWinPoints&& isPlayerAlive[playerId])
        {
			//Set the is alive status to false
			isPlayerAlive [playerId] = false;
			alive --;
			//Check if must start new Game
			checkGame ();
			//Debug.Log(playerId + "  Died");

		}

	}

	/**
    *Check if must start a new Game and asign points
    */
	void checkGame()
	{
		//Start new round if must
		if(alive ==1&&canWinPoints)
		{
            //Debug.Log("-------");
			GameManager.PlayerId aliveid = GameManager.PlayerId.Default;		
			//Asign points to players that keep staying alive
            if(GameSession.instance.currentSessionType == GameSession.GameSessionType.LAST_ROUND)
            {
                PlayerData[] finalists = GameSession.instance.tournamentFinalists;
                foreach(PlayerData temp in finalists)
                {
                    int playerID = (int)temp.PlayerId;
                    if(isPlayerAlive[playerID])
                    {
                        OnTouched(temp.PlayerId, 30);
                        aliveid = temp.PlayerId;
                    }
                }
            }
            else
            {
                if (isPlayerAlive[0])
                {
                    OnTouched(GameManager.PlayerId.Player1, 30);
                    aliveid = GameManager.PlayerId.Player1;
                }
                if (isPlayerAlive[1])
                {
                    OnTouched(GameManager.PlayerId.Player2, 30);
                    aliveid = GameManager.PlayerId.Player2;
                }
                if (isPlayerAlive[2])
                {
                    OnTouched(GameManager.PlayerId.Player3, 30);
                    aliveid = GameManager.PlayerId.Player3;
                }
                if (isPlayerAlive[3])
                {
                    OnTouched(GameManager.PlayerId.Player4, 30);
                    aliveid = GameManager.PlayerId.Player4;
                }
            }


			canWinPoints = false;
            // Deactivate Score Canvas
            roundWinnerCanvas.SetActive(true);
            //Debug.Log ("New Game");
            roundWinnerImage.sprite = characterManager.getCharacterSpriteUni((int)GameManager.instance.PlayersData[(int)aliveid].CharacterId);
			//roundWinnerImage.sprite = characterManager.getInGameCharacterSprite((int)GameManager.instance.PlayersData[(int)aliveid].CharacterId,aliveid);
			Time.timeScale = 0.4f;
			StartCoroutine(startnewGameInTSeconds(newGameWaitTime));
		}
	}
    /**
    * Starts a new game in a given number of seconds
    *@param timeForNewRound number of seconds for the new round 
    */
    IEnumerator startnewGameInTSeconds(float timeForNewRound)
	{
		
		//Waits and then starts the behaviour	
		yield return new WaitForSecondsRealtime(timeForNewRound/2);
		Time.timeScale = 0;
		yield return new WaitForSecondsRealtime(timeForNewRound);

		startNewRound ();


	}
    /**
    *Initializes the survival mode
    */
    public override void initializeMode()
    {
        //Resets all the hazards of the game
		survivalHazard.restartSurvivalHazards();
    }

    /// <summary>
    /// Method that leave only the finalists play
    /// </summary>
    /// <param name="finalistsPlayers">Array of finalists players</param>
    public void leaveFinalists(PlayerData[] finalistsPlayers)
    {
        for(int i = 0; i < GameManager.instance.PlayersData.Length; i++)
        {
            PlayerData temp = GameManager.instance.PlayersData[i];
            bool isFinalist = isAFinalist(finalistsPlayers, temp);
            if(isFinalist == false)
            {
                eggs[i].SetActive(false);
            }
        }
    }

    /// <summary>
    /// Method that checks if the player given by parameter is a finalist
    /// </summary>
    /// <param name="finalistsPlayers"></param>
    /// <param name="playerID"></param>
    /// <returns></returns>
    public bool isAFinalist(PlayerData[] finalistsPlayers, PlayerData playerID)
    {
        bool answer = false;
        for(int i = 0; i < finalistsPlayers.Length && answer == false; i++)
        {
            PlayerData playerTemp = finalistsPlayers[i];
            if(playerTemp == playerID)
            {
                answer = true;
            }
        }
        return answer;
    }
}
