﻿using UnityEngine;
using System.Collections;

public class BonusZone : MonoBehaviour {

	//----------------------------------
	// Constants
	//----------------------------------
	//Id of the BonusZone
	public int bonus_id;
	//------------------------------------
	// Variables
	//------------------------------------
	//FaceGod
	public SHLaserFace theFaceGod;

	//----------------------------------
	// Methods
	//----------------------------------

	// Use this for initialization
	void Start ()
	{
	
	}

	//If a player enters the zone notify the FaceGod
	void OnTriggerEnter2D(Collider2D col)
	{
		string objectTag = col.tag;

		if(objectTag=="Player_1")
		{
			theFaceGod.enterBonusZone(0,bonus_id);	
		}
		else if(objectTag=="Player_2")
		{
			theFaceGod.enterBonusZone(1,bonus_id);	
		}
		else if(objectTag=="Player_3")
		{
			theFaceGod.enterBonusZone(2,bonus_id);	
		}
		else if(objectTag=="Player_4")
		{
			theFaceGod.enterBonusZone(3,bonus_id);	
		}
	}

	//If a player enters the zone notify the FaceGod
	void OnTriggerStay2D(Collider2D col)
	{
		string objectTag = col.tag;

		if(objectTag=="Player_1")
		{
			theFaceGod.enterBonusZone(0,bonus_id);	
		}
		else if(objectTag=="Player_2")
		{
			theFaceGod.enterBonusZone(1,bonus_id);	
		}
		else if(objectTag=="Player_3")
		{
			theFaceGod.enterBonusZone(2,bonus_id);	
		}
		else if(objectTag=="Player_4")
		{
			theFaceGod.enterBonusZone(3,bonus_id);	
		}
	}

	//If a player Exits the zone notify the FaceGod
	void OnTriggerExit2D(Collider2D col)
	{
		string objectTag = col.tag;

		if(objectTag=="Player_1")
		{
			theFaceGod.exitBonusZone(0,bonus_id);	
		}
		else if(objectTag=="Player_2")
		{
			theFaceGod.exitBonusZone(1,bonus_id);	
		}
		else if(objectTag=="Player_3")
		{
			theFaceGod.exitBonusZone(2,bonus_id);	
		}
		else if(objectTag=="Player_4")
		{
			theFaceGod.exitBonusZone(3,bonus_id);	
		}
	}

}
