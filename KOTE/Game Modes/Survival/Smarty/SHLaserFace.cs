﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
/**
*This class is the Surival hazard for Smarty Plants
***********************************************************************************
* DEVELOPER LOG
*+ <Sebbaraca> <01/09/2017 > < Format Change>
*+ <Sebbaraca> <23/09/2017 > < Hazard Zone adition>
*+ <Sebbaraca> <05/10/2017 > < Fixes for pinnaple spawn and zone percentage>
*/
public class SHLaserFace : SurvivalHazard
{

	//----------------------------------
	// Constants
	//----------------------------------

	//Constants for the triggers of the animator
	int startRandomTrigger = Animator.StringToHash("startRandom");
	int  startBuff_1Trigger  = Animator.StringToHash("startBuff1");
	int  startBuff_2Trigger  = Animator.StringToHash("startBuff2");
	int  startBuff_3Trigger  = Animator.StringToHash("startBuff3");
	int  startBuff_4Trigger  = Animator.StringToHash("startBuff4");
    int startHazard_1Trigger = Animator.StringToHash("startHazard1");
    int startHazard_2Trigger = Animator.StringToHash("startHazard2");
    int startHazard_3Trigger = Animator.StringToHash("startHazard3");
    int startHazard_4Trigger = Animator.StringToHash("startHazard4");
    int  startIddleTrigger  = Animator.StringToHash("startIddle");
	int[] laserTriggerArray;
	//------------------------------------
	// Public Variables
	//------------------------------------
	//Player status for each player
	public PlayerStatus[] p_status;
	// The animator of the Face
	public Animator theAnimator;
	//Wait Time for each animation
	public float iddleWait;
	public float randomWait;
	public float buffWait;
    public float hazardWait;
    public float laserWait;
	//Array for the lasers
	public int laserArraySize;
    //Number of each laser  pos 0 easy pos 1 mid pos 2 hard
    public int[] numberOfItemDifficulty;
	//Electric Item
	public GameObject item;
	//SpawnZone & id
	public Transform[] itemSpawnTransform;
	//Item Parent for destruction
	public Transform itemParent;
    //AiSurvival For each egg
    public AISurvivalSmarty[] aiSurvival;
    //Is Laser & items going to be used?
    public bool laserSwitch;
    public bool itemSwitch;
    public bool hazardSwitch;
    //Pinnaple life span
    public float pinnapleDestructionTime=5f;
    //Time to spawn pinnaple
    public float pinnapleSpawnTime = 1f;
    //Is hotchiks
    public bool ishotChick;
    //Number of subrounds difficulty step
    public int difficultyStep=2;
    //LevelManagerSurvival
    public LevelManagerSurvival levelManager;

    public AudioMixerGroup mixerGroupCargaLaser;
    public AudioMixerGroup mixerGroupCargaCara;

    public AudioMixerGroup mixerGroupSpawnPiña;

    public AudioMixerGroup mixerGroupSpawnPuas;
    //------------------------------------
    // Private Variables
    //------------------------------------
    //Matrix of players on each bonus platform
    //First param player 
    //Second param bonusId
    bool  [][] bonusMatrix;
    //who is the bonusZone
	int bonusZoneid;
    //int to indicate the current round
	int currentRound;
    
    //audiosourcecontrol
    AudioSourceControl audioSourceControl;
    //State of the laser 
    //0 is easy, 1 is mid, 2 is hard
    int laserState;
    int subRounds;
    //----------------------------------
    // Methods
    //----------------------------------


    // Use this for initialization
    void Start () 
	{
        
        audioSourceControl = GetComponent<AudioSourceControl>();
		currentRound = 1;
		bonusZoneid = 4;	
		bonusMatrix = new bool [4][]
		{
			new bool[4],
			new bool[4],
			new bool[4],
			new bool[4]
		};
		//Create the triggers of the array
		laserTriggerArray = new int[laserArraySize];
		for(int i = 0;i<laserTriggerArray.Length;i++)
		{
			laserTriggerArray[i] = Animator.StringToHash("startLaser"+(i+1));
		}
		resetAllAnimationTriggers();
		//Starts random animation in a fixed time
		//manageAnimation(startIddleTrigger);
	}

	/**
    *Restarts the hazards of the world
    */

	public override void restartSurvivalHazards()
	{
        subRounds = 0;
        laserState = 0;
        audioSourceControl.stopAudioSource();
        destroyItems ();
		currentRound++;
		//Debug.Log("Restarting hazards");
		theAnimator.enabled = false;
		theAnimator.enabled = true;
		resetAllAnimationTriggers();
		theAnimator.SetBool(startIddleTrigger,true);
		exitAllPlayersFromBonusZone();
		StartCoroutine(startAnimationInTSeconds(iddleWait,startRandomTrigger,currentRound));
	}

	void destroyItems()
	{
		foreach (Transform child in itemParent)
		{
			GameObject.Destroy(child.gameObject);
		}
	}
	/**
    *Resets all triggers
    */
	void resetAllAnimationTriggers()
	{
		theAnimator.SetBool(startBuff_1Trigger,false);
		theAnimator.SetBool(startBuff_2Trigger,false);
		theAnimator.SetBool(startBuff_3Trigger,false);
		theAnimator.SetBool(startBuff_4Trigger,false);
        theAnimator.SetBool(startHazard_1Trigger, false);
        theAnimator.SetBool(startHazard_2Trigger, false);
        theAnimator.SetBool(startHazard_3Trigger, false);
        theAnimator.SetBool(startHazard_4Trigger, false);
        theAnimator.SetBool(startRandomTrigger,false);
		for(int i = 0;i<laserTriggerArray.Length;i++)
		{
			theAnimator.SetBool(laserTriggerArray[i] ,false);
		}
		theAnimator.SetBool(startIddleTrigger,false);
	}
	/**
    *Starts an animation a after t seconds
	*After starting the animation anounces the manager the new state
    *@param time for the next animation
    *@param next state after the animation
    *@param the current round for the animation
    */
	IEnumerator startAnimationInTSeconds(float time, int after,int round)
	{

		//Waits and then starts the behaviour
		yield return new WaitForSeconds (time);
        if (Time.timeScale == 1f&&currentRound==round)
		{
			resetAllAnimationTriggers();
			theAnimator.SetBool (after,true);
			manageAnimation (after);

		}
	}

    /**
    *Manager for the animations
	*Sets the new animation given the last state
    *@lastAnimationState the last animation state
    */
    void manageAnimation(int lastAnimationState)
    {
        if (Time.timeScale < 1f)
        {
            return;
        }
        //Iddle State
        if (lastAnimationState == startIddleTrigger)
        {
            audioSourceControl.stopAudioSource();
            StartCoroutine(startAnimationInTSeconds(iddleWait, startRandomTrigger, currentRound));
        }
        //Random face state

        else if (lastAnimationState == startRandomTrigger)
        {
            
            if (ishotChick)
            {
                GetComponent<AudioSource>().outputAudioMixerGroup = mixerGroupCargaLaser;
                EffectsManager.instance.playEffect(EffectsManager.EffectType.laserCharge, GetComponent<AudioSource>(), 2f, 0);
            }               
            else
            {
                GetComponent<AudioSource>().outputAudioMixerGroup = mixerGroupCargaCara;
                EffectsManager.instance.playEffect(EffectsManager.EffectType.faceCharge, GetComponent<AudioSource>(), 2f, 0);
            }
            
            //Randomly choose between lasers or buff
            int isLaser = 0;
            if (laserSwitch && itemSwitch&&hazardSwitch)
                isLaser = Random.Range(0, 3);
            else if (laserSwitch && itemSwitch && !hazardSwitch)
                isLaser = Random.Range(0, 2);
            else if (!laserSwitch && itemSwitch && hazardSwitch)
                isLaser = Random.Range(1, 4);
            else if (laserSwitch && !itemSwitch && !hazardSwitch)
                isLaser = 0;
            else if (!laserSwitch && itemSwitch && !hazardSwitch)
                isLaser = 1;
            else if (!laserSwitch && !itemSwitch && hazardSwitch)
                isLaser = 2;

            if (isLaser==0)
            {//Check difficulty
                if (subRounds < difficultyStep&&!levelManager.changedToB) laserState = 0;
                else if (subRounds < difficultyStep * 2) laserState = 1;
                else if (subRounds < difficultyStep * 4) laserState = 2;
                //set random laser for each difficulty
                int x = 0;
                //laserState = 3;//BORRAR ESTO CUANDO PROGRESIÓN ESTÉ COMPLETA
                switch (laserState)
                {
                    case 0://easy
                        x = Random.Range(0, numberOfItemDifficulty[0]);
                        break;
                    case 1://mid
                        x = Random.Range(numberOfItemDifficulty[0], numberOfItemDifficulty[0]+ numberOfItemDifficulty[1]);
                        break;
                    case 2://hard
                        x = Random.Range(numberOfItemDifficulty[0] + numberOfItemDifficulty[1], laserTriggerArray.Length);
                        break;
                    default:
                        x = Random.Range(0, laserTriggerArray.Length);
                        break;
                }
                subRounds++;
                //Debug.Log("Laser Number " + x+" - SR " +subRounds);
                StartCoroutine(startAnimationInTSeconds(randomWait, laserTriggerArray[x], currentRound));
            }
            else if(isLaser==2)
            {            
                    //Chooses a random hazard Zone
                    bonusZoneid = Random.Range(0, 4);

                    switch (bonusZoneid)
                    {
                        case 0:
                            StartCoroutine(startAnimationInTSeconds(randomWait, startHazard_1Trigger, currentRound));
                            break;
                        case 1:
                            StartCoroutine(startAnimationInTSeconds(randomWait, startHazard_2Trigger, currentRound));
                            break;
                        case 2:
                            StartCoroutine(startAnimationInTSeconds(randomWait, startHazard_3Trigger, currentRound));
                            break;
                        case 3:
                            StartCoroutine(startAnimationInTSeconds(randomWait, startHazard_4Trigger, currentRound));
                            break;
                    }
                
            }
            else 
            {
                //Chooses a random buff
                bonusZoneid =Random.Range(0,4);
                
                //Debug.Log(bonusZoneid+" buff");
                switch (bonusZoneid)
			    {
			        case 0:
				        StartCoroutine(startAnimationInTSeconds(randomWait,startBuff_1Trigger,currentRound));
				        break;
			        case 1:
				        StartCoroutine(startAnimationInTSeconds(randomWait,startBuff_2Trigger,currentRound));
				        break;
			        case 2:
				        StartCoroutine(startAnimationInTSeconds(randomWait,startBuff_3Trigger,currentRound));
				        break;
			        case 3:
				        StartCoroutine(startAnimationInTSeconds(randomWait,startBuff_4Trigger,currentRound));
				        break;
			    }
            }
            
            

		}
		//Buff 1-4
		else if(lastAnimationState == startBuff_1Trigger ||lastAnimationState == startBuff_2Trigger || lastAnimationState == startBuff_3Trigger ||lastAnimationState == startBuff_4Trigger)
		{
            EffectsManager.instance.playEffect(EffectsManager.EffectType.MachineDespawn, GetComponent<AudioSource>(), 1f, 0);
            StartCoroutine( instantiateItem());
            StartCoroutine(startAnimationInTSeconds(buffWait, startIddleTrigger, currentRound));
        }
        //Hazard 1-4
        else if (lastAnimationState == startHazard_1Trigger || lastAnimationState == startHazard_2Trigger || lastAnimationState == startHazard_3Trigger || lastAnimationState == startHazard_4Trigger)
        {
            GetComponent<AudioSource>().outputAudioMixerGroup = mixerGroupSpawnPuas;
            EffectsManager.instance.playEffect(EffectsManager.EffectType.doom, GetComponent<AudioSource>(), 0.8f, 0);
            StartCoroutine(startAnimationInTSeconds(hazardWait, startIddleTrigger, currentRound));
        }

        else
        {//Laser Time
			bool isLaserTime = false;
			for(int i = 0;i<laserTriggerArray.Length&&!isLaserTime;i++)
			{
				if(lastAnimationState==laserTriggerArray[i])
				{
					isLaserTime = true;
				}
			}

			if(isLaserTime) 
			{
				EffectsManager.instance.playEffect(EffectsManager.EffectType.laserShoot, GetComponent<AudioSource>(),1f,100);
				StartCoroutine(startAnimationInTSeconds(laserWait,startIddleTrigger,currentRound));
			}
		}
	}
	/**
    *Checks where must Instantiate the new item
    */
	IEnumerator instantiateItem()
	{
        yield return new WaitForSeconds(pinnapleSpawnTime);
		if(Time.timeScale>=1f)
		{
            GetComponent<AudioSource>().outputAudioMixerGroup = mixerGroupSpawnPiña;
            EffectsManager.instance.playEffect(EffectsManager.EffectType.newThunder, GetComponent<AudioSource>(), 1f, 100);
            GameObject.Destroy(GameObject.Instantiate(item, itemSpawnTransform[bonusZoneid].position, itemSpawnTransform[bonusZoneid].rotation, itemParent), pinnapleDestructionTime);
        }
        
		
	}
	/**
    *Notify when someOne enters a bonus Zone
    */
	public void enterBonusZone(int playerId, int bonusId)
	{
		bonusMatrix[playerId][bonusId] = true;
	}

	/**
    *Notify when someOne enters a bonus Zone
    */
	public void exitBonusZone(int playerId, int bonusId)
	{
		bonusMatrix[playerId][bonusId] = false;
	}
	/**
    *Marks all players as if they exited the bonus zone
    */
	void exitAllPlayersFromBonusZone()
	{
		for(int i = 0;i< 4;i++)
		{
			for(int j=0;j<4;j++)
			{
				exitBonusZone (i,j);
			}
		}
	}
}
