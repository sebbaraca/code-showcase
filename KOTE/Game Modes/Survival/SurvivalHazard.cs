﻿using UnityEngine;
using System.Collections;

/**
*Interface for survival mode. It must restart the hazards of each world
***********************************************************************************
* DEVELOPER LOG
*+ <Sebbaraca> <18/08/2017 > < Format Change>
*/

public abstract class SurvivalHazard : MonoBehaviour {


	//-----------------------------------
	// Methods
	//-----------------------------------
	//Restarts the hazards of the world

	public abstract void restartSurvivalHazards();
}
