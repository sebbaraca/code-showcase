﻿using UnityEngine;
using System.Collections;
/**
*This Class is set one each player for the survival mode
***********************************************************************************
* DEVELOPER LOG
*+ <Sebbaraca> <18/08/2017 > < Format Change>
*/
public class SurvivalPlayer : MonoBehaviour
{
	

	//------------------------------------
	// Public Variables
	//------------------------------------
	//The id of the Player
	public int playerId;
	//The level manager for survival
	public LevelManagerSurvival theGeneral;
    //Boolean to know if is boiled egg or not
    public bool isBoiledEgg;
    //Total points to be completed on boiled egg
    public float boilingPoint;
    //The number of points that increase each second to boil
    public float boilStep;
    //Number of points that must be increased when collected a hot item
    public float boilUpPoints;
    //Number of points that must be decreased when collected a cold item
    public float boilDownPoints;
    //time for each increase
    public float timeIncrease;
    //Actual boil points
    public float actualBoilPoints;
    
    //Hot and cold FX
    public GameObject hotFX;
    public GameObject coldFX;
    public float timeFX;
    //------------------------------------
    // Private Variables
    //------------------------------------
    //The sprite renderer
    private SpriteRenderer sr;
    //Player status
    PlayerStatus playerStatus;

    //----------------------------------------
    // Methods
    //----------------------------------------

    // Use this for initialization
    void Start () 
	{
        if (isBoiledEgg)
        {
            actualBoilPoints = 0;
            sr = GetComponent<SpriteRenderer>();
            playerStatus = GetComponent<PlayerStatus>();
        }
    }
	
	/**
    *Kills the player if touches something that kills him
    */
	void OnCollisionEnter2D(Collision2D col)
	{
		if(col.gameObject.tag=="SurvivalHazard")
		{			
			killPlayer ();
		}
        
    }

    /**
    *Increase or decrease hot on hotchicks
    *On ice breakers sets the snowball Fx
    */
    void OnTriggerEnter2D(Collider2D col)
    {
        if(isBoiledEgg)
        {
            if (col.gameObject.tag == "HotItem")
            {
                actualBoilPoints += boilUpPoints;
                GameObject.Destroy(GameObject.Instantiate(hotFX, col.gameObject.transform.position, transform.rotation), timeFX);
                GameObject.Destroy(col.gameObject);

            }
            else if (col.gameObject.tag == "ColdItem")
            {
                actualBoilPoints -= boilDownPoints;
                GameObject.Destroy(GameObject.Instantiate(coldFX,col.gameObject.transform.position,transform.rotation), timeFX);
                GameObject.Destroy(col.gameObject);
                
            }
            if (actualBoilPoints <= boilingPoint)
            {
                //Set color
                sr.color = new Color(1, (((boilingPoint - actualBoilPoints) / boilingPoint)), (((boilingPoint - actualBoilPoints) / boilingPoint)), 1);
            }
        }

        if (col.gameObject.tag.Contains("Snowball"))
        {
            //Debug.Log("Se dio con la bola");
            gameObject.GetComponent<PlayerStatus>().applyEffect(PlayerStatus.MINI_BLIZZARD);
        }

    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.tag.Contains("Snowball"))
        {
            gameObject.GetComponent<PlayerStatus>().applyEffect(PlayerStatus.NORMAL);
        }
    }

    //Kills the player
    public void killPlayer()
	{
		//Tell Survival General that he is dead
		if(theGeneral.canWinPoints)
		{
			theGeneral.killPlayer (playerId);
		}
	}

    //Restarts the boil points
    public void restartBoilPoints()
    {
        CancelInvoke();
        actualBoilPoints = 0;
        sr.color = Color.white;
        InvokeRepeating("boiledCountdown", 1f, timeIncrease);
    }

    //Increases permanently the boiled points on each egg
    void boiledCountdown()
    {
        //Increase
        actualBoilPoints += boilStep;
        if(actualBoilPoints<=boilingPoint)
        {
            //Set color
            sr.color = new Color(1,( ((boilingPoint - actualBoilPoints) / boilingPoint) ), (((boilingPoint - actualBoilPoints) / boilingPoint) ),1);
            Debug.Log("Color is " + sr.color.ToString());
        }
        else
        {
            sr.color = Color.red;
            //Death time
            theGeneral.killPlayer(playerId);
            CancelInvoke();
            playerStatus.setPlayerStatus(7);
        }


    }

  
}
