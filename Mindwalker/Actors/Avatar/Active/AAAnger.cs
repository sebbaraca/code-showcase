﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class inhereits from Avatar Active class
///It's the one that deals with the behaviour of the anger avatar state
/// </summary>
public class AAAnger : AvatarActive
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    //¨¨¨¨¨¨¨¨¨¨¨¨¨¨FRENZY STUFF¨¨¨¨¨¨¨¨¨¨¨¨¨¨
    //damageAmountMultiplier from frenzy
    public float frenzyDamageAmountMultiplier { get; set; }
    //damageSpeedMultiplier  from frenzy
    public float frenzyDamageSpeedMultiplier { get; set; }
    //lifeLeechAmount from frenzy
    public float frenzyLeechAmount { get; set; }
    //precisionAngle from frenzy
    public float frenzyDashMultiplier { get; set; }
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Duration for the Stun on the attacks
    private float stunDuration;
    //Rigid body of the avatar
    private Rigidbody rigidBody;
    //The avatar Movement
    private AvatarMovement avatarMovement;
    //Acceleration for the dash
    private float accelerationFactor;
    //Movement cooldown after the jump
    private float jumpMovementCooldown;
    //Movement cooldown before the jump
    private float jumpMovementPreparationTime;
    //The factor to slow the avatar before the Jump
    private float jumpPreparationSlowFactor;
    //The ammount of damage the slash attack does
    private float slashAttackDamage;
    //The ammount of damage the Tornado attack does
    private float tornadoAttackDamage;
    //Invulnerability for dash duration
    private float dashInvulnerabilityDuration;
    //Invulnerability for tornado duration
    private float tornadoInvulnerabilityDuration;
    //Force of tornado push back
    private float tornadoAttackForce;
    //Movement cooldown before the jump
    private float tornadoPreparationTime;
    //The factor to slow the avatar before the Jump
    private float tornadoSlowFactor;
    //Lift force for attacks
    private float upLiftForce;
    //Passive for anger
    private APAnger aPAnger;
    //********Animation Stuff**********
    //Number of ghosts for movement dash
    private int ghostsAmmountForDash;
    //Exclusivity time for tornado attack
    private float animExclusivityTornadoTime;
    //Exclusivity time for Dash attack
    private float animExclusivityDashTime;
    //Particle FX for tornado
    private ParticleSystem tornadoParticleSystem;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    protected override void Awake()
    {
        base.Awake();
        //Set Avatar state
        avatarState = AvatarManager.AvatarStates.Anger;
        //Get rigidbody of the avatar
        rigidBody = GetComponent<Rigidbody>();
        //Get Component For the movement and APAnger
        avatarMovement = GetComponent<AvatarMovement>();
        tornadoParticleSystem = GetComponentInChildren<ParticleSystem>();
        aPAnger = GetComponent<APAnger>();
    }
    /// <summary>
    /// Executes the active movement hability of the anger avatar state
    ///First charges forward and then excecute an slash attack
    /// </summary>
    public override void ActivateMovementAbility()
    {
        if (!isEnabled || isOnActiveMovementCoolDown) return;
        //Disable cooldown so can't spam
        isOnActiveMovementCoolDown = true;
        StartCoroutine(WaitAndDash());
    }

    /// <summary>
    /// Executes the active movement ability of the anger avatar state
    ///First charges forward and then excecute an slash attack
    /// </summary>
    public override void ActivatePowerAbility()
    {
        if (!isEnabled || isOnActivePowerCoolDown) return;
        //Disable cooldown so can't spam
        isOnActivePowerCoolDown = true;
        StartCoroutine(TornadoAndWait());
    }

    /// <summary>
    /// Waits a given ammount of time,
    /// then dashes forward and attacks
    /// </summary>
    private IEnumerator WaitAndDash()
    {
        avatarMovement.SlowAvatarMovement(jumpPreparationSlowFactor);
        //Disable Movement for a short period
        StartCoroutine(avatarMovement.DisableMovementTemporal(jumpMovementPreparationTime));
        yield return new WaitForSeconds(jumpMovementPreparationTime);
        //Manages animation
        ManageAnimation(true);
        //Charge Forward
        float chargeX = InputManager.instance.LastLJoystickInput.x;
        float chargeZ = InputManager.instance.LastLJoystickInput.y;
        float pythagoras = ((chargeX * chargeX) + (chargeZ * chargeZ));
        if (pythagoras > 1f) //Normalize input for same results on diagonals
        {
            float multiplier = 1f / (Mathf.Sqrt(pythagoras));
            chargeX *= multiplier;
            chargeZ *= multiplier;
        }
        rigidBody.AddForce(new Vector3(chargeX, 0f, chargeZ) * accelerationFactor * frenzyDashMultiplier);
        // Excecute attack
        try { ExecuteDashAttack(); } catch (System.Exception) { }
        //Disable Movement for a short period
        StartCoroutine(avatarMovement.DisableMovementTemporal(jumpMovementCooldown));
        //Initiate Cooldown time to enable power
        StartCoroutine(InitiateActiveMovementCoolDown(movementCoolDownTime * frenzyDamageSpeedMultiplier));
    }

    /// <summary>
    /// Waits a given ammount of time,
    ///then execute tornado attack
    /// </summary>
    private IEnumerator TornadoAndWait()
    {
        //Manages animation
        ManageAnimation(false);
        // Excecute attack
        try { ExecuteTornadoAttack(); } catch (System.Exception) { }
        avatarMovement.SlowAvatarMovement(tornadoSlowFactor);
        //Disable Movement for a short period
        StartCoroutine(avatarMovement.DisableMovementTemporal(tornadoPreparationTime));
        yield return new WaitForSeconds(tornadoPreparationTime);

        //Initiate Cooldown time to enable power
        StartCoroutine(InitiateActivePowerCoolDown(powerCoolDownTime * frenzyDamageSpeedMultiplier));
    }

    /// <summary>
    /// Executes the dash attack
    ///damaging all the objects in range
    /// </summary>
    private void ExecuteDashAttack()
    {        
        //Invulnerability for a few seconds
        StartCoroutine(hPManager.StartInvulnerableCoolDown(dashInvulnerabilityDuration));
        foreach (var item in objectsOnConeRange)
        {
            //Increase hitnumber on APanger
            aPAnger.IncreaseFrenzyHitPoints();
            //Life leech
            //hPManager.RecoverHP(frenzyLeechAmount);
            //Executes the attack
            item.GetComponent<HPManager>().TakeDamage(slashAttackDamage * frenzyDamageAmountMultiplier, true, stunDuration, false);
        }
    }

    /// <summary>
    /// Executes the tornado attack
    ///damaging all the objects in range and pushing them around
    ///however the user can't move during this attack
    /// </summary>
    private void ExecuteTornadoAttack()
    {
        Vector3 pushDirection = new Vector3();
        //Invulnerability for a few seconds
        StartCoroutine(hPManager.StartInvulnerableCoolDown(tornadoInvulnerabilityDuration));
        foreach (var item in objectsOnCircleRange)
        {
            //Increase hitnumber on APanger
            aPAnger.IncreaseFrenzyHitPoints();
            //Life leech
            // hPManager.RecoverHP(frenzyLeechAmount);
            //Executes the attack
            item.GetComponent<HPManager>().TakeDamage(tornadoAttackDamage * frenzyDamageAmountMultiplier, true, stunDuration, false);            
            //Calculate angle
            pushDirection = item.transform.position - transform.position;
            pushDirection.y += upLiftForce;
            //Pushes the enemy
            item.GetComponent<Rigidbody>().AddForce(pushDirection * tornadoAttackForce);
        }
    }

    /// <summary>
    /// Cleans the state before reActivating the Script by reseting it's values 
    /// </summary>
    public override void CleanAvatarState()
    {
        isEnabled = true;
        isOnActiveMovementCoolDown = false; //Disable Cooldowns
        isOnActivePowerCoolDown = false;
    }
    /// <summary>
    /// Manages the animation for the attacks
    /// </summary>
    /// <param name="isMovement">Is this a movement power?</param>
    private void ManageAnimation(bool isMovement)
    {
        if (isMovement)//Dash n Slash
        {
            //SFX for movement
            audioSource[0].clip = sfxAudioClips[0];
            audioSource[0].Play();
            //Ghost fx particles
            StartCoroutine(spriteGhostFX.SpawnGhostFx(ghostsAmmountForDash));
            //Animation for dash
            avatarAnimationManager.SetAttackAnimation(isMovement, animExclusivityDashTime);
        }
        else //Tornado 
        {
            //SFX for power
            audioSource[1].clip = sfxAudioClips[1];
            audioSource[1].Play();
            //Particles
            tornadoParticleSystem.Play();
            //Tornado animation
            avatarAnimationManager.SetAttackAnimation(isMovement, animExclusivityTornadoTime);
        }

    }

    /// <summary>    
    /// Sets the values for the data of this script    
    /// </summary>
    /// <param name="pMovementCoolDownTime">cooldownTime for dash & attack</param>
    /// <param name="pPowerCoolDownTime">cooldowntime for tornado attack   </param>
    /// <param name="pAccelerationFactor">Acceleration for the dash</param>
    /// <param name="pJumpMovementCooldown">Movement cooldown after the jump</param>
    /// <param name="pJumpMovementPreparationTime">Movement cooldown before the jump</param>
    /// <param name="pJumpPreparationSlowFactor">The factor to slow the avatar before the Jump</param>
    /// <param name="pSlashAttackDamage">The ammount of damage the slash attack does</param>
    /// <param name="pTornadoAttackDamage">The ammount of damage the Tornado attack does</param>
    /// <param name="pDashInvulnerabilityDuration">Invulnerability for dash duration</param>
    /// <param name="pTornadoInvulnerabilityDuration">Invulnerability for tornado duration</param>
    /// <param name="pTornadoAttackForce">Force of tornado push back   </param>
    /// <param name="pTornadoPreparationTime">Movement cooldown before the tornado</param>
    /// <param name="pTornadoSlowFactor">The factor to slow the avatar before the Jump   </param>
    /// <param name="pUpLiftForce">Lift force for attacks</param>
    /// <param name="pStunDuration">Duration for stun</param>
    /// <param name="pGhostsAmmountForDash">Number of ghosts for movement dash</param>
    /// <param name="pAnimExclusivityTornadoTime">Exclusivity time for tornado attack</param>
    /// <param name="pAnimExclusivityDashTime">Exclusivity time for Dash attack</param>
    public void SetValues(float pMovementCoolDownTime, float pPowerCoolDownTime, float pAccelerationFactor, float pJumpMovementCooldown,
        float pJumpMovementPreparationTime, float pJumpPreparationSlowFactor, float pSlashAttackDamage, float pTornadoAttackDamage,
        float pDashInvulnerabilityDuration, float pTornadoInvulnerabilityDuration, float pTornadoAttackForce, float pTornadoPreparationTime, float pTornadoSlowFactor, float pUpLiftForce, float pStunDuration
        , int pGhostsAmmountForDash, float pAnimExclusivityTornadoTime, float pAnimExclusivityDashTime)
    {
        ghostsAmmountForDash = pGhostsAmmountForDash;
        animExclusivityTornadoTime = pAnimExclusivityTornadoTime;
        animExclusivityDashTime = pAnimExclusivityDashTime;
        stunDuration = pStunDuration;
        upLiftForce = pUpLiftForce;
        movementCoolDownTime = pMovementCoolDownTime;
        powerCoolDownTime = pPowerCoolDownTime;
        accelerationFactor = pAccelerationFactor;
        jumpMovementCooldown = pJumpMovementCooldown;
        jumpMovementPreparationTime = pJumpMovementPreparationTime;
        jumpPreparationSlowFactor = pJumpPreparationSlowFactor;
        slashAttackDamage = pSlashAttackDamage;
        tornadoAttackDamage = pTornadoAttackDamage;
        dashInvulnerabilityDuration = pDashInvulnerabilityDuration;
        tornadoInvulnerabilityDuration = pTornadoInvulnerabilityDuration;
        tornadoAttackForce = pTornadoAttackForce;
        tornadoPreparationTime = pTornadoPreparationTime;
        tornadoSlowFactor = pTornadoSlowFactor;
    }
}
