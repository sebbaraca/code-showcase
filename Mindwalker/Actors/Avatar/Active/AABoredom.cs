﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class inhereits from Avatar Active class
/// It's the one that deals with the behaviour of the Boredom avatar state
/// </summary>
public class AABoredom : AvatarActive
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    //Is on one punch variation?
    public bool isOnePunchVariation;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Rigid body of the avatar
    private Rigidbody rigidBody;
    //Is the avatar on movement mode?
    private bool isOnMovementMode;
    //The avatarMovement of the avatar
    private AvatarMovement avatarMovement;
    //Acceleration for the dash
    private float accelerationFactor;
    //The boost factor of the movement speed for the ball mode of the state
    private float boostFactor;
    //The boost factor for floor attack (slow)
    private float floorBoostFactor;
    //Movement cooldown for transition
    private float movementCoolDown;
    //Movement cooldown for transition
    private float floorCoolDownTime;
    //The ammount of damage the one punch attack does
    private float onePunchAttackDamage;
    //Invulnerability for one punch duration
    private float onePunchInvulnerabilityDuration;
    //The force of the one punch when pushing enemies
    private float onePunchForce;
    //The ammount of damage the rolling punch attack does
    private float rollingPunchAttackDamage;
    //Invulnerability for rolling punch duration
    private float rollingPunchInvulnerabilityDuration;
    //The force of the rolling punch when pushing enemies
    private float rollingPunchForce;
    //The ammount of damage the floor attack does
    private float floorAttackDamage;
    //Invulnerability for floor duration
    private float floorInvulnerabilityDuration;
    //The force of the floor when pushing enemies
    private float floorForce;
    //Lift force for attacks
    public float upLiftForce;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    protected override void Awake()
    {
        base.Awake();
        //Get rigidbody of the avatar
        rigidBody = GetComponent<Rigidbody>();
        //Set Avatar state
        avatarState = AvatarManager.AvatarStates.Boredom;
        //Reset movement
        isOnMovementMode = false;
        //Get Component For the movement
        avatarMovement = GetComponent<AvatarMovement>();
    }
    /// <summary>
    /// Executes the active movement ability of the Boredom avatar state
    /// The avatar becomes a ball and starts rolling arround if is on golem mode and has no defense
    /// If is on ball mode becomesa golem that has high defense
    /// </summary>
    public override void ActivateMovementAbility()
    {
        if (!isEnabled) return;
        isOnMovementMode = !isOnMovementMode; //Switch the movement mode
        //Disable/Enable defense for HPManager
        hPManager.isIgnoringDefense = isOnMovementMode;
        //Disable Movement for a short period
        StartCoroutine(avatarMovement.DisableMovementTemporal(movementCoolDown));
        //If is on Movement mode the movement is multiplied for the boost factor, else returns to normal
        avatarMovement.boostFactor *= (isOnMovementMode ? boostFactor : 1 / boostFactor);
    }
    /// <summary>
    /// Executes the active power ability of the boredom avatar state
    /// executes a one punch damaging and pushing enemies
    /// </summary>
    public override void ActivatePowerAbility()
    {
        if (!isEnabled || isOnActivePowerCoolDown) return;
        //Disable cooldown so can't spam
        isOnActivePowerCoolDown = true;
        //Is on movement mode, therefore executes rolling punch attack
        if (isOnMovementMode) ExecuteRollingPunchAttack();
        //Isn't on movement mode, therefore executes one punch attack
        else if (isOnePunchVariation) ExecuteOnePunchAttack(); //One punch variation
        else ExecuteFloorAttack(); // Floor attack variation
    }
    /// <summary>
    /// Cleans the state before reActivating the Script by reseting it's values  
    /// </summary>
    public override void CleanAvatarState()
    {
        isEnabled = true;
        isOnActiveMovementCoolDown = false; //Disable Cooldowns
        isOnActivePowerCoolDown = false;
        isOnMovementMode = false; //Sets Golem Mode as Default
    }
    /// <summary>
    /// Executes a rolling punch attack
    /// exiting movement mode and pushing the people forward
    /// </summary>
    private void ExecuteRollingPunchAttack()
    {
        //Disable movement mode
        ActivateMovementAbility();
        //Charge Forward //Charge Forward
        float chargeX = InputManager.instance.LastLJoystickInput.x;
        float chargeZ = InputManager.instance.LastLJoystickInput.y;
        float pythagoras = ((chargeX * chargeX) + (chargeZ * chargeZ));
        if (pythagoras > 1f) //Normalize input for same results on diagonals
        {
            float multiplier = 1f / (Mathf.Sqrt(pythagoras));
            chargeX *= multiplier;
            chargeZ *= multiplier;
        }
        rigidBody.AddForce(new Vector3(chargeX, 0f, chargeZ) * accelerationFactor);
        Vector3 pushDirection = new Vector3();
        //Invulnerability for a few seconds
        StartCoroutine(hPManager.StartInvulnerableCoolDown(rollingPunchInvulnerabilityDuration));
        try
        {
            foreach (var item in objectsOnConeRange)
            {
                //Executes the attack
                item.GetComponent<HPManager>().TakeDamage(rollingPunchAttackDamage,false,0f,false);
                //Calculate angle
                pushDirection = item.transform.position - transform.position;
                pushDirection.y += 0.0001f;
                //Pushes the enemy
                item.GetComponent<Rigidbody>().AddForce(pushDirection * rollingPunchForce);
            }
        }
        catch (System.Exception) { }
        StartCoroutine(InitiateActivePowerCoolDown(powerCoolDownTime));
    }
    /// <summary>
    /// Executes a floor attack
    /// damaging all the objects in range and throwing and stun them in air
    /// </summary>
    private void ExecuteFloorAttack()
    {
        StartCoroutine(SlowAvatar());
        Vector3 pushDirection = new Vector3();
        //Invulnerability for a few seconds
        StartCoroutine(hPManager.StartInvulnerableCoolDown(floorInvulnerabilityDuration));
        try
        {
            foreach (var item in objectsOnCircleRange)
            {
                //Executes the attack
                item.GetComponent<HPManager>().TakeDamage(floorAttackDamage,false,0f,false);
                //Calculate angle
                pushDirection = item.transform.position - transform.position;
                pushDirection.y += upLiftForce;
                //Pushes the enemy
                item.GetComponent<Rigidbody>().AddForce(pushDirection * floorForce);
            }
        }
        catch (System.Exception) { }
        StartCoroutine(InitiateActivePowerCoolDown(floorCoolDownTime));
    }
    /// <summary>
    /// Executes a one punch attack
    /// damaging all the objects in range and pushing them
    /// </summary>
    private void ExecuteOnePunchAttack()
    {
        Vector3 pushDirection = new Vector3();
        //Invulnerability for a few seconds
        StartCoroutine(hPManager.StartInvulnerableCoolDown(onePunchInvulnerabilityDuration));
        try
        {
            foreach (var item in objectsOnConeRange)
            {
                //Executes the attack
                item.GetComponent<HPManager>().TakeDamage(onePunchAttackDamage,false,0f,false);
                //Calculate angle
                pushDirection = item.transform.position - transform.position;
                pushDirection.y += 0.0001f;
                //Pushes the enemy
                item.GetComponent<Rigidbody>().AddForce(pushDirection * onePunchForce);
            }
        }
        catch (System.Exception) { }
        StartCoroutine(InitiateActivePowerCoolDown(powerCoolDownTime));
    }

    /// <summary>
    /// Slows the avatar movement speed for a given duration
    /// </summary>
    private IEnumerator SlowAvatar()
    {
        avatarMovement.boostFactor = floorBoostFactor; //Slow
        yield return new WaitForSeconds(floorCoolDownTime);
        avatarMovement.boostFactor = 1f; //Back to normal
    }
    /// <summary>
    /// Sets the values for the data of this script 
    /// </summary>
    /// <param name="pMovementCoolDownTime">cooldownTime for dash & attack</param>
    /// <param name="pPowerCoolDownTime">cooldowntime for tornado attack</param>
    /// <param name="pAccelerationFactor">Acceleration for the dash</param>
    /// <param name="pBoostFactor">The boost factor of the movement speed for the ball mode of the state</param>
    /// <param name="pMovementCoolDown">Movement cooldown for transition</param>
    /// <param name="pOnePunchAttackDamage">The ammount of damage the one punch attack does</param>
    /// <param name="pOnePunchInvulnerabilityDuration">Invulnerability for one punch duration</param>
    /// <param name="pOnePunchForce">The force of the one punch when pushing enemies</param>
    /// <param name="pRollingPunchAttackDamage">The ammount of damage the rolling punch attack does</param>
    /// <param name="pRollingPunchInvulnerabilityDuration">Invulnerability for rolling punch duration</param>
    /// <param name="pRollingPunchForce">The force of the rolling punch when pushing enemies</param>
    /// <param name="pFloorAttackDamage">Damage amount for floor attack</param>
    /// <param name="pFloorInvulnerabilityDuration">Invulnerability duration for floor attack</param>
    /// <param name="pFloorForce">force of floor attack</param>
    /// <param name="pFloorCoolDownTime">cool down time for floor attack</param>
    /// <param name="pFloorBoostFactor">The slow factor for the floot attack</param>
    /// <param name="pUpLiftForce">Lift force for attacks</param>
    public void SetValues(float pMovementCoolDownTime, float pPowerCoolDownTime, float pAccelerationFactor, float pBoostFactor, float pMovementCoolDown, float pOnePunchAttackDamage, float pOnePunchInvulnerabilityDuration, float pOnePunchForce, float pRollingPunchAttackDamage, float pRollingPunchInvulnerabilityDuration, float pRollingPunchForce, float pFloorAttackDamage, float pFloorInvulnerabilityDuration, float pFloorForce, float pFloorCoolDownTime, float pFloorBoostFactor, float pUpLiftForce)
    {
        upLiftForce = pUpLiftForce;
        floorBoostFactor = pFloorBoostFactor;
        floorCoolDownTime = pFloorCoolDownTime;
        movementCoolDownTime = pMovementCoolDownTime;
        powerCoolDownTime = pPowerCoolDownTime;
        accelerationFactor = pAccelerationFactor;
        boostFactor = pBoostFactor;
        movementCoolDown = pMovementCoolDown;
        onePunchAttackDamage = pOnePunchAttackDamage;
        onePunchInvulnerabilityDuration = pOnePunchInvulnerabilityDuration;
        onePunchForce = pOnePunchForce;
        rollingPunchAttackDamage = pRollingPunchAttackDamage;
        rollingPunchInvulnerabilityDuration = pRollingPunchInvulnerabilityDuration;
        rollingPunchForce = pRollingPunchForce;
        floorAttackDamage = pFloorAttackDamage;
        floorForce = pFloorForce;
        floorInvulnerabilityDuration = pFloorInvulnerabilityDuration;
    }

}
