﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class inhereits from Avatar Active class
/// It's the one that deals with the behaviour of the Neutral avatar state 
/// </summary>
public class AANeutral : AvatarActive
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    //Prefab for projectile
    public GameObject projectilePrefab;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Transform of spawn point for projectile
    private Transform projectileSpawnPoint;
    //Rigid body of the avatar
    private Rigidbody rigidBody;
    //Movement Speed of the avatar (acceleration)
    private float accelerationFactor;
    //¨¨¨¨¨¨¨¨¨¨¨¨¨¨BULLET¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
    //The damage base amount
    private float bulletDamageBase;
    //The damage step while the bullet increases
    private float bulletDamageStep;
    //Travel Speed
    private float bulletSpeed;
    //Increasing size for explosion
    //Explosion Time
    private float explosionTime;
    //Explosion size
    private float explosionSizeStep;
    //Rotation speed
    private float rotationSpeed;
    //Time to explode for the bullet
    private float bulletTimeToExplode;
    //List of tags to be damaged
    private List<string> damageTags;
    //Number of times to shoot on each action
    private int shootAmmo;
    //Time between shots
    private float shootCooldown;
    //¨¨¨¨¨¨¨¨¨¨¨¨ANIMATION STUFF¨¨¨¨¨¨¨¨
    //Number of ghosts for movement dash
    private int ghostsAmmountForDash;
    //Exclusivity time for shoot animation
    private float animExclusivityDashTime;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    protected override void Awake()
    {
        base.Awake();
        //Set Avatar state
        avatarState = AvatarManager.AvatarStates.Neutral;
        //Get rigidbody of the avatar
        rigidBody = GetComponent<Rigidbody>();
        projectileSpawnPoint = GameObject.FindGameObjectWithTag("AvatarCannon").transform;
        //Tags of objects that will be damaged by the bullet
        damageTags = new List<string>() { "Enemy" };
    }
    /// <summary>
    /// Executes the active  movement hability of the Neutral avatar state
    /// Dashing forward
    /// </summary>
    public override void ActivateMovementAbility()
    {
        if (!isEnabled || isOnActiveMovementCoolDown) return;
        //Disable cooldown so can't spam
        isOnActiveMovementCoolDown = true;
        //Manage animation
        ManageAnimation(true);
        //Charge Forward//Charge Forward
        float chargeX = InputManager.instance.LastLJoystickInput.x;
        float chargeZ = InputManager.instance.LastLJoystickInput.y;
        float pythagoras = ((chargeX * chargeX) + (chargeZ * chargeZ));
        if (pythagoras > 1f) //Normalize input for same results on diagonals
        {
            float multiplier = 1f / (Mathf.Sqrt(pythagoras));
            chargeX *= multiplier;
            chargeZ *= multiplier;
        }
        rigidBody.AddForce(new Vector3(chargeX, 0f, chargeZ) * accelerationFactor);
        //Initiate Cooldown time to enable power
        StartCoroutine(InitiateActiveMovementCoolDown(movementCoolDownTime));
    }
    /// <summary>
    /// Executes the active power ability of the boredom avatar state
    ///executes a one punch damaging and pushing enemies
    /// </summary>
    public override void ActivatePowerAbility()
    {
        if (!isEnabled || isOnActivePowerCoolDown) return;
        //Manages animation
        ManageAnimation(false);
        //Disable cooldown so can't spam
        isOnActivePowerCoolDown = true;
        StartCoroutine(SpawnBullets());
        StartCoroutine(InitiateActivePowerCoolDown(powerCoolDownTime));
    }
    /// <summary>
    /// Cleans the state before reActivating the Script by reseting it's values
    /// </summary>
    public override void CleanAvatarState()
    {
        isEnabled = true;
        isOnActiveMovementCoolDown = false; //Disable Cooldowns
        isOnActivePowerCoolDown = false;
    }

    /// <summary>
    /// Spawns X bullets with a delay between them
    /// </summary>
    private IEnumerator SpawnBullets()
    {
        int currentShoot = 0;
        while (currentShoot < shootAmmo)
        {
            //Spawns Bullet
            GameObject bullet = GameObject.Instantiate(projectilePrefab, projectileSpawnPoint.position, projectileSpawnPoint.rotation);
            //Set values for bullet
            bullet.GetComponent<BulletManager>().SetValues(bulletDamageBase, bulletDamageStep, bulletSpeed, explosionTime, explosionSizeStep, rotationSpeed, damageTags, bulletTimeToExplode);
            yield return new WaitForSeconds(shootCooldown);
            currentShoot++;
        }
    }

    /// <summary>
    /// Manages the animation for the attacks
    /// </summary>
    /// <param name="isMovement">Is this a movement power?</param>
    private void ManageAnimation(bool isMovement)
    {
        if (isMovement)
        {
            //SFX for movement
            audioSource[0].clip = sfxAudioClips[0];
            audioSource[0].Play();
            //Ghost fx particles
            StartCoroutine(spriteGhostFX.SpawnGhostFx(ghostsAmmountForDash));
        }
        else //Knife time
        {           
            avatarAnimationManager.SetAttackAnimation(isMovement, animExclusivityDashTime);
        }
    }
    /// <summary>
    /// Sets the values for the data of this script
    /// </summary>
    /// <param name="pMovementCoolDownTime">cooldownTime for dash & attack</param>
    /// <param name="pPowerCoolDownTime">cooldowntime for tornado attack</param>
    /// <param name="pAccelerationFactor">Movement Speed of the avatar(acceleration)</param>
    /// <param name="pBulletDamageBase">The damage base amount</param>
    /// <param name="pBulletDamageStep">The damage step while the bullet increases</param>
    /// <param name="pBulletSpeed">Travel Speed</param>
    /// <param name="pExplosionTime">Explosion Time</param>
    /// <param name="pExplosionSizeStep">Explosion size</param>
    /// <param name="pRotationSpeed">Rotation speed</param>
    /// <param name="pBulletTimeToExplode">Time to explode for the bullet</param>
    /// <param name="pShootAmmo">Number of times to shoot on each action</param>
    /// <param name="pShootCooldown">Time between shots</param>
    /// <param name="pGhostsAmmountForDash">TNumber of ghosts for movement dash</param>
    /// <param name="pAnimExclusivityDashTime">Exclusivity time for shoot animation</param>
    public void SetValues(float pMovementCoolDownTime, float pPowerCoolDownTime, float pAccelerationFactor, float pBulletDamageBase, float pBulletDamageStep, float pBulletSpeed, float pExplosionTime, float pExplosionSizeStep, float pRotationSpeed, float pBulletTimeToExplode, int pShootAmmo, float pShootCooldown,int pGhostsAmmountForDash,float pAnimExclusivityDashTime)
    {
        ghostsAmmountForDash = pGhostsAmmountForDash;
        animExclusivityDashTime = pAnimExclusivityDashTime;
        shootAmmo = pShootAmmo;
        shootCooldown = pShootCooldown;
        bulletTimeToExplode = pBulletTimeToExplode;
        movementCoolDownTime = pMovementCoolDownTime;
        powerCoolDownTime = pPowerCoolDownTime;
        accelerationFactor = pAccelerationFactor;
        bulletDamageBase = pBulletDamageBase;
        bulletDamageStep = pBulletDamageStep;
        bulletSpeed = pBulletSpeed;
        explosionTime = pExplosionTime;
        explosionSizeStep = pExplosionSizeStep;
        rotationSpeed = pRotationSpeed;
    }



}

