﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class is the interface used for all avatar active habilities
/// The classes that inhereit from this class start with AA
/// The Avatar Manager Uses this script and sets the values for different avatar states
/// </summary>
public abstract class AvatarActive : MonoBehaviour
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    //ID to identify the Avatar State of the Active power
    public AvatarManager.AvatarStates avatarState;
    //Is the Avatar Active Power Enabled?
    public bool isEnabled { get; set; }
    //List of sfx for Active Ability 0 movement, 1 power
    public AudioClip[] sfxAudioClips;
    //Audio Source for active ability 0 movement, 1 power
    public AudioSource[] audioSource;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE-PROTECTED VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Cooldown time for the active movement
    protected float movementCoolDownTime;
    //Cooldown time for the active movement
    protected float powerCoolDownTime;
    //Is on cooldown for the activeMovement?
    protected bool isOnActiveMovementCoolDown;
    //Is on cooldown for the activePower?
    protected bool isOnActivePowerCoolDown;
    //List of objects on range that can be attacked
    protected List<GameObject> objectsOnConeRange;
    //List of objects on range that can be attacked
    protected List<GameObject> objectsOnCircleRange;
    //HP manager of the avatar
    protected HPManager hPManager;
    //UI Manager for Active Abilities
    protected UIAAManager uIAAManager;
    //Sprite GhostFx
    protected SpriteGhostFX spriteGhostFX;
    //Animation manager
    protected AvatarAnimationManager avatarAnimationManager;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //ABSTRACT METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~   
    //Activates the active movement Ability of the avatar state
    public abstract void ActivateMovementAbility();
    //Activates the active power Ability of the avatar state
    public abstract void ActivatePowerAbility();
    //Cleans the state before reActivating the Script by reseting it's values
    public abstract void CleanAvatarState();
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    
    protected virtual void Awake()
    {
        //Diable Scripts to avoid conflicts
        uIAAManager = GameObject.FindGameObjectWithTag("UIAAManager").GetComponent<UIAAManager>();
        hPManager = GetComponent<HPManager>();
        avatarAnimationManager = GetComponent<AvatarAnimationManager>();
        spriteGhostFX = GetComponentInChildren<SpriteGhostFX>();
        isEnabled = false;
        isOnActiveMovementCoolDown = false;
        objectsOnConeRange = new List<GameObject>();
        objectsOnCircleRange = new List<GameObject>();
    }

    //Waits for the cooldown time to re enable the cooldown flag for  active movement
    protected IEnumerator InitiateActiveMovementCoolDown(float coolDownTime)
    {
        StartCoroutine(uIAAManager.RefreshUI(false,coolDownTime));
        yield return new WaitForSeconds(coolDownTime);
        isOnActiveMovementCoolDown = false;
    }

    //Waits for the cooldown time to re enable the cooldown flag for  active power
    protected IEnumerator InitiateActivePowerCoolDown(float coolDownTime)
    {
        StartCoroutine(uIAAManager.RefreshUI(true, coolDownTime));
        yield return new WaitForSeconds(coolDownTime);
        isOnActivePowerCoolDown = false;
    }

    /// <summary>
    /// Registers the Game object with the enemy tag that entered on the hit 
    /// collider zone of the player.This will be used to hit all those who
    /// are inside when the attack button is pressed
    /// </summary>
    /// <param name="collision">collision the game object that entered the area</param>
    /// <param name="isCone">indicates if this object is from the cone area or the circle area</param>
    public void RegisterObject(GameObject collision,bool isCone)
    {        
        if (isCone) objectsOnConeRange.Add(collision);
        else objectsOnCircleRange.Add(collision);
    }

    /// <summary>
    /// Un Registers the Game object with the enemy tag that
    /// entered on the hit collider zone of the player
    /// </summary>
    /// <param name="collision">the game object that entered the area</param>
    /// <param name="isCone">indicates if this object is from the cone area or the circle area</param>
    public void UnRegisterObject(GameObject collision,bool isCone)
    {
        if (isCone) objectsOnConeRange.Remove(collision);
        else objectsOnCircleRange.Remove(collision);
    }
    /// <summary>
    /// Clears the lists of attack
    /// </summary>
    public void ClearAttackQueues()
    {
        objectsOnConeRange.Clear();
        objectsOnCircleRange.Clear();
    }

    //[[[[[[[[[[[[[[[  DELEGATES  ]]]]]]]]]]]]]]] 
    // Subscribes input 
    private void Start()
    { SubscribeInputs(); }

    // Unsuscribes from delegates before being destroyed to avoid memory leaks
    void OnDestroy()
    { UnSubscribeInputs(); }

    //Suscribes the Input Manager delegates 
    void SubscribeInputs()
    {
        InputManager.instance.OnAButtonEvent += ActivateMovementAbility;
        InputManager.instance.OnXButtonEvent += ActivatePowerAbility;
    }
    // Unsuscribes the Input Manager delegates
    void UnSubscribeInputs()
    {
        InputManager.instance.OnAButtonEvent -= ActivateMovementAbility;
        InputManager.instance.OnXButtonEvent -= ActivatePowerAbility;
    }

}
