﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class used to manage a trigger on an object, informing other classes when 
/// an object enters or exits an area
/// </summary>
public class AvatarTriggerManager : MonoBehaviour
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC  VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Is this trigger cone or circle area
    public bool isCone;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    //The Avatar Active Scripts
    private List<AvatarActive> avatarActives;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    private void Awake()
    {
        avatarActives = new List<AvatarActive>();
        foreach (var item in GetComponentsInParent<AvatarActive>())
        {
            avatarActives.Add(item);
        }

    }
    /// <summary>
    /// Registers the Game object with the enemy tag that entered on the hit 
    /// collider zone of the player.This will be used to hit all those who
    /// are inside when the attack button is pressed
    /// </summary>
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Enemy")
        {
            foreach (var item in avatarActives)
            {
                item.RegisterObject(collision.gameObject,isCone);
            }
        }
    }

    /// <summary>
    /// Un Registers the Game object with the enemy tag that
    /// entered on the hit collider zone of the player
    /// </summary>
    private void OnTriggerExit(Collider collision)
    {
        if (collision.tag == "Enemy")
        {
            foreach (var item in avatarActives)
            {
                item.UnRegisterObject(collision.gameObject,isCone);
            }
        }
    }
}
