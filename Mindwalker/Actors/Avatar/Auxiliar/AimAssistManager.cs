﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class is in charge of managing the aim assist feature of the game
/// </summary>
public class AimAssistManager : MonoBehaviour
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //CONSTANTS
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    //List of tags of game objects to be targeted
    public enum TargetTags{Enemy}
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Variable that determines if this script is in use
    private bool isUsingAssist;
    //List of targets in range
    private List<GameObject> targets;
    //Id of the current enemy selected in the List, is null if no target selected
    private GameObject currentTarget;
    //The target indicator
    private GameObject targetIndicator;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
  
    private void Start()
    {      
        targets = new List<GameObject>();
        currentTarget = null;
        targetIndicator = GameObject.FindGameObjectWithTag("TargetIndicator");
        targetIndicator.SetActive(false);        
        SubscribeInputs();
        SetNextTarget();
    }

    private void FixedUpdate()
    {
        if(currentTarget!=null)
        targetIndicator.transform.position = currentTarget.transform.position;
    }
    /// <summary>
    /// Returns the assist angle of the selected enemy in range
    /// </summary>
    /// <returns>The Angle of the selected enemy, The same angle if there is not enemy in range</returns>
    public float GiveAssistAngle(float currentAngle)
    {
        //If current target doesn't exist try to find another one
        if(currentTarget==null) SetNextTarget();
        if (currentTarget == null) return currentAngle;
        //Calculate angle
        Vector2 vector = new Vector2 ((currentTarget.transform.position.z - transform.position.z) * -1,(currentTarget.transform.position.x - transform.position.x));
        
        return Vector2.SignedAngle(vector, Vector2.right);
    }
    /// <summary>
    /// Sets the next target from the available targets. 
    /// Sets null if there is no target available
    /// </summary>
    public void SetNextTarget()
    {
        if (targets.Count > 0&&isUsingAssist)
        {
            int index = targets.IndexOf(currentTarget);
            //Change target to next in list
            if (index + 1 < targets.Count) index++;
            //List ended, try to return to start of list
            else index = 0;
            currentTarget = targets[index];
            targetIndicator.SetActive(true);            
        }
        //List of targets is empty, set the current target to null
        else
        {
            currentTarget = null;
            targetIndicator.SetActive(false);
        }
    }

    /// <summary>
    /// Sets the previous target from the available targets. 
    /// Sets null if there is no target available
    /// </summary>
    public void SetPreviousTarget()
    {
        if (targets.Count > 0 && isUsingAssist)
        {
            int index = targets.IndexOf(currentTarget);
            //Change target to previous in list
            if (index - 1 >= 0) index--;
            //List ended, try to return to end of list
            else index = targets.Count - 1;
            currentTarget = targets[index];
            targetIndicator.SetActive(true);            
        }
        //List of targets is empty, set the current target to null
        else
        {
            currentTarget = null;
            targetIndicator.SetActive(false);
        }
    }
    /// <summary>
    /// Registers a target in the list of available targets
    /// If there aren't any current targets, swap to this one
    /// </summary>
    /// <param name="newTarget">The target to register</param>
    public void RegisterTarget(GameObject newTarget)
    {
        //Add the target
        if(newTarget!=null)
        targets.Add(newTarget);
        //Set this as target if no current target
        if (currentTarget == null) SetNextTarget();
    }
    /// <summary>
    /// Unregisters a target from the available target list 
    /// Also Finds a new target if the current target is the one to be deleted
    /// </summary>
    /// <param name="target">The target to be deleted from the target list</param>
    public void UnRegisterTarget(GameObject target)
    {
        bool flag = (currentTarget == target);        
        //Delete from list
        targets.Remove(target);
        //Same target as the one to be deleted
        if (flag) SetNextTarget();
    }
    /// <summary>
    /// Turns on and off this script
    /// </summary>
    /// <param name="value">True if in use, false if not</param>
    public void SetAssistValue(bool value)
    {
        bool flag = (value==isUsingAssist);
        isUsingAssist = value;
        if(!flag) SetNextTarget();
    }
    /// <summary>
    /// Cleans the aim assist
    /// </summary>
    public void CleanAimAssist()
    {
        targets.Clear();
        SetNextTarget();
    }
    //¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨TRIGGER STUFF¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨

    private void OnTriggerEnter(Collider other)
    {
        if(TargetTags.Enemy.ToString()==other.tag)
        {
            RegisterTarget(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (TargetTags.Enemy.ToString() == other.tag)
        {
            UnRegisterTarget(other.gameObject);
        }
    }

    //[[[[[[[[[[[[[[[  DELEGATES  ]]]]]]]]]]]]]]] 
    //Unsuscribes from delegates before being destroyed
    void OnDestroy()
    {
        // We unsubscribe to avoid memory leaks
        UnSubscribeInputs();
    }

    //Suscribes the Input Manager delegates
    void SubscribeInputs()
    {
        InputManager.instance.OnRBButtonEvent += SetNextTarget;
        InputManager.instance.OnLBButtonEvent += SetPreviousTarget;
    }
    //Unsuscribes the Input Manager delegates
    void UnSubscribeInputs()
    {
        InputManager.instance.OnRBButtonEvent -= SetNextTarget;
        InputManager.instance.OnLBButtonEvent -= SetPreviousTarget;
    }
}
