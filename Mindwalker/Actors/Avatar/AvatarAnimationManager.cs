﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class is in charge of coordinating the animations for the avatar
/// </summary>
public class AvatarAnimationManager : MonoBehaviour
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //CONSTANTS 
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //The list of possible animation states
    public enum AnimationStates
    {
        //Neutral
        Attack_Right_Trigger, Attack_Left_Trigger, Attack_Back_Trigger, Attack_Front_Trigger,
        Movement_Right_Trigger, Movement_Left_Trigger, Movement_Back_Trigger, Movement_Front_Trigger,
        Iddle_Right_Trigger, Iddle_Left_Trigger, Iddle_Back_Trigger, Iddle_Front_Trigger,
        //Anger
        Anger_Attack_Right_Trigger, Anger_Attack_Left_Trigger, Anger_Attack_Back_Trigger, Anger_Attack_Front_Trigger,
        Anger_Movement_Right_Trigger, Anger_Movement_Left_Trigger, Anger_Movement_Back_Trigger, Anger_Movement_Front_Trigger,
        Anger_Iddle_Right_Trigger, Anger_Iddle_Left_Trigger, Anger_Iddle_Back_Trigger, Anger_Iddle_Front_Trigger
        //Boredom
    }
    //The avatar orientation
    public enum Orientations
    {
        Right,Left,Front,Back,Front_Right,Front_Left,Back_Right,Back_Left
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES 
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //The current orientation of the avatar
    public Orientations currentOrientation;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES 
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Current state
    private AnimationStates currentState;
    //The Avatar Animator
    private Animator animator;
    //The current avatar state
    private AvatarManager.AvatarStates avatarState;    
    //Can set a new animation?
    private bool canSetAnimation;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS 
    //~~~~~~~~~~~~~~~~~~~~~~~~
    private void Awake()
    {
        animator = this.GetComponent<Animator>();
        canSetAnimation = true;
    }
    /// <summary>
    /// Swaps the avatar state and sets iddle animation
    /// </summary>
    /// <param name="newAvatarState">The new avatar state</param>
    public void SwapAvatarState(AvatarManager.AvatarStates newAvatarState)
    {
        StopAllCoroutines();
        canSetAnimation = true;
        avatarState = newAvatarState;
        SetMovementAnimation(Orientations.Front, true);
    }
    /// <summary>
    /// Sets an animation state for the avatar and disables previous one
    /// </summary>
    /// <param name="animationState">The new animation state</param>
    private void SetAnimationState(AnimationStates animationState)
    {        
        //Disable previous state
        animator.SetBool(currentState.ToString(),false);
        //Set new state for animator
        currentState = animationState;        
        animator.SetBool(currentState.ToString(), true);
    }
    /// <summary>
    /// Sets the animation for movement-iddle given the avatar state
    /// </summary>
    /// <param name="orientation">The orientation of the animation</param>
    /// <param name="isIddle">Is this animation iddle?</param>
    public void SetMovementAnimation(Orientations orientation,bool isIddle)
    {
        currentOrientation= orientation;        
        if (!canSetAnimation) return;        
        switch (avatarState)
        {
            case AvatarManager.AvatarStates.Neutral:
                switch (currentOrientation)
                {
                    case Orientations.Right:
                        if (isIddle) SetAnimationState(AnimationStates.Iddle_Right_Trigger);
                        else SetAnimationState(AnimationStates.Movement_Right_Trigger);
                        break;
                    case Orientations.Left:
                        if (isIddle) SetAnimationState(AnimationStates.Iddle_Left_Trigger);
                        else SetAnimationState(AnimationStates.Movement_Left_Trigger);
                        break;
                    case Orientations.Front:
                        if (isIddle) SetAnimationState(AnimationStates.Iddle_Front_Trigger);
                        else SetAnimationState(AnimationStates.Movement_Front_Trigger);
                        break;
                    case Orientations.Back:
                        if (isIddle) SetAnimationState(AnimationStates.Iddle_Back_Trigger);
                        else SetAnimationState(AnimationStates.Movement_Back_Trigger);
                        break;
                    case Orientations.Front_Right:
                        if (isIddle) SetAnimationState(AnimationStates.Iddle_Right_Trigger);
                        else SetAnimationState(AnimationStates.Movement_Right_Trigger);
                        break;
                    case Orientations.Front_Left:
                        if (isIddle) SetAnimationState(AnimationStates.Iddle_Left_Trigger);
                        else SetAnimationState(AnimationStates.Movement_Left_Trigger);
                        break;
                    case Orientations.Back_Right:
                        if (isIddle) SetAnimationState(AnimationStates.Iddle_Right_Trigger);
                        else SetAnimationState(AnimationStates.Movement_Right_Trigger);
                        break;
                    case Orientations.Back_Left:
                        if (isIddle) SetAnimationState(AnimationStates.Iddle_Left_Trigger);
                        else SetAnimationState(AnimationStates.Movement_Left_Trigger);
                        break;
                    default:
                        break;
                }
                break;
            case AvatarManager.AvatarStates.Anger:
                switch (currentOrientation)
                {
                    case Orientations.Right:
                        if (isIddle) SetAnimationState(AnimationStates.Anger_Iddle_Right_Trigger);
                        else SetAnimationState(AnimationStates.Anger_Movement_Right_Trigger);
                        break;
                    case Orientations.Left:
                        if (isIddle) SetAnimationState(AnimationStates.Anger_Iddle_Left_Trigger);
                        else SetAnimationState(AnimationStates.Anger_Movement_Left_Trigger);
                        break;
                    case Orientations.Front:
                        if (isIddle) SetAnimationState(AnimationStates.Anger_Iddle_Front_Trigger);
                        else SetAnimationState(AnimationStates.Anger_Movement_Front_Trigger);
                        break;
                    case Orientations.Back:
                        if (isIddle) SetAnimationState(AnimationStates.Anger_Iddle_Back_Trigger);
                        else SetAnimationState(AnimationStates.Anger_Movement_Back_Trigger);
                        break;
                    case Orientations.Front_Right:
                        if (isIddle) SetAnimationState(AnimationStates.Anger_Iddle_Right_Trigger);
                        else SetAnimationState(AnimationStates.Anger_Movement_Right_Trigger);
                        break;
                    case Orientations.Front_Left:
                        if (isIddle) SetAnimationState(AnimationStates.Anger_Iddle_Left_Trigger);
                        else SetAnimationState(AnimationStates.Anger_Movement_Left_Trigger);
                        break;
                    case Orientations.Back_Right:
                        if (isIddle) SetAnimationState(AnimationStates.Anger_Iddle_Right_Trigger);
                        else SetAnimationState(AnimationStates.Anger_Movement_Right_Trigger);
                        break;
                    case Orientations.Back_Left:
                        if (isIddle) SetAnimationState(AnimationStates.Anger_Iddle_Left_Trigger);
                        else SetAnimationState(AnimationStates.Anger_Movement_Left_Trigger);
                        break;
                    default:
                        break;
                }
                break;
            case AvatarManager.AvatarStates.Boredom:
                break;
            default:
                break;
        }
    }
    /// <summary>
    /// Sets the attack animation on the fly,
    /// giving it a exclusivity time
    /// </summary>
    /// <param name="isMovement">Is this a movement attack?</param>
    /// <param name="exclusiveTime">The time this animation can't be interrupted</param>
    public void SetAttackAnimation(bool isMovement,float exclusiveTime)
    {
        StartCoroutine(SetExclusiveTime(exclusiveTime));
        switch (avatarState)
        {
            case AvatarManager.AvatarStates.Neutral:
                switch (currentOrientation)
                {
                    case Orientations.Right:
                        if (isMovement) SetAnimationState(AnimationStates.Attack_Right_Trigger);
                        else SetAnimationState(AnimationStates.Attack_Right_Trigger);
                        break;
                    case Orientations.Left:
                        if (isMovement) SetAnimationState(AnimationStates.Attack_Left_Trigger);
                        else SetAnimationState(AnimationStates.Attack_Left_Trigger);
                        break;
                    case Orientations.Front:
                        if (isMovement) SetAnimationState(AnimationStates.Attack_Front_Trigger);
                        else SetAnimationState(AnimationStates.Attack_Front_Trigger);
                        break;
                    case Orientations.Back:
                        if (isMovement) SetAnimationState(AnimationStates.Attack_Back_Trigger);
                        else SetAnimationState(AnimationStates.Attack_Back_Trigger);
                        break;
                    case Orientations.Front_Right:
                        if (isMovement) SetAnimationState(AnimationStates.Attack_Right_Trigger);
                        else SetAnimationState(AnimationStates.Attack_Right_Trigger);
                        break;
                    case Orientations.Front_Left:
                        if (isMovement) SetAnimationState(AnimationStates.Attack_Left_Trigger);
                        else SetAnimationState(AnimationStates.Attack_Left_Trigger);
                        break;
                    case Orientations.Back_Right:
                        if (isMovement) SetAnimationState(AnimationStates.Attack_Right_Trigger);
                        else SetAnimationState(AnimationStates.Attack_Right_Trigger);
                        break;
                    case Orientations.Back_Left:
                        if (isMovement) SetAnimationState(AnimationStates.Attack_Left_Trigger);
                        else SetAnimationState(AnimationStates.Attack_Left_Trigger);
                        break;
                    default:
                        break;
                }
                break;
            case AvatarManager.AvatarStates.Anger:
                switch (currentOrientation)
                {
                    case Orientations.Right:
                        if (isMovement) SetAnimationState(AnimationStates.Anger_Attack_Right_Trigger);
                        else SetAnimationState(AnimationStates.Anger_Attack_Right_Trigger);
                        break;
                    case Orientations.Left:
                        if (isMovement) SetAnimationState(AnimationStates.Anger_Attack_Left_Trigger);
                        else SetAnimationState(AnimationStates.Anger_Attack_Left_Trigger);
                        break;
                    case Orientations.Front:
                        if (isMovement) SetAnimationState(AnimationStates.Anger_Attack_Front_Trigger);
                        else SetAnimationState(AnimationStates.Anger_Attack_Front_Trigger);
                        break;
                    case Orientations.Back:
                        if (isMovement) SetAnimationState(AnimationStates.Anger_Attack_Back_Trigger);
                        else SetAnimationState(AnimationStates.Anger_Attack_Back_Trigger);
                        break;
                    case Orientations.Front_Right:
                        if (isMovement) SetAnimationState(AnimationStates.Anger_Attack_Right_Trigger);
                        else SetAnimationState(AnimationStates.Anger_Attack_Right_Trigger);
                        break;
                    case Orientations.Front_Left:
                        if (isMovement) SetAnimationState(AnimationStates.Anger_Attack_Left_Trigger);
                        else SetAnimationState(AnimationStates.Anger_Attack_Left_Trigger);
                        break;
                    case Orientations.Back_Right:
                        if (isMovement) SetAnimationState(AnimationStates.Anger_Attack_Right_Trigger);
                        else SetAnimationState(AnimationStates.Anger_Attack_Right_Trigger);
                        break;
                    case Orientations.Back_Left:
                        if (isMovement) SetAnimationState(AnimationStates.Anger_Attack_Left_Trigger);
                        else SetAnimationState(AnimationStates.Anger_Attack_Left_Trigger);
                        break;
                    default:
                        break;
                }
                break;
            case AvatarManager.AvatarStates.Boredom:
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Sets an exlusivity time for an animation
    /// </summary>
    /// <param name="time">The exclusivity time</param>
    private IEnumerator SetExclusiveTime(float time)
    {
        canSetAnimation = false;
        yield return new WaitForSeconds(time);
        canSetAnimation = true;
    }
}
