﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class is in charge of managing the avatar state
/// Therefore it activates the profiles on movement, changes the active powers
/// and passives.Also manages health profiles.
/// </summary>
public class AvatarManager : MonoBehaviour
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //CONSTANTS 
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //The list of avatar states to be used on the game.
    //Note that you must also add or remove this info on the same order as AvatarSnapshotManager.xml
    public enum AvatarStates
    { Neutral, Anger, Boredom }
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~   
    //List of colors for avatar
    public Color[] avatarColors;
    //The current avatar snapshot
    public AvatarStates avatarState;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~ 
    //The current avatar snapshot
    private AvatarSnapshotManager.AvatarSnapshot currentAvatarSnapshot;
    //The data structure with the loaded data of each avatar state
    private List<AvatarSnapshotManager.AvatarSnapshot> avatarSnapshots;
    //The player movement of the avatar
    private AvatarMovement avatarMovement;
    //List of Avatar Active scripts
    private Dictionary<AvatarStates, AvatarActive> avatarActives;
    //The Active Avatar State Selected
    private AvatarActive avatarActive;
    //List of Avatar Passive scripts
    private Dictionary<AvatarStates, AvatarPassive> avatarPassives;
    //The Passive Avatar State Selected
    private AvatarPassive avatarPassive;
    //HP Manager of the avatar
    private HPManager hPManager;
    //The animation manager
    private AvatarAnimationManager avatarAnimationManager;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~   
    // Use this for initialization
    void Start()
    {
        //Get Script Components
        GetScriptReferences();
        //Load Parameters
        LoadAvatarData();
        //Set default avatar
        SwapAvatarState(AvatarStates.Neutral, false);
    }
    /// <summary>
    /// Initializes all script references of the script
    /// </summary>
    private void GetScriptReferences()
    {
        //Get avatar animation manager
        avatarAnimationManager = GetComponent<AvatarAnimationManager>();
        //Get Avatar Movement
        avatarMovement = GetComponent<AvatarMovement>();
        avatarMovement.InitializeMovement();
        //Get HP manager 
        hPManager = GetComponent<HPManager>();
        //Get Avatar Active Powers
        avatarActives = new Dictionary<AvatarStates, AvatarActive>();
        foreach (var item in GetComponents<AvatarActive>())
        { avatarActives.Add(item.avatarState, item); }
        //Set default active power
        avatarActive = avatarActives[AvatarStates.Neutral];
        avatarActive.isEnabled = true;
        // Get Avatar Passive Powers
        avatarPassives = new Dictionary<AvatarStates, AvatarPassive>();
        foreach (var item in GetComponents<AvatarPassive>())
        { avatarPassives.Add(item.avatarState, item); }
        avatarPassive = avatarPassives[AvatarStates.Neutral];
    }

    /// <summary>
    /// Loads the avatar states data into the game
    /// Data is loaded into the snapshots list
    /// </summary>
    void LoadAvatarData()
    {
        //General Snapshot
        avatarSnapshots = AvatarSnapshotManager.Load().AvatarSnapshots;
        //Anger active and passive values
        AvatarAngerSnapshotManager.AngerSnapshot angerSnapshot = AvatarAngerSnapshotManager.Load().angerSnapshot;
        AAAnger aAAnger = (AAAnger)avatarActives[AvatarStates.Anger];
        aAAnger.SetValues(angerSnapshot.movementCoolDownTime, angerSnapshot.powerCoolDownTime, angerSnapshot.accelerationFactor, angerSnapshot.jumpMovementCooldown, angerSnapshot.jumpMovementPreparationTime, angerSnapshot.jumpPreparationSlowFactor, angerSnapshot.slashAttackDamage, angerSnapshot.tornadoAttackDamage, angerSnapshot.dashInvulnerabilityDuration, angerSnapshot.tornadoInvulnerabilityDuration, angerSnapshot.tornadoAttackForce, angerSnapshot.tornadoPreparationTime, angerSnapshot.tornadoSlowFactor, angerSnapshot.upLiftForce, angerSnapshot.stunDuration, angerSnapshot.ghostsAmmountForDash, angerSnapshot.animExclusivityTornadoTime, angerSnapshot.animExclusivityDashTime);
        APAnger aPAnger = (APAnger)avatarPassives[AvatarStates.Anger];
        aPAnger.SetValues(angerSnapshot.maxFrenzyTime, angerSnapshot.frenzyHitsNeeded, angerSnapshot.damageAmountMultipliers, angerSnapshot.damageSpeedMultipliers, angerSnapshot.selfDamagesPerSecond, angerSnapshot.lifeLeechAmounts, angerSnapshot.dashMultiplier);
        //Boredom active and passive values
        AvatarBoredomSnapshotManager.BoredomSnapshot boredomSnapshot = AvatarBoredomSnapshotManager.Load().boredomSnapshot;
        AABoredom aABoredom = (AABoredom)avatarActives[AvatarStates.Boredom];
        aABoredom.SetValues(boredomSnapshot.movementCoolDownTime, boredomSnapshot.powerCoolDownTime, boredomSnapshot.accelerationFactor, boredomSnapshot.boostFactor, boredomSnapshot.movementCoolDown, boredomSnapshot.onePunchAttackDamage, boredomSnapshot.onePunchInvulnerabilityDuration, boredomSnapshot.onePunchForce, boredomSnapshot.rollingPunchAttackDamage, boredomSnapshot.rollingPunchInvulnerabilityDuration, boredomSnapshot.rollingPunchForce, boredomSnapshot.floorAttackDamage, boredomSnapshot.floorInvulnerabilityDuration, boredomSnapshot.floorForce, boredomSnapshot.floorCoolDownTime, boredomSnapshot.floorBoostFactor, boredomSnapshot.upLiftForce);
        APBoredom aPBoredom = (APBoredom)avatarPassives[AvatarStates.Boredom];
        aPBoredom.SetValues();//TODO
        //Neutral active and passive values
        AvatarNeutralSnapshotManager.NeutralSnapshot neutralSnapshot = AvatarNeutralSnapshotManager.Load().neutralSnapshot;
        AANeutral aANeutral = (AANeutral)avatarActives[AvatarStates.Neutral];
        aANeutral.SetValues(neutralSnapshot.movementCoolDownTime, neutralSnapshot.powerCoolDownTime, neutralSnapshot.accelerationFactor, neutralSnapshot.bulletDamageBase, neutralSnapshot.bulletDamageStep, neutralSnapshot.bulletSpeed, neutralSnapshot.explosionTime, neutralSnapshot.explosionSizeStep, neutralSnapshot.rotationSpeed, neutralSnapshot.bulletTimeToExplode, neutralSnapshot.shootAmmo, neutralSnapshot.shootCooldown, neutralSnapshot.ghostsAmmountForDash, neutralSnapshot.animExclusivityDashTime);
        APNeutral aPNeutral = (APNeutral)avatarPassives[AvatarStates.Neutral];
        aPNeutral.SetValues();//TODO
    }
    /// <summary>
    /// Swaps the avatar state. 
    /// This implies changing movement, damage, health     
    /// </summary>
    /// <param name="pAvatarState">The Id of the new state to be applied</param>
    /// <param name="isLoadingCheckpoint">is the method loading a checkpoint?</param>
    public void SwapAvatarState(AvatarStates pAvatarState, bool isLoadingCheckpoint)
    {
        //TODO-------------------- Remove this  ---------------------    
        switch (pAvatarState)
        {
            case AvatarStates.Neutral:
                GetComponentInChildren<SpriteRenderer>().color = avatarColors[0];
                break;
            case AvatarStates.Anger:
                GetComponentInChildren<SpriteRenderer>().color = avatarColors[1];
                break;
            case AvatarStates.Boredom:
                GetComponentInChildren<SpriteRenderer>().color = avatarColors[2];
                break;
            default:
                break;
        }
        //----------------------------------------------------------
        //Clear queues if reloading checkpoint
        if(isLoadingCheckpoint)
        {
            for (int i = 0; i < avatarActives.Count; i++)
            {
                avatarActives[(AvatarStates)i].ClearAttackQueues();
            }          
        }
        avatarState = pAvatarState;
        //SWap state on animator
        avatarAnimationManager.SwapAvatarState(pAvatarState);
        //Change current avatar state
        currentAvatarSnapshot = avatarSnapshots[(int)pAvatarState];
        //Swap Movement Behaviour
        avatarMovement.ChangePlayerMovementValues(currentAvatarSnapshot.accelerationFactor, currentAvatarSnapshot.maxSpeed, currentAvatarSnapshot.slowFactor, currentAvatarSnapshot.mass, currentAvatarSnapshot.linearDrag, currentAvatarSnapshot.isMelee);
        //Swap and clean Active Behaviour
        avatarActive.isEnabled = false;
        avatarActive = avatarActives[pAvatarState];
        avatarActive.CleanAvatarState();
        //Swap Passive and clean Behaviour        
        avatarPassive.DisablePassive();
        avatarPassive = avatarPassives[pAvatarState];
        avatarPassive.CleanAvatarState();
        //HP settings
        hPManager.SetValues(currentAvatarSnapshot.health, currentAvatarSnapshot.defensivePower, currentAvatarSnapshot.hurtingCooldown, currentAvatarSnapshot.warningThreshold, isLoadingCheckpoint, true);
    }
    /// <summary>
    /// Stuns the avatar for a given duration, disabling movement and actions
    /// </summary>
    /// <param name="duration">The duration of the stun</param>    
    public IEnumerator StunForDuration(float duration)
    {
        avatarActive.isEnabled = false;
        StartCoroutine(avatarMovement.DisableMovementTemporal(duration));
        yield return new WaitForSeconds(duration);
        avatarActive.isEnabled = true;
    }
    /// <summary>
    /// Enables or disables the Avatar behaviour
    /// Used for cinematics n stuff
    /// </summary>
    /// <param name="isEnabled">Is the avatar enabled?</param>
    public void EnableAvatarBehaviour(bool isEnabled)
    {
        if (avatarActive != null)
        {
            avatarActive.isEnabled = isEnabled;
            avatarMovement.SetMovementState(isEnabled);
        }
    }
}
