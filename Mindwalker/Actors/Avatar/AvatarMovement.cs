﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class is the logic for the Avatar movement
/// The Avatar Manager Uses this script and sets the values for different avatar states
/// </summary>
public class AvatarMovement : MonoBehaviour
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    //Boost factor used as multiplier of the base speed
    public float boostFactor { get; set; }
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Movement Speed of the avatar (acceleration)
    private float accelerationFactor;
    //Max speed that the avatar can achieve on x or y
    private float maxSpeed;
    //Slow factor when the avatar changes direction or stops accelerating
    private float slowFactor;
    //Mass of the player's rigid body
    private float mass;
    //Linear drag of the rigid body
    private float linearDrag;    
    //Rigid body of the avatar
    private Rigidbody rigidBody;
    //currentMovement direction from the input
    private Vector3 currentMovement;
    //Is the movement enabled?
    private bool isMovementEnabled;
    //The transform of the Pivot of the things that must rotate with the character
    private Transform pivotTransform;
    //Aim Assist manager
    private AimAssistManager aimAssistManager;
    //Is this avatar mode melee?
    private bool isMelee;
    //Animation Manager for the avatar
    private AvatarAnimationManager avatarAnimationManager;
    //Is the avatar in iddle state?
    private bool isIddle;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~

  
    /// <summary>
    /// Initializes all the variables of this script
    /// </summary>
    public void InitializeMovement()
    {
        pivotTransform = GameObject.FindGameObjectWithTag("Pivot").transform;
        aimAssistManager = GetComponentInChildren<AimAssistManager>();
        avatarAnimationManager = GetComponent<AvatarAnimationManager>();
        boostFactor = 1;
        rigidBody = GetComponent<Rigidbody>();
        isMovementEnabled = true;
        isIddle = true;
        //Subscribe to delegates for input
        SubscribeInputs();
    }
    /// <summary>
    /// Disables the movement for a given period     
    /// </summary>
    /// <param name="coolDown">the time that the movement will be disabled</param>    
    public IEnumerator DisableMovementTemporal(float coolDown)
    {
        isMovementEnabled = false;
        yield return new WaitForSeconds(coolDown);
        isMovementEnabled = true;
    }
    /// <summary>
    /// Enables or disables movement 
    /// </summary>
    /// <param name="isEnabled">Is the movement enabled?</param>
    public void SetMovementState(bool isEnabled)
    {
        isMovementEnabled=isEnabled;
    }
     
    /// <summary>
    /// Moves the player on the X,Y direction given
    /// </summary>
    /// <param name="movX">the vector to move on X axis</param>
    /// <param name="movZ"> the vector to move on Z axis</param>
    public void MovePlayer(float movX, float movZ)
    {
        //Normalize input
        if (movX > 0.1f) movX = 1; else if (movX < -0.1f) movX = -1; else movX = 0;
        if (movZ > 0.1f) movZ = 1; else if (movZ < -0.1f) movZ = -1; else movZ = 0;
        if (!isMovementEnabled) return;
        //Save  input    
        currentMovement = new Vector3(movX,0f, movZ);
        // Check for max speed and disable acceleration if > max
        float pythagoras = ((rigidBody.velocity.x * rigidBody.velocity.x) + (rigidBody.velocity.z * rigidBody.velocity.z));        
        if (pythagoras < (maxSpeed * maxSpeed*boostFactor))
        {
            //Move Player   
            rigidBody.AddForce(currentMovement * accelerationFactor * boostFactor);
        }
        Vector2 abs = new Vector2(Mathf.Abs(movX), Mathf.Abs(movZ));
        isIddle = false; //Default not iddle
        if (abs.x<0.5f)  // Slow Factor in X
        {            
            rigidBody.velocity = new Vector3(rigidBody.velocity.x * slowFactor,0f, rigidBody.velocity.z);
            if (abs.y<0.5f) isIddle = true; //Is iddle            
        }              
        if (abs.y < 0.5f) // Slow Factor in Z
        {
            rigidBody.velocity = new Vector3(rigidBody.velocity.x ,0f, rigidBody.velocity.z * slowFactor);
        }
        
        //Sets the avatar on the rotation rotating the pivot and setting the animation
        if(InputManager.instance.isUsingController||isMelee)
            SetAvatarRotation(movX, movZ); //Only executes if is using controller or if the avatar state is melee

    }

    /// <summary>
    /// Rotate the Pivot of in the direction of the movement
    /// Also sets the animation in the correct direction
    /// </summary>
    /// <param name="movX">X axis between -1 to 1</param>
    /// <param name="movZ">Z axis between -1 to 1</param>
    private void SetAvatarRotation(float movX,float movZ)
    {
        //Set Rotation on animation
        SetMovementAnimation(movX, movZ);
        //Returns in case that is using controller and is in dead angle
        if ((InputManager.instance.isUsingController||isMelee) && (Mathf.Abs(movX) < 0.1 && Mathf.Abs(movZ) < 0.1))
            return;
        
        //Rotates the pivot
        float rotationAngle = Vector2.SignedAngle(new Vector2(movZ * -1, movX), Vector2.right);
        //If using controller and is ranged        
        if (InputManager.instance.isUsingController && !isMelee)
        {            
            aimAssistManager.SetAssistValue(true);
            rotationAngle = aimAssistManager.GiveAssistAngle(rotationAngle);
        }
        else
        {
            aimAssistManager.SetAssistValue(false);
        }
        pivotTransform.rotation = Quaternion.Euler(0f,rotationAngle,0f);
    }
    /// <summary>
    /// Set the animation for the avatar
    /// </summary>
    /// <param name="movX">magnitude on x between -1, 1</param>
    /// <param name="movZ">magnitude on z between -1, 1</param>
    private void SetMovementAnimation(float movX,float movZ)
    { 
        if (movX > 0.1f)
        {
            if (movZ > 0.1f) //1,1 Back-Right
            {
                avatarAnimationManager.SetMovementAnimation(AvatarAnimationManager.Orientations.Back_Right,isIddle);
            }
            else if (movZ < -0.1f)//1,-1 Front-Right
            {
                avatarAnimationManager.SetMovementAnimation(AvatarAnimationManager.Orientations.Front_Right, isIddle);
            }
            else //1,0 Right
            {
                avatarAnimationManager.SetMovementAnimation(AvatarAnimationManager.Orientations.Right, isIddle);
            }
        }
        else if (movX < -0.1f)
        {
            if (movZ > 0.1f) //-1,1 Left-Back
            {
                avatarAnimationManager.SetMovementAnimation(AvatarAnimationManager.Orientations.Back_Left, isIddle);
            }
            else if (movZ < -0.1f) //-1,-1 Left-Front
            {
                avatarAnimationManager.SetMovementAnimation(AvatarAnimationManager.Orientations.Front_Left, isIddle);
            }
            else //-1,0 Left
            {
                avatarAnimationManager.SetMovementAnimation(AvatarAnimationManager.Orientations.Left, isIddle);
            }
        }
        else
        {
            if (movZ > 0.1f) //0,1 Back
            {
                avatarAnimationManager.SetMovementAnimation(AvatarAnimationManager.Orientations.Back, isIddle);
            }
            else if (movZ < -0.1f)//0,-1 Front
            {
                avatarAnimationManager.SetMovementAnimation(AvatarAnimationManager.Orientations.Front, isIddle);
            }
            else //0,0 No new rotation
            {
                avatarAnimationManager.SetMovementAnimation(avatarAnimationManager.currentOrientation, isIddle);
            }
        }
    }
    /// <summary>
    /// Slows the Avatar movement on a given factor
    /// </summary>
    /// <param name="factor">the factor to be used to slow the avatar</param>
    public void SlowAvatarMovement(float factor)
    {
        rigidBody.velocity = new Vector3(rigidBody.velocity.x * factor, 0f, rigidBody.velocity.z * factor);
    }

    /// <summary>
    /// Changes the value of the player movement params
    /// It's used on each avatar state swap
    /// </summary>
    /// <param name="pAccelerationFactor">The new acceleration value</param>
    /// <param name="pMaxSpeed">The new MAX speed for the player</param>
    /// <param name="pSlowFactor">The new slowing factor when the player stops accelerating</param>
    /// <param name="pMass">The new mass for the player's rigidbody</param>
    /// <param name="pLinearDrag">The new Linear Drag for the player's rigidbody</param>
    /// <param name="pIsMelee">Is the avatar on a melee state</param>
    public void ChangePlayerMovementValues(float pAccelerationFactor,float pMaxSpeed,float pSlowFactor,float pMass,float pLinearDrag, bool pIsMelee)
    {
        isMelee= pIsMelee;
        boostFactor = 1;
        accelerationFactor = pAccelerationFactor;
        maxSpeed = pMaxSpeed;
        slowFactor= pSlowFactor;
        mass= pMass;
        if (rigidBody == null) rigidBody = GetComponent<Rigidbody>();
        rigidBody.mass = mass;
        linearDrag = pLinearDrag;
        rigidBody.drag = linearDrag;
        //aimAssistManager.SetNextTarget();
        //Set  Aim assist value
        if (InputManager.instance.isUsingController && !isMelee)
            aimAssistManager.SetAssistValue(true);
        else aimAssistManager.SetAssistValue(false);
    }
    /// <summary>
    /// Method that manages inputs from mouse/ right joystick
    /// </summary>
    /// <param name="movX">the x valor from input from 0 to 1</param>
    /// <param name="movY">the Y valor from input from 0 to 1</param>
    private void ManageRJoystickInput(float movX,float movY)
    {
        //Executes only on pc controller
        if (!InputManager.instance.isUsingController&&!isMelee)
            SetAvatarRotation(movX, movY);
    }
    //[[[[[[[[[[[[[[[  DELEGATES  ]]]]]]]]]]]]]]] 
    //Unsuscribes from delegates before being destroyed 
    void OnDestroy()
    {
        // We unsubscribe to avoid memory leaks
        UnSubscribeInputs();
    }

    //Suscribes the Input Manager delegates
    void SubscribeInputs()
    {
        InputManager.instance.OnLJoystickMovementEvent += MovePlayer;
        InputManager.instance.OnRJoystickMovementEvent += ManageRJoystickInput;
    }
    //Unsuscribes the Input Manager delegates
    void UnSubscribeInputs()
    {
        InputManager.instance.OnLJoystickMovementEvent -= MovePlayer;
        InputManager.instance.OnRJoystickMovementEvent -= ManageRJoystickInput;
    }

    //----------- AI for Cutscenes
    /// <summary>
    /// Method that sets the information required to move the player automatically in a cutscene
    /// </summary>
    /// <param name="newPosition"></param>
    /// <param name="clipDuration"></param>
    public void CutsceneMovePlayer(Vector3 newPosition, float clipDuration)
    {
        float distance = Vector3.Distance(transform.position, newPosition);

        //Debug.Log("Vamo a moverlo a: "+ newPosition.ToString());

        float speed = distance / clipDuration;

        StartCoroutine(AutomaticMovePlayer(newPosition,speed*10));
    }

    /// <summary>
    /// Metho that moves the player in a cutscene
    /// </summary>
    /// <param name="destination"></param>
    /// <param name="speed"></param>
    /// <returns></returns>
    IEnumerator AutomaticMovePlayer(Vector3 destination, float speed)
    {
        transform.position = Vector3.MoveTowards(transform.position, destination, speed *Time.deltaTime);
       //transform.position = Vector3.Slerp(transform.position, destination, 1f* Time.deltaTime);
        if(transform.position != destination)
        {
            yield return new WaitForSeconds(Time.deltaTime/2);
            StartCoroutine(AutomaticMovePlayer(destination, speed));
        }
    }

}
