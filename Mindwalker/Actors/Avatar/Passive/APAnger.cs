﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class inhereits from Avatar Passive class
/// It's the one that deals with the passive behaviour of the anger avatar state
/// </summary>
public class APAnger : AvatarPassive
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //CONSTANTS
    //~~~~~~~~~~~~~~~~~~~~~~~~ 
    //Available Frenzy levels (Lvl 1 min, lvl3 max)
    private enum FrenzyLevels { lvl0, lvl1, lvl2, lvl3 };
    //Aura animation constants 0 is no aura, 1 is lvl1,2 lvl2, 3 lvl3
    private string[] auraTriggers = new string[] { "0FrenzyTrigger", "1FrenzyTrigger", "2FrenzyTrigger", "3FrenzyTrigger" };
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~ 
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //The active ability manager of the avatar state
    private AAAnger aAAnger;
    //The current FrenzyLevel
    private FrenzyLevels frenzyLevel;
    //Hit count (either given or taken)
    private int hitCount;
    //Time left before reseting the avatar state
    private float frenzyTimeLeft;
    //Amount of max time  before frenzy resets
    private float maxFrenzyTime;
    //Amount of hits needed on each lvl (NOTE: This must be on same order as FrenzyLevels)
    private int[] frenzyHitsNeeded;
    //Damage amount multiplier of each frenzylvl
    private float[] damageAmountMultipliers;
    //Damage speed multiplier of each frenzylvl
    private float[] damageSpeedMultipliers;
    //Self damage per second of each frenzylvl
    private float[] selfDamagesPerSecond;
    //Life leech of each frenzy lvl
    private float[] lifeLeechAmounts;
    //dash multiplier force of each frenzylvl
    private float[] dashMultiplier;
    //Animator of the aura for frenzy
    private Animator auraAnimator;

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    protected override void Awake()
    {
        base.Awake();
        //Set Avatar state
        avatarState = AvatarManager.AvatarStates.Anger;
        //Get Component For the active ability manager
        aAAnger = GetComponent<AAAnger>();
        //Invokes the repeating behaviour for frenzy
        InvokeRepeating("FrenzyTimer", 0f, 1f);
        //Get aura animator
        auraAnimator = GameObject.FindGameObjectWithTag("Aura").GetComponent<Animator>();

    }
    /// <summary>
    /// Cleans the values of this script
    /// </summary>
    public override void CleanAvatarState()
    {
        //Enable the script
        isEnabled = true;
        //Hit count and time left set to 0
        hitCount = 0;
        frenzyTimeLeft = 0;
        //Reset frenzy
        SetFrenzyLvl(FrenzyLevels.lvl1);
    }

    /// <summary>
    /// Decreases the time Left for frenzy each second
    /// If it reaches 0 resets the frenzy lvl
    /// Also applies damage on the player depending on the state
    /// this is a invoke repeating method
    /// </summary>
    private void FrenzyTimer()
    {
        if (!isEnabled)
            return;
        //Take self damage per second
        //hPManager.TakeDamage(selfDamagesPerSecond[(int)frenzyLevel]);
        frenzyTimeLeft--;
        // frenzy lvl is already 0 or time left is positive return
        if (frenzyLevel == FrenzyLevels.lvl1 || frenzyTimeLeft > 0)
            return;
        SetFrenzyLvl(FrenzyLevels.lvl1);
    }
    /// <summary>
    /// Increases frenzy hit points and reset frenzy timer
    /// if it reaches enough points increases frenzy level
    /// </summary>
    public void IncreaseFrenzyHitPoints()
    {
        hitCount++;
        frenzyTimeLeft = maxFrenzyTime; //Reset time
        //Has enough points to increase frenzy?
        if (frenzyHitsNeeded[(int)frenzyLevel] == hitCount)
            IncreaseFrenzyLvl();
    }
    /// <summary>
    /// Increase the frenzy lvl and its values
    /// </summary>
    private void IncreaseFrenzyLvl()
    {
        switch (frenzyLevel)
        {
            case FrenzyLevels.lvl1:
                SetFrenzyLvl(FrenzyLevels.lvl2);
                break;
            case FrenzyLevels.lvl2:
                SetFrenzyLvl(FrenzyLevels.lvl3);
                break;
            // Already at max lvl, no need to do anything
            case FrenzyLevels.lvl3:
                break;
        }
    }

    /// <summary>
    /// Increase the frenzy lvl and the values    
    /// </summary>
    /// <param name="newFrenzyLevel">The new Id of the Frenzy lvl to set</param>
    private void SetFrenzyLvl(FrenzyLevels newFrenzyLevel)
    {
        if (newFrenzyLevel == frenzyLevel)
            return;
        //Aura animation
        ManageAuraAnimation(newFrenzyLevel);
        //Set new frenzy level
        frenzyLevel = newFrenzyLevel;
        //damageAmountMultipliers 
        aAAnger.frenzyDamageAmountMultiplier = damageAmountMultipliers[(int)frenzyLevel];
        //damageSpeedMultipliers        
        aAAnger.frenzyDamageSpeedMultiplier = damageSpeedMultipliers[(int)frenzyLevel];
        //lifeLeechAmounts 
        aAAnger.frenzyLeechAmount = lifeLeechAmounts[(int)frenzyLevel];
        //precisionAngles
        aAAnger.frenzyDashMultiplier = dashMultiplier[(int)frenzyLevel];

    }

    /// <summary>
    /// Sets aura animation given a frenzy level    
    /// </summary>
    /// <param name="newFrenzyLevel">The new frenzy level</param>
    private void ManageAuraAnimation(FrenzyLevels newFrenzyLevel)
    {
        //Turn off previous FX
        auraAnimator.SetBool(auraTriggers[(int)frenzyLevel], false);
        //Swap
        auraAnimator.SetBool(auraTriggers[(int)newFrenzyLevel], true);
    }
    /// <summary>
    /// Disables the passive ability 
    /// </summary>
    public override void DisablePassive()
    {
        //Disable active
        isEnabled = false;

        //Turn off previous FX
        auraAnimator.SetBool(auraTriggers[(int)frenzyLevel], false);
        //current lvl to 0
        frenzyLevel = FrenzyLevels.lvl0;
        //Swap to no aura
        auraAnimator.SetBool(auraTriggers[0], true);
    }

    /// <summary>
    /// Sets the values of the variables of this script   
    /// </summary>
    /// <param name="pMaxFrenzyTime">Amount of max time  before frenzy resets</param>
    /// <param name="pfrenzyHitsNeeded">Amount of hits needed on each lvl(NOTE: This must be on same order as FrenzyLevels)</param>
    /// <param name="pdamageAmountMultipliers">Damage amount multiplier of each frenzylvl</param>
    /// <param name="pdamageSpeedMultipliers">Damage speed multiplier of each frenzylvl</param>
    /// <param name="pselfDamagesPerSecond">Self damage per second of each frenzylvl</param>
    /// <param name="plifeLeechAmounts">Life leech of each frenzy lvl</param>
    /// <param name="pdashMultiplier">dash multiplier force of each frenzylvl</param>
    public void SetValues(float pMaxFrenzyTime, int[] pfrenzyHitsNeeded, float[] pdamageAmountMultipliers, float[] pdamageSpeedMultipliers, float[] pselfDamagesPerSecond, float[] plifeLeechAmounts, float[] pdashMultiplier)
    {
        maxFrenzyTime = pMaxFrenzyTime;
        frenzyHitsNeeded = pfrenzyHitsNeeded;
        damageAmountMultipliers = pdamageAmountMultipliers;
        damageSpeedMultipliers = pdamageSpeedMultipliers;
        selfDamagesPerSecond = pselfDamagesPerSecond;
        lifeLeechAmounts = plifeLeechAmounts;
        dashMultiplier = pdashMultiplier;
    }
}
