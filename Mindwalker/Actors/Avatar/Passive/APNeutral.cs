﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class inhereits from Avatar Passive class
/// It's the one that deals with the passive behaviour of the neutral avatar state
/// </summary>
public class APNeutral : AvatarPassive
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //CONSTANTS
    //~~~~~~~~~~~~~~~~~~~~~~~~ 

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~ 
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    protected override void Awake()
    {
        base.Awake();
        //Set Avatar state
        avatarState = AvatarManager.AvatarStates.Neutral;
    }
    /// <summary>
    /// Cleans the values of this script
    /// </summary>
    public override void CleanAvatarState()
    {
        //TODO
        //Enable the script
        isEnabled = true;
    }
    /// <summary>
    /// Disables this avatar passive
    /// </summary>
    public override void DisablePassive()
    {
       //TODO
    }
    /// <summary>
    /// Sets the values for this avatar passive
    /// </summary>
    public void SetValues()
    {
        //TODO
    }
}