﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class is the interface used for all avatar passive habilities
/// The classes that inhereit from this class start with AP
/// The Avatar Manager Uses this script and sets the values for different avatar states
/// </summary>
public abstract class AvatarPassive : MonoBehaviour
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    //ID to identify the Avatar State of the Passive power
    public AvatarManager.AvatarStates avatarState;
    //Is the Avatar Active Power Enabled?
    public bool isEnabled { get; set; }
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE & PROTECTED VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //HP manager of the avatar
    protected HPManager hPManager;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //ABSTRACT METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Cleans the state before reActivating the Script by reseting it's values
    public abstract void CleanAvatarState();
    //Behaviour for disable
    public abstract void DisablePassive();
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Diable Scripts to avoid conflicts
    protected virtual void Awake()
    {        
        hPManager = GetComponent<HPManager>();
        isEnabled = false;     
    }
    
}
