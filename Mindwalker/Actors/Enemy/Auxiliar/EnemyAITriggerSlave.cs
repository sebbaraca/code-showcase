﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This script helps registering and unregistering when the player enters the triger area of action of the AI
/// </summary>
public class EnemyAITriggerSlave : MonoBehaviour
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    public bool isReachTrigger;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    private EnemyAIGeneral enemyAIGeneral;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    private void Awake()
    {
        enemyAIGeneral = GetComponentInParent<EnemyAIGeneral>();
    }

    private void OnTriggerEnter(Collider other)
    {
        //Detect Player and register it
        if(other.tag=="Player")
        {
            enemyAIGeneral.CheckInAvatarIddleArea(false,isReachTrigger);
        }
        //Detect Hazard and register it
        else if(other.tag=="Hazard"&&isReachTrigger)
        {
            enemyAIGeneral.CheckInAvoidableObject(other.gameObject, true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //Detect Player and unregister it
        if (other.tag == "Player")
        {
            enemyAIGeneral.CheckInAvatarIddleArea(true,isReachTrigger);
        }
        //Detect Hazard and unregister it
        else if (other.tag == "Hazard"&&isReachTrigger)
        {
            enemyAIGeneral.CheckInAvoidableObject(other.gameObject, false);
        }
    }
}
