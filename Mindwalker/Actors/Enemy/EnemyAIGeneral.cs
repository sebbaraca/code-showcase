﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This script serves as Interface for all enemy AI scripts
/// </summary>
public abstract class EnemyAIGeneral : MonoBehaviour
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //CONSTANTS 
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    public EnemyManager.EnemyStates enemyState;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    //Bool that determines if the AI is in an iddle state
    protected bool isIddle;
    //Bool that determines if the AI is in use
    protected bool isAIActive;
    //Bool that indicates if the avatar is within reach area
    protected bool isInReachArea;
    //Movement of the enemy
    protected EnemyMovement enemyMovement;
    //Player Game Object
    protected GameObject avatarGameObject;
    //List of Game objects to avoid
    protected List<GameObject> objectsToAvoid;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //ABSTRACT METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    /// Examines and Excecutes the AI behaviour of the enemy on a given state
    /// </summary>
    public abstract void ExcecuteAIBehaviour();
    /// <summary>
    /// Starts the AI behaviour
    /// </summary>
    public abstract void StartAIBehaviour();
    /// <summary>
    /// Stops the AI behaviour 
    /// </summary>
    public abstract void StopAIBehaviour();

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~ 
    protected virtual void Awake()
    {
        enemyMovement = GetComponent<EnemyMovement>();
        objectsToAvoid = new List<GameObject>();
        isIddle = true;
    }
    private void FixedUpdate()
    {
        if (isAIActive)
        {
            //Executes the custom AI behaviour of each enemy
            ExcecuteAIBehaviour();
        }        
    }
    /// <summary>
    /// Registers when the avatar enters or exits an area 
    /// </summary>
    /// <param name="isCheckIn">Is the avatar checkin in or out</param>
    /// <param name="isReachArea">Is the value indicating reach area or iddle area</param>
    public void CheckInAvatarIddleArea(bool isCheckIn,bool isReachArea)
    {
        if (!isReachArea) { isIddle = isCheckIn; }
        else { isInReachArea = !isCheckIn; }
    }
    /// <summary>
    /// Checks in or out a game object that wants to be avoided
    /// </summary>
    /// <param name="theObject">The object to be checked in/out</param>
    /// <param name="isCheckIn">Is the object checkin in or out</param>
    public void CheckInAvoidableObject(GameObject theObject,bool isCheckIn)
    {
        if (isCheckIn) { objectsToAvoid.Add(theObject); }
        else { objectsToAvoid.Remove(theObject); }
    }

}
