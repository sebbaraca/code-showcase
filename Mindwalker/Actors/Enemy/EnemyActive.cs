﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class is the basic structuer for active habilities of enemies
/// Classes that inhereit from this class start with EA
/// </summary>
public abstract class EnemyActive : MonoBehaviour
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    //ID to identify the enemy State of the Active power
    public EnemyManager.EnemyStates enemyState;
    //Is the enemy Active Power Enabled?
    public bool isEnabled { get; set; }
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE-PROTECTED VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Cooldown time for the active movement
    protected float movementCoolDownTime;
    //Cooldown time for the active movement
    protected float powerCoolDownTime;
    //Is on cooldown for the activeMovement?
    protected bool isOnActiveMovementCoolDown;
    //Is on cooldown for the activePower?
    protected bool isOnActivePowerCoolDown;
    //List of objects on range that can be attacked
    protected List<GameObject> objectsOnConeRange;
    //List of objects on range that can be attacked
    protected List<GameObject> objectsOnCircleRange;
    //HP manager of the enemy
    protected HPManager hPManager;
    //Animation manager
    protected EnemyAnimationManager enemyAnimationManager;

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //ABSTRACT METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~       
    //Cleans the state before reActivating the Script by reseting it's values
    public abstract void CleanActiveAbility();
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~

    protected virtual void Awake()
    {
        //Diable Scripts to avoid conflicts        
        hPManager = GetComponent<HPManager>();
        isEnabled = false;
        isOnActiveMovementCoolDown = false;
        objectsOnConeRange = new List<GameObject>();
        objectsOnCircleRange = new List<GameObject>();
        enemyAnimationManager = GetComponent<EnemyAnimationManager>();
    }

    //Waits for the cooldown time to re enable the cooldown flag for  active movement
    protected IEnumerator InitiateActiveMovementCoolDown(float coolDownTime)
    {
        yield return new WaitForSeconds(coolDownTime);        
        isOnActiveMovementCoolDown = false;
    }

    //Waits for the cooldown time to re enable the cooldown flag for  active power
    protected IEnumerator InitiateActivePowerCoolDown(float coolDownTime)
    {        
        yield return new WaitForSeconds(coolDownTime);
        isOnActivePowerCoolDown = false;
    }

    /// <summary>
    /// Registers the Game object with the enemy tag that entered on the hit 
    /// collider zone of the enemy.This will be used to hit all those who
    /// are inside when the attack is executed
    /// </summary>
    /// <param name="collision">collision the game object that entered the area</param>
    /// <param name="isCone">indicates if this object is from the cone area or the circle area</param>
    public void RegisterObject(GameObject collision, bool isCone)
    {
        if (isCone) objectsOnConeRange.Add(collision);
        else objectsOnCircleRange.Add(collision);
    }

    /// <summary>
    /// Un Registers the Game object with the enemy tag that
    /// entered on the hit collider zone of the player
    /// </summary>
    /// <param name="collision">the game object that entered the area</param>
    /// <param name="isCone">indicates if this object is from the cone area or the circle area</param>
    public void UnRegisterObject(GameObject collision, bool isCone)
    {
        if (isCone) objectsOnConeRange.Remove(collision);
        else objectsOnCircleRange.Remove(collision);
    }
}
