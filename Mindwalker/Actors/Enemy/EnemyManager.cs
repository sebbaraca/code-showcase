﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class is the base structure used by enemies
/// Classes that inhereit from this start with EM 
/// They manage the logic that activates the profiles on movement, changes the active powers
/// and passives. Also manages health profiles.
/// </summary>
public abstract class EnemyManager : MonoBehaviour
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //CONSTANTS 
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //The list of Enemy states to be used on the game. This affects all the enemies.
    //Note that you must also add or remove this info on the same order as the xml of each enemy
    public enum EnemyStates
    { Neutral, Anger, Boredom }

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~      
    //Current state
    protected EnemyStates enemyState;
    //List of Enemies Active scripts
    protected Dictionary<EnemyStates, EnemyActive> enemiesActives;
    //List of Enemies AI scripts
    protected Dictionary<EnemyStates, EnemyAIGeneral> enemiesAIs;
    //Current active behaviour
    protected EnemyActive enemyActive;
    //Curren AI active
    protected EnemyAIGeneral currentAI;
    //HP Manager of the Enemy
    protected HPManager hPManager;
    //Enemy movement
    protected EnemyMovement enemyMovement;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //ABSTRACT METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~ 
    /// <summary>
    /// Loads the avatar states data into the game
    /// Data is loaded into the snapshots list
    /// </summary>
    protected abstract void LoadEnemyData();
    
    /// <summary>
    /// Gets the specific script references for the given enemy
    /// </summary>
    protected abstract void GetSpecificScriptReferences();
    /// <summary>
    /// Excecutes especific death behaviour of the enemy
    /// </summary>
    public abstract void DeathBehaviour();
    
    /// <summary>
    /// Swaps the avatar state. 
    /// This implies changing movement, active, passive, health
    /// </summary>
    /// <param name="pEnemyState">The Id of the new state to be applied</param>
    /// <param name="isLoadingCheckpoint">is the enemy state swaping from a loading data?</param>
    public abstract void SwapEnemyState(EnemyStates pEnemyState, bool isLoadingCheckpoint);
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~   
    // Use this for initialization
    void Start()
    {
        //Get Script Components
        GetGeneralScriptReferences();
        //Load Parameters
        LoadEnemyData();        
    }
    /// <summary>
    /// Initializes all script references of the script
    /// </summary>
    protected void GetGeneralScriptReferences()
    {
        //Get HP manager 
        hPManager = GetComponent<HPManager>();
        //Get movement
        enemyMovement = GetComponent<EnemyMovement>();
        enemyMovement.InitializeMovement();
        //Get Enemy Active Powers
        enemiesActives = new Dictionary<EnemyStates, EnemyActive>();        
        foreach (var item in GetComponents<EnemyActive>())
        { enemiesActives.Add(item.enemyState, item); }
        //Get Enemy AI Scripts
        enemiesAIs = new Dictionary<EnemyStates, EnemyAIGeneral>();
        foreach (var item in GetComponents<EnemyAIGeneral>())
        { enemiesAIs.Add(item.enemyState, item); }
        //Get Specific references for the script
        GetSpecificScriptReferences();
    }

    /// <summary>
    /// Stuns the enemy for a given duration, disabling movement and actions
    /// </summary>
    /// <param name="duration">The duration of the stun</param>    
    public IEnumerator StunForDuration(float duration)
    {
        enemyActive.isEnabled = false;
        StartCoroutine(enemyMovement.DisableMovementTemporal(duration));
        yield return new WaitForSeconds(duration);
        enemyActive.isEnabled = true;
    }
    /// <summary>
    /// Spawns an object on the enemy position
    /// </summary>
    /// <param name="theObj">The object to be spawned</param>
    protected void SpawnObjectOnEnemyPosition(GameObject theObj)
    {
        //Spawn on enemy position
        GameObject.Instantiate(theObj, transform.position, transform.rotation);
    }

    /// <summary>
    /// Enables or disables the Avatar behaviour
    /// Used for cinematics n stuff
    /// </summary>
    /// <param name="isEnabled">Is the avatar enabled?</param>
    public void EnableEnemyBehaviour(bool isEnabled)
    {
        enemyActive.isEnabled = isEnabled;
        enemyMovement.SetMovementState(isEnabled);
    }
}
