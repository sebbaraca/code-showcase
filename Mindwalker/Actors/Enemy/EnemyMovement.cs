﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class is the logic for the Enemy movement
/// The Enemy Manager Uses this script and sets the values for different enemies
/// </summary>
public class EnemyMovement : MonoBehaviour
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    //Boost factor used as multiplier of the base speed
    public float boostFactor { get; set; }
    //The transform of the Pivot of the things that must rotate with the character
    public Transform pivotTransform;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Movement Speed of the enemy (acceleration)
    private float accelerationFactor;
    //Max speed that the avatar can achieve on x or y
    private float maxSpeed;
    //Slow factor when the avatar changes direction or stops accelerating
    private float slowFactor;
    //Mass of the player's rigid body
    private float mass;
    //Linear drag of the rigid body
    private float linearDrag;
    //Rigid body of the avatar
    private Rigidbody rigidBody;
    //currentMovement direction from the input
    private Vector3 currentMovement;
    //Is the movement enabled?
    private bool isMovementEnabled;
    //Animation Manager for the avatar
    private EnemyAnimationManager enemyAnimationManager;
    //Is the avatar in iddle state?
    private bool isIddle;

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~


    /// <summary>
    /// Initializes all the variables of this script
    /// </summary>
    public void InitializeMovement()
    {        
        boostFactor = 1;
        enemyAnimationManager = GetComponent<EnemyAnimationManager>();
        isIddle = true;
        rigidBody = GetComponent<Rigidbody>();
        isMovementEnabled = true;
        
    }
    /// <summary>
    /// Disables the movement for a given period     
    /// </summary>
    /// <param name="coolDown">the time that the movement will be disabled</param>    
    public IEnumerator DisableMovementTemporal(float coolDown)
    {
        isMovementEnabled = false;
        yield return new WaitForSeconds(coolDown);
        isMovementEnabled = true;        
    }

    /// <summary>
    /// Enables or disables movement 
    /// </summary>
    /// <param name="isEnabled">Is the movement enabled?</param>
    public void SetMovementState(bool isEnabled)
    {
        isMovementEnabled = isEnabled;
    }

    /// <summary>
    /// Moves the enemy on the X,Y direction given
    /// </summary>
    /// <param name="movX">the vector to move on X axis</param>
    /// <param name="movZ"> the vector to move on Z axis</param>
    public void MoveEnemy(float movX, float movZ)
    {        
        //Normalize input
        if (movX > 0.1f) movX = 1; else if (movX < -0.1f) movX = -1; else movX = 0;
        if (movZ > 0.1f) movZ = 1; else if (movZ < -0.1f) movZ = -1; else movZ = 0;
        if (!isMovementEnabled) return;
        //Save  input    
        currentMovement = new Vector3(movX, 0f, movZ);
        // Check for max speed and disable acceleration if > max
        float pythagoras = ((rigidBody.velocity.x * rigidBody.velocity.x) + (rigidBody.velocity.z * rigidBody.velocity.z));
        if (pythagoras < (maxSpeed * maxSpeed * boostFactor))
        {
            //Move Enemy   
            rigidBody.AddForce(currentMovement * accelerationFactor * boostFactor);
        }

        if (Mathf.Abs(movX) < 0.5f)  // Slow Factor in X
        {
            rigidBody.velocity = new Vector3(rigidBody.velocity.x * slowFactor, 0f, rigidBody.velocity.z);
        }
        if (Mathf.Abs(movZ) < 0.5f) // Slow Factor in Z
        {
            rigidBody.velocity = new Vector3(rigidBody.velocity.x, 0f, rigidBody.velocity.z * slowFactor);
        }
        //Rotate the enemy
        //SetEnemyRotation(movX, movZ); 

    }

    /// <summary>
    /// Rotate the Pivot of in the direction of the movement
    ///TODO Also sets the animation in the correct direction
    /// </summary>
    /// <param name="movX">X axis between -1 to 1</param>
    /// <param name="movZ">Z axis between -1 to 1</param>
    public void SetEnemyRotation(float movX, float movZ)
    {
        //Returns in case that is in dead angle
        if ((Mathf.Abs(movX) < 0.1 && Mathf.Abs(movZ) < 0.1))
            return;
        //Rotates the pivot
        float rotationAngle = Vector2.SignedAngle(new Vector2(movZ * -1, movX), Vector2.right);       
        pivotTransform.rotation = Quaternion.Euler(0f, rotationAngle, 0f);
        //Set Rotation on animation
        SetMovementAnimation(movX, movZ);
    }

    /// <summary>
    /// Set the animation for the enemy
    /// </summary>
    /// <param name="movX">magnitude on x between -1, 1</param>
    /// <param name="movZ">magnitude on z between -1, 1</param>
    private void SetMovementAnimation(float movX, float movZ)
    {
        if (movX > 0.1f)
        {
            if (movZ > 0.1f) //1,1 Back-Right
            {
                enemyAnimationManager.SetMovementAnimation(EnemyAnimationManager.Orientations.Back_Right, isIddle);
            }
            else if (movZ < -0.1f)//1,-1 Front-Right
            {
                enemyAnimationManager.SetMovementAnimation(EnemyAnimationManager.Orientations.Front_Right, isIddle);
            }
            else //1,0 Right
            {
                enemyAnimationManager.SetMovementAnimation(EnemyAnimationManager.Orientations.Right, isIddle);
            }
        }
        else if (movX < -0.1f)
        {
            if (movZ > 0.1f) //-1,1 Left-Back
            {
                enemyAnimationManager.SetMovementAnimation(EnemyAnimationManager.Orientations.Back_Left, isIddle);
            }
            else if (movZ < -0.1f) //-1,-1 Left-Front
            {
                enemyAnimationManager.SetMovementAnimation(EnemyAnimationManager.Orientations.Front_Left, isIddle);
            }
            else //-1,0 Left
            {
                enemyAnimationManager.SetMovementAnimation(EnemyAnimationManager.Orientations.Left, isIddle);
            }
        }
        else
        {
            if (movZ > 0.1f) //0,1 Back
            {
                enemyAnimationManager.SetMovementAnimation(EnemyAnimationManager.Orientations.Back, isIddle);
            }
            else if (movZ < -0.1f)//0,-1 Front
            {
                enemyAnimationManager.SetMovementAnimation(EnemyAnimationManager.Orientations.Front, isIddle);
            }
            else //0,0 No new rotation
            {
                //enemyAnimationManager.SetMovementAnimation(EnemyAnimationManager.Orientations.Back_Right, isIddle);
            }
        }
    }

    /// <summary>
    /// Slows the Enemy movement on a given factor
    /// </summary>
    /// <param name="factor">the factor to be used to slow the avatar</param>
    public void SlowEnemyMovement(float factor)
    {
        rigidBody.velocity = new Vector3(rigidBody.velocity.x * factor, 0f, rigidBody.velocity.z * factor);
    }


    /// <summary>
    /// Changes the value of the enemy movement params
    /// It's used on each avatar state swap
    /// </summary>
    /// <param name="pAccelerationFactor">The new acceleration value</param>
    /// <param name="pMaxSpeed">The new MAX speed for the player</param>
    /// <param name="pSlowFactor">The new slowing factor when the player stops accelerating</param>
    /// <param name="pMass">The new mass for the player's rigidbody</param>
    /// <param name="pLinearDrag">The new Linear Drag for the player's rigidbody</param>    
    public void ChangeEnemyMovementValues(float pAccelerationFactor, float pMaxSpeed, float pSlowFactor, float pMass, float pLinearDrag)
    {     
        boostFactor = 1;
        accelerationFactor = pAccelerationFactor;
        maxSpeed = pMaxSpeed;
        slowFactor = pSlowFactor;
        mass = pMass;
        if (rigidBody == null) rigidBody = GetComponent<Rigidbody>();
        rigidBody.mass = mass;
        linearDrag = pLinearDrag;
        rigidBody.drag = linearDrag;       
    }
   
}
