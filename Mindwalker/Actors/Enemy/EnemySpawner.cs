﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class is in charge of spawning 
/// and tracking the enemies spawned
/// </summary>
public class EnemySpawner : Singleton<EnemySpawner>
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //CONSTANTS 
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //The different enemy types that exist
    public enum EnemyTypes
    { Moira, ShieldMaiden }
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES 
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //******DELEGATES*****
    // This delegate allows to know when all the enemies are killed
    public delegate void OnAllEnemiesKilled(bool isWipe);
    // Event that notifies that all enemies were killed
    public static OnAllEnemiesKilled OnAllEnemiesKilledEvent;
    //***********
    //Prefabs for the enemies (Has to be in same order as enemyTypes)
    public GameObject[] enemiesPrefabs;
    //Max number of enemies
    public int maxNumberOfEnemies = 6;

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES 
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //List of Spawned Mobs
    private List<GameObject> spawnedMobs;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    private void Start()
    {
        spawnedMobs = new List<GameObject>();
    }

    /// <summary>
    /// Spawns an specific type of enemy in the given position and rotation
    /// </summary>
    /// <param name="position">The position in world coordinates</param>
    /// <param name="rotation">The rotation of the mob</param>
    /// <param name="type">The type of the enemy to be spawned</param>
    public void SpawnSpecificEnemy(Vector3 position, Quaternion rotation, EnemyTypes type)
    {
        if (spawnedMobs.Count < maxNumberOfEnemies)
            spawnedMobs.Add(GameObject.Instantiate(enemiesPrefabs[(int)type], position, rotation));
    }

    /// <summary>
    /// Spawns a random type of enemy in the given position and rotation
    /// </summary>
    /// <param name="position">The position in world coordinates</param>
    /// <param name="rotation">The rotation of the mob</param>    
    public void SpawnRandomEnemy(Vector3 position, Quaternion rotation)
    {
        if (spawnedMobs.Count < maxNumberOfEnemies)
            spawnedMobs.Add(GameObject.Instantiate(enemiesPrefabs[Random.Range(0, enemiesPrefabs.Length)], position, rotation));
    }
    /// <summary>
    /// Starts or stops the AI of the spawned enemies
    /// </summary>
    /// <param name="isAIEnabled">Is the AI enabled?</param>
    public void SetAIValue(bool isAIEnabled)
    {
        foreach (var item in spawnedMobs)
        {
            if (isAIEnabled) item.GetComponent<EnemyAIGeneral>().StartAIBehaviour();
            else item.GetComponent<EnemyAIGeneral>().StopAIBehaviour();
        }
    }
    /// <summary>
    /// Destroys all enemies spawned by this enemy spawner
    /// </summary>
    public void DestroyAllEnemies(bool isWipe)
    {
        while (spawnedMobs.Count > 0)
        {
            //Destroy Game Object
            GameObject.Destroy(spawnedMobs[0]);
            //Remove from list
            spawnedMobs.RemoveAt(0);
        }
        //Notify delegates that all enemies where cleared
        if (OnAllEnemiesKilledEvent != null)
        {
            OnAllEnemiesKilledEvent(isWipe);
        }

        //Reset auto aim
        GameObject.FindGameObjectWithTag("AimAssist").GetComponent<AimAssistManager>().CleanAimAssist();
    }
    /// <summary>
    /// Removes an enemy from the list of spawned mobs
    /// </summary>
    /// <param name="enemy">The enemy to remove</param>
    public void RemoveEnemy(GameObject enemy)
    {
        spawnedMobs.Remove(enemy);
        //Notify delegates that all enemies where cleared
        if (OnAllEnemiesKilledEvent != null && spawnedMobs.Count == 0)
        {
            OnAllEnemiesKilledEvent(false);
        }
    }

    private void Update()
    {
        //TODO REMOVE
        if (Input.GetKeyDown(KeyCode.K))
        {
            SpawnSpecificEnemy(transform.position, transform.rotation, EnemyTypes.Moira);
            SetAIValue(true);
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            SpawnSpecificEnemy(transform.position, transform.rotation, EnemyTypes.ShieldMaiden);
            SetAIValue(true);
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            DestroyAllEnemies(false);
        }
    }
}
