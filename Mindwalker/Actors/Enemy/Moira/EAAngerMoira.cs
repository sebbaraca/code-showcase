﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This script is in charge of the active abilities of the Moira Enemy
/// It inhereits from EnemyActive
/// </summary>
public class EAAngerMoira : EnemyActive
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //CONSTANTS 
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Prefab for projectile
    public GameObject projectilePrefab;
    //Transform of spawn point for projectile
    public Transform projectileSpawnPoint;
    //List of sfx for Moira enemy 0 dash, 1 spawn
    public AudioClip[] sfxAudioClips;
    //Audio Source for HP manager
    public AudioSource audioSource;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    //¨¨¨¨¨¨¨¨¨¨¨¨¨DASH¨¨¨¨¨¨¨¨¨¨
    //Rigid body of the avatar
    private Rigidbody rigidBody;
    //The enemy Movement
    private EnemyMovement enemyMovement;
    //Acceleration for the dash
    private float dashAccelerationFactor;
    //Movement cooldown after the jump
    private float jumpMovementCooldown;
    //Movement cooldown before the jump
    private float jumpMovementPreparationTime;
    //The factor to slow the avatar before the Jump
    private float jumpPreparationSlowFactor;
    //¨¨¨¨¨¨¨¨¨¨¨¨¨¨BULLET¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨    
    //The damage base amount
    private float bulletDamageBase;
    //The damage step while the bullet increases
    private float bulletDamageStep;
    //Travel Speed
    private float bulletSpeed;
    //Explosion Time
    private float explosionTime;
    //Increasing size step for explosion
    private float explosionSizeStep;
    //Rotation speed
    private float rotationSpeed;
    //Time to explode for the bullet
    private float bulletTimeToExplode;
    //List of tags to be damaged
    private List<string> damageTags;
    //¨¨¨¨¨¨¨¨¨¨ANIMATION & FX¨¨¨¨¨¨¨¨
    //Sprite Ghost fx 
    private SpriteGhostFX spriteGhostFX;
    //Number of Ghosts to spawn on dash
    private int dashGhostAmount=5;
    //Exclusivity time for shoot animation
    private float animExclusivityDashTime=0.5f;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~   
    protected override void Awake()
    {
        base.Awake();
        //Set Enemy state
        enemyState = EnemyManager.EnemyStates.Anger;
        //Get rigidbody of the enemy
        rigidBody = GetComponent<Rigidbody>();
        //Get Component For the movement and APAnger
        enemyMovement = GetComponent<EnemyMovement>();
        //List of objects to be damaged by bullet
        damageTags = new List<string> { "Player" };
        spriteGhostFX = GetComponentInChildren<SpriteGhostFX>();        
    }

    private void Start()
    {
        //SFX for spawn
        audioSource.clip = sfxAudioClips[1];
        audioSource.Play();
    }
    /// <summary>
    /// Cleans the script, setting the cooldowns to 0 and restarting variables
    /// </summary>
    public override void CleanActiveAbility()
    {        
        isEnabled = true;
        isOnActiveMovementCoolDown = false; //Disable Cooldowns
        isOnActivePowerCoolDown = false;
    }
    /// <summary>
    /// Shoots a projectile in a given angle
    /// </summary>
    /// <param name="shootingAngle">The angle for the projectile to be shoot</param>
    public void ShootProjectile(float shootingAngle)
    {
        if (!isEnabled || isOnActivePowerCoolDown) return;
        //Disable cooldown so can't spam
        isOnActivePowerCoolDown = true;
        //Manage animation
        ManageAnimation(false);
        SpawnBullet();
        StartCoroutine(InitiateActivePowerCoolDown(powerCoolDownTime));
    }

    /// <summary>
    /// Spawns the projectile that will damage the player
    /// </summary>
    private void SpawnBullet()
    {
        //Spawns Bullet
        GameObject bullet = GameObject.Instantiate(projectilePrefab, projectileSpawnPoint.position, projectileSpawnPoint.rotation);
        //Set values for bullet
        bullet.GetComponent<BulletManager>().SetValues(bulletDamageBase, bulletDamageStep, bulletSpeed, explosionTime, explosionSizeStep, rotationSpeed,damageTags,bulletTimeToExplode);        
    }
    /// <summary>
    /// Dashes on a given direction on the x,z axis
    /// </summary>
    /// <param name="movX">The direction on X angle</param>
    /// <param name="movZ">The direction on Z angle</param>
    /// <param name="boostAmount">The multiplier for the dash force</param>
    public IEnumerator DashOnDirection(float movX, float movZ,float boostAmount)
    {
        if (isEnabled && !isOnActiveMovementCoolDown)
        {
            isOnActiveMovementCoolDown = true;
            //Manage animation
            ManageAnimation(true);
            enemyMovement.SlowEnemyMovement(jumpPreparationSlowFactor);
            //Disable Movement for a short period
            StartCoroutine(enemyMovement.DisableMovementTemporal(jumpMovementPreparationTime));
            yield return new WaitForSeconds(jumpMovementPreparationTime);

            //Charge Forward        
            float pythagoras = ((movX * movX) + (movZ * movZ));
            if (pythagoras > 1f) //Normalize input for same results on diagonals
            {
                float multiplier = 1f / (Mathf.Sqrt(pythagoras));
                movX *= multiplier;
                movZ *= multiplier;
            }
            rigidBody.AddForce(new Vector3(movX, 0f, movZ) * dashAccelerationFactor*boostAmount);

            //Disable Movement for a short period
            StartCoroutine(enemyMovement.DisableMovementTemporal(jumpMovementCooldown));
            //Initiate Cooldown time to enable power
            StartCoroutine(InitiateActiveMovementCoolDown(movementCoolDownTime));
        }
        
    }
    /// <summary>
    /// Manages animations for the enemy
    /// </summary>
    /// <param name="isMovement">Is the animation for movement power?</param>
    private void ManageAnimation(bool isMovement)
    {
        if (isMovement)
        {
            //Ghost fx particles
            StartCoroutine(spriteGhostFX.SpawnGhostFx(dashGhostAmount));
            //SFX
            audioSource.clip = sfxAudioClips[0];
            audioSource.Play();
        }
        else //Attack
        {            
            enemyAnimationManager.SetAttackAnimation(isMovement, animExclusivityDashTime);
        }
       
        
    }

    /// <summary>
    /// Sets the values for the data of this script
    /// </summary>
    /// <param name="pMovementCoolDownTime">cooldownTime for dash & attack</param>
    /// <param name="pPowerCoolDownTime">cooldowntime for tornado attack</param>
    /// <param name="pDashAccelerationFactor">Movement Speed of the avatar(acceleration)</param>
    /// <param name="pBulletDamageBase">The damage base amount</param>
    /// <param name="pBulletDamageStep">The damage step while the bullet increases</param>
    /// <param name="pBulletSpeed">Travel Speed</param>
    /// <param name="pExplosionTime">Explosion Time</param>
    /// <param name="pExplosionSizeStep">Explosion size</param>
    /// <param name="pRotationSpeed">Rotation speed</param>
    /// <param name="pBulletTimeToExplode">Time to explode for the bullet</param>
    /// <param name="pJumpMovementCooldown">Movement cooldown after the jump</param>
    /// <param name="pJumpMovementPreparationTime">Movement cooldown before the jump</param>
    /// <param name="pJumpPreparationSlowFactor">The factor to slow the avatar before the Jump</param>
    public void SetValues(float pMovementCoolDownTime, float pPowerCoolDownTime, float pDashAccelerationFactor, float pBulletDamageBase, float pBulletDamageStep, float pBulletSpeed, float pExplosionTime, float pExplosionSizeStep, float pRotationSpeed, float pBulletTimeToExplode, float pJumpMovementCooldown,float pJumpMovementPreparationTime, float pJumpPreparationSlowFactor)
    {
        bulletTimeToExplode = pBulletTimeToExplode;
        movementCoolDownTime = pMovementCoolDownTime;
        powerCoolDownTime = pPowerCoolDownTime;
        dashAccelerationFactor = pDashAccelerationFactor;
        bulletDamageBase = pBulletDamageBase;
        bulletDamageStep = pBulletDamageStep;
        bulletSpeed = pBulletSpeed;
        explosionTime = pExplosionTime;
        explosionSizeStep = pExplosionSizeStep;
        rotationSpeed = pRotationSpeed;
        jumpMovementCooldown = pJumpMovementCooldown;
        jumpMovementPreparationTime = pJumpMovementPreparationTime;
        jumpPreparationSlowFactor = pJumpPreparationSlowFactor;
    }
}
