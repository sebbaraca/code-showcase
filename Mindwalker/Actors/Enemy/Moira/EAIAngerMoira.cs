﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class that implements the AI of the Moira Enemy
/// </summary>
public class EAIAngerMoira : EnemyAIGeneral
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //CONSTANTS 
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    //Number of times to shoot on stay
    private int shootAmmo;
    //Time to change direction randomly
    private float changeDirectionTime;
    //Shoot cooldown
    private float shootCooldown;
    //Active for anger moira
    private EAAngerMoira eAAngerMoira;
    //SHooting angle
    private float shootingAngle;
    //Direction of movement and dash
    private Vector2 dashDirection;
    private Vector2 movementDirection;
    //Is the enemy shooting?
    private bool isShooting;
    //Reload Time
    private float reloadTime;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~ 
    protected override void Awake()
    {
        base.Awake();
        //Set enemy state
        enemyState = EnemyManager.EnemyStates.Anger;
        eAAngerMoira = GetComponent<EAAngerMoira>();
        //StartAIBehaviour();
    }
    /// <summary>
    /// Examines and Excecutes the AI behaviour of the enemy on a given state
    /// Checks if is iddle, if yes patrols, if not, avoids the player and shoots at him
    /// </summary>
    public override void ExcecuteAIBehaviour()
    {
        //Patrol randomly
        enemyMovement.MoveEnemy(movementDirection.x, movementDirection.y);
        if (!isIddle) //Not Iddle
        {
            //Aim at player
            RefreshAngle();
            enemyMovement.SetEnemyRotation(dashDirection.y, dashDirection.x * -1);
            //Shoot at player, then dash
            StartCoroutine(ShootThenDash());
        }
    }
    /// <summary>
    /// Shoots as many bullets as the ammo indictates
    /// Then dashes either forward or backward
    /// </summary>
    private IEnumerator ShootThenDash()
    {
        if (!isShooting)
        {
            //****Shoot****
            isShooting = true;
            int currentShoot = 0;
            while (currentShoot < shootAmmo)
            {
                eAAngerMoira.ShootProjectile(shootingAngle);
                yield return new WaitForSeconds(shootCooldown);
                currentShoot++;
            }
            yield return new WaitForSeconds(reloadTime);
            //******Dash******
            //Is player within reach? 
            if (objectsToAvoid.Count > 0)
            {
                try
                {
                    //Move and Dash avoiding hazard
                    RefreshAvoidHazardAngle();
                    StartCoroutine(eAAngerMoira.DashOnDirection(dashDirection.y, dashDirection.x * -1, 1f));
                }
                catch (System.Exception) { }
            }
            else if (isInReachArea)
            {
                //Move and Dash random direction
                StartCoroutine(eAAngerMoira.DashOnDirection(movementDirection.y * -1, movementDirection.x, 0.8f));
            }
            else
            {
                //Move and Dash towards him
                StartCoroutine(eAAngerMoira.DashOnDirection(dashDirection.y, dashDirection.x * -1, 1f));
            }
            isShooting = false;
        }

    }
    /// <summary>
    /// Changes the direction of the movement every given seconds
    /// </summary>
    private IEnumerator ChangeRandomMovementDirection()
    {
        movementDirection = new Vector2(Random.Range(-1, 2), Random.Range(-1, 2));
        yield return new WaitForSeconds(changeDirectionTime);
        StartCoroutine(ChangeRandomMovementDirection());
    }
    /// <summary>
    /// Returns the  shooting angle 
    /// </summary>
    /// <returns>The angle between the enemy and the player</returns>
    private void RefreshAngle()
    {
        //Calculate angle
        dashDirection = new Vector2((avatarGameObject.transform.position.z - transform.position.z) * -1, (avatarGameObject.transform.position.x - transform.position.x));
        shootingAngle = Vector2.SignedAngle(dashDirection, Vector2.right);
    }
    /// <summary>
    /// Calculates to avoid the first hazard on the list
    /// </summary>
    private void RefreshAvoidHazardAngle()
    {
        dashDirection = new Vector2((objectsToAvoid[0].transform.position.z - transform.position.z), objectsToAvoid[0].transform.position.x - transform.position.x);
    }
    /// <summary>
    /// Starts the AI behaviour of the Moira Anger enemy
    /// </summary>
    public override void StartAIBehaviour()
    {
        avatarGameObject = GameObject.FindGameObjectWithTag("Player");
        isAIActive = true;
        isShooting = false;
        StartCoroutine(ChangeRandomMovementDirection());
    }
    /// <summary>
    /// Stops the AI behaviour of the Moira Anger enemy
    /// </summary>
    public override void StopAIBehaviour()
    {        
        isAIActive = false;
        StopCoroutine(ChangeRandomMovementDirection());
    }
    /// <summary>
    /// Sets the values for the script
    /// </summary>
    /// <param name="pShootAmmo">Number of times to shoot on stay</param>
    /// <param name="pChangeDirectionTime">Time to change direction randomly</param>
    /// <param name="pShootCooldown">Shoot cooldown</param>
    /// <param name="pReloadTime">The reload Time</param>
    public void SetValues(int pShootAmmo,float pChangeDirectionTime,float pShootCooldown,float pReloadTime)
    {
        reloadTime = pReloadTime;
        shootAmmo = pShootAmmo;
        changeDirectionTime = pChangeDirectionTime;
        shootCooldown = pShootCooldown;
    }
}
