﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class inhereits from the enemy manager.
/// It coordinates the Moira enemy's behaviour.
/// Thus, it comunicates with active and passive behaviour, hp and movement manager. Also AI.
/// </summary>
public class EMMoira : EnemyManager
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //CONSTANTS 
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Prefab of different HP Pots that can be spawned by this enemy
    public GameObject[] HpPots;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~   
    //The data structure with the loaded data of each enemy state
    private List<EMoiraSnapshotManager.EMoiraSnapshot> snapshots;
    //The current Enemy state
    private EMoiraSnapshotManager.EMoiraSnapshot enemySnapshot;
   
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    
    /// <summary>
    /// Gets the specific script references for the given enemy
    /// </summary>
    protected override void GetSpecificScriptReferences()
    {
        //Set default active power
        enemyActive = enemiesActives[EnemyStates.Anger];
        currentAI = enemiesAIs[EnemyStates.Anger];
        enemyActive.isEnabled = true;
    }
    /// <summary>
    /// Loads the avatar states data into the game
    /// Data is loaded into the snapshots list
    /// </summary>
    protected override void LoadEnemyData()
    {
        snapshots = EMoiraSnapshotManager.Load().EMoiraSnapshots;
        //Load active anger data
        EAAngerMoira eAAngerMoira = (EAAngerMoira)enemiesActives[EnemyStates.Anger];
        eAAngerMoira.SetValues(snapshots[(int)EnemyStates.Anger].movementCoolDownTime, snapshots[(int)EnemyStates.Anger].powerCoolDownTime, snapshots[(int)EnemyStates.Anger].dashAccelerationFactor, snapshots[(int)EnemyStates.Anger].bulletDamageBase, snapshots[(int)EnemyStates.Anger].bulletDamageStep, snapshots[(int)EnemyStates.Anger].bulletSpeed, snapshots[(int)EnemyStates.Anger].explosionTime, snapshots[(int)EnemyStates.Anger].explosionSizeStep, snapshots[(int)EnemyStates.Anger].rotationSpeed, snapshots[(int)EnemyStates.Anger].bulletTimeToExplode, snapshots[(int)EnemyStates.Anger].jumpMovementCooldown, snapshots[(int)EnemyStates.Anger].jumpMovementPreparationTime, snapshots[(int)EnemyStates.Anger].jumpPreparationSlowFactor);
        //Set values for AI
        EAIAngerMoira eAIAngerMoira = (EAIAngerMoira)enemiesAIs[EnemyStates.Anger];
        eAIAngerMoira.SetValues(snapshots[(int)EnemyStates.Anger].shootAmmo, snapshots[(int)EnemyStates.Anger].changeDirectionTime, snapshots[(int)EnemyStates.Anger].powerCoolDownTime, snapshots[(int)EnemyStates.Anger].reloadTime);
        //Set default Enemy state
        SwapEnemyState(EnemyStates.Anger, false);        
    }
    /// <summary>
    /// Excecutes especific death behaviour of the enemy
    /// In this case spawn a small HP Pot
    /// </summary>
    public override void DeathBehaviour()
    {
        //Spawn a small hp pot
        SpawnObjectOnEnemyPosition(HpPots[0]);
    }
    /// <summary>
    /// Swaps the avatar state. 
    /// This implies changing movement, active, passive, health
    /// </summary>
    /// <param name="pEnemyState">The Id of the new state to be applied</param>
    /// <param name="isLoadingCheckpoint">is the enemy state swaping from a loading data?</param>
    public override void SwapEnemyState(EnemyStates pEnemyState, bool isLoadingCheckpoint)
    {
        /*/TODO-------------------- Remove this  ---------------------    
        switch (pEnemyState)
        {
            case EnemyStates.Neutral:
                GetComponentInChildren<SpriteRenderer>().color = Color.cyan;
                break;
            case EnemyStates.Anger:
                GetComponentInChildren<SpriteRenderer>().color = Color.red;
                break;
            case EnemyStates.Boredom:
                GetComponentInChildren<SpriteRenderer>().color = Color.yellow;
                break;
            default:
                break;
        }
        //----------------------------------------------------------*/        
        enemyState = pEnemyState;
        //Change current avatar state
        enemySnapshot = snapshots[(int)pEnemyState];
        //HP settings
        hPManager.SetValues(enemySnapshot.health, enemySnapshot.defensivePower, enemySnapshot.hurtingCooldown, 0f, isLoadingCheckpoint,true);
        //Movement swap
        enemyMovement.ChangeEnemyMovementValues(enemySnapshot.accelerationFactor, enemySnapshot.maxSpeed, enemySnapshot.slowFactor, enemySnapshot.mass, enemySnapshot.linearDrag);
        //Swap and clean Active Behaviour
        enemyActive.isEnabled = false;
        enemyActive = enemiesActives[pEnemyState];
        enemyActive.CleanActiveAbility();
        //AI Values
        //currentAI.StopAIBehaviour();
        currentAI = enemiesAIs[pEnemyState];
        //currentAI.StartAIBehaviour();
    }
}
