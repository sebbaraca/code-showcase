﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class that implements the AI of the Shield Maiden Enemy
/// </summary>
public class EAIShieldMaiden : EnemyAIGeneral
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //CONSTANTS 
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~     
    //Time to change direction randomly
    private float changeDirectionTime;
    //Shield Bash cooldown
    private float bashCoolDown;
    //Active for anger moira
    private EAShieldMaiden eAShieldMaiden;
    //Direction of movement and dash
    private Vector2 dashDirection;
    private Vector2 movementDirection;
    //Is the enemy Bashing the shield?
    private bool isBashing;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~ 
    protected override void Awake()
    {
        base.Awake();
        //Set enemy state
        enemyState = EnemyManager.EnemyStates.Anger;
        eAShieldMaiden = GetComponent<EAShieldMaiden>();
        //StartAIBehaviour();
    }
    /// <summary>
    /// Examines and Excecutes the AI behaviour of the enemy on a given state
    /// Checks if is iddle, if yes patrols, if not, gets close to the player and bashes him
    /// </summary>
    public override void ExcecuteAIBehaviour()
    {
        if (!isIddle) //Not Iddle
        {
            //Aim at player
            RefreshAngle();
            enemyMovement.SetEnemyRotation(dashDirection.y, dashDirection.x * -1);
            //Walk at player
            enemyMovement.MoveEnemy(dashDirection.y, dashDirection.x*-1);
            //If is onreach, bash shield
            StartCoroutine(BashShield());
        }
        else
        {
            //Patrol randomly
            enemyMovement.MoveEnemy(movementDirection.x, movementDirection.y);
        }
    }
    /// <summary>
    /// Bashes the shield directed to the player, stunning him
    /// </summary>
    private IEnumerator BashShield()
    {
        if (!isBashing)
        {           
            //Is player within reach? 
            if (objectsToAvoid.Count > 0)
            {
                try
                {
                    //Move and Dash avoiding hazard
                    RefreshAvoidHazardAngle();
                    StartCoroutine(eAShieldMaiden.DashAndBashOnDirection(this, 1f));                    
                }
                catch (System.Exception) { }
                yield return new WaitForSeconds(bashCoolDown);
            }
            else if (isInReachArea)
            {
                //Move and Dash towards him
                StartCoroutine(eAShieldMaiden.DashAndBashOnDirection(this, 1f));
                yield return new WaitForSeconds(bashCoolDown);
            }
            isBashing = false;
        }
    }
    /// <summary>
    /// Changes the direction of the movement every given seconds
    /// </summary>
    private IEnumerator ChangeRandomMovementDirection()
    {
        movementDirection = new Vector2(Random.Range(-1, 2), Random.Range(-1, 2));
        yield return new WaitForSeconds(changeDirectionTime);
        StartCoroutine(ChangeRandomMovementDirection());
    }
    /// <summary>
    /// Returns the  shooting angle 
    /// </summary>
    /// <returns>The angle between the enemy and the player</returns>
    public Vector2 RefreshAngle()
    {
        //Calculate angle
        dashDirection = new Vector2((avatarGameObject.transform.position.z - transform.position.z) * -1, (avatarGameObject.transform.position.x - transform.position.x));
        return dashDirection;
    }
    /// <summary>
    /// Calculates to avoid the first hazard on the list
    /// </summary>
    private void RefreshAvoidHazardAngle()
    {
        dashDirection = new Vector2((objectsToAvoid[0].transform.position.z - transform.position.z), objectsToAvoid[0].transform.position.x - transform.position.x);
    }
    /// <summary>
    /// Starts the AI behaviour of the Moira Anger enemy
    /// </summary>
    public override void StartAIBehaviour()
    {
        avatarGameObject = GameObject.FindGameObjectWithTag("Player");
        isAIActive = true;
        isBashing = false;
        StartCoroutine(ChangeRandomMovementDirection());
    }
    /// <summary>
    /// Stops the AI behaviour of the Moira Anger enemy
    /// </summary>
    public override void StopAIBehaviour()
    {
        isAIActive = false;
        StopCoroutine(ChangeRandomMovementDirection());
    }
    /// <summary>
    /// Sets the values for the script
    /// </summary>
    /// <param name="pChangeDirectionTime">Time to change direction randomly</param>
    /// <param name="pBashCoolDown"> cooldown between dashes</param>
    public void SetValues( float pChangeDirectionTime, float pBashCoolDown)
    {
        changeDirectionTime = pChangeDirectionTime;
        bashCoolDown = pBashCoolDown;
    }
}
