﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This script is in charge of the active abilities of the Shield Maiden
/// It inhereits from EnemyActive
/// </summary>
public class EAShieldMaiden : EnemyActive
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //CONSTANTS 
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~ 
    //The Game object that contains the shield
    public GameObject ShieldGameObject;
    //List of sfx for shield maiden enemy 0 dash, 1 spawn
    public AudioClip[] sfxAudioClips;
    //Audio Source for HP manager
    public AudioSource audioSource;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    //¨¨¨¨¨¨¨¨¨¨¨¨¨DASH¨¨¨¨¨¨¨¨¨¨
    //Rigid body of the avatar
    private Rigidbody rigidBody;
    //The enemy Movement
    private EnemyMovement enemyMovement;
    //Acceleration for the dash
    private float dashAccelerationFactor;
    //Movement cooldown after the jump
    private float jumpMovementCooldown;
    //Movement cooldown before the jump
    private float jumpMovementPreparationTime;
    //The factor to slow the avatar before the Jump
    private float jumpPreparationSlowFactor;
    //Bash attack damage
    private float bashAttackDamage;
    //The duration for the bash stun
    private float stunDuration;
    //The duration of the stun Window
    private float stunWindowDuration;
    //Is teh enemy bashing?
    private bool isBashing;
    //Vulnerable window duration
    private float vulnerableWindowDuration;
    //¨¨¨¨¨¨¨¨¨¨ANIMATION & FX¨¨¨¨¨¨¨¨
    //Sprite Ghost fx 
    private SpriteGhostFX spriteGhostFX;
    //Number of Ghosts to spawn on dash
    private int dashGhostAmount=15;
    //Exclusivity time for shoot animation
    private float animExclusivityDashTime=1;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~   
    protected override void Awake()
    {
        base.Awake();
        //Set Enemy state
        enemyState = EnemyManager.EnemyStates.Anger;
        //Get rigidbody of the enemy
        rigidBody = GetComponent<Rigidbody>();     
        //Get Component For the movement and APAnger
        enemyMovement = GetComponent<EnemyMovement>();
        isBashing = false;
        spriteGhostFX = GetComponentInChildren<SpriteGhostFX>();
    }
    private void Start()
    {        
        //SFX for spawn
        audioSource.clip = sfxAudioClips[1];
        audioSource.Play();
    }
    /// <summary>
    /// Cleans the script, setting the cooldowns to 0 and restarting variables
    /// </summary>
    public override void CleanActiveAbility()
    {
        isEnabled = true;
        isOnActiveMovementCoolDown = false; //Disable Cooldowns
        isOnActivePowerCoolDown = false;
    }
    /// <summary>
    /// Dashes on a given direction on the x,z axis
    /// </summary>
    /// <param name="eAIShieldMaiden">The AI of this enemy</param>   
    /// <param name="boostAmount">The multiplier for the dash force</param>
    public IEnumerator DashAndBashOnDirection(EAIShieldMaiden eAIShieldMaiden, float boostAmount)
    {
        if (isEnabled && !isOnActiveMovementCoolDown)
        {
            isOnActiveMovementCoolDown = true;
            //Manage animation
            ManageAnimation();
            enemyMovement.SlowEnemyMovement(jumpPreparationSlowFactor);
            //Disable Movement for a short period
            StartCoroutine(enemyMovement.DisableMovementTemporal(jumpMovementPreparationTime));
            yield return new WaitForSeconds(jumpMovementPreparationTime);
            Vector2 dashDirection = eAIShieldMaiden.RefreshAngle();
            
            float movX= dashDirection.y;
            float movZ = dashDirection.x * -1;
            //Charge Forward        
            float pythagoras = ((movX * movX) + (movZ * movZ));
            if (pythagoras > 1f) //Normalize input for same results on diagonals
            {
                float multiplier = 1f / (Mathf.Sqrt(pythagoras));
                movX *= multiplier;
                movZ *= multiplier;
            }
            rigidBody.AddForce(new Vector3(movX, 0f, movZ) * dashAccelerationFactor * boostAmount);
            // Excecute attack and make vulnerable
            try { StartCoroutine(OpenBashNVulnerableWindow()); } catch (System.Exception) { }
            //Disable Movement for a short period
            StartCoroutine(enemyMovement.DisableMovementTemporal(jumpMovementCooldown));
            //Initiate Cooldown time to enable power
            StartCoroutine(InitiateActiveMovementCoolDown(movementCoolDownTime));
        }
    }
    /// <summary>
    /// Executes a bash attack, stunning and damaging the avatar if caught
    /// Then makes the enemy vulnerable for a given duration
    /// </summary>
    private IEnumerator OpenBashNVulnerableWindow()
    {
        isBashing = true;
        yield return new WaitForSeconds(stunWindowDuration);
        isBashing = false;
        ShieldGameObject.SetActive(false);// Shield disable
        //Open vulnerability
        StartCoroutine(hPManager.StartVulnerableCoolDown(vulnerableWindowDuration));
        yield return new WaitForSeconds(vulnerableWindowDuration);
        ShieldGameObject.SetActive(true);//Shield enable
    }
    /// <summary>
    /// On each collision check if collides with player
    /// if enemy is bashing stun him
    /// </summary>
    private void OnCollisionEnter(Collision collision)
    {
        //If is bashing and collides with the player stun him
        if (isBashing && collision.gameObject.tag == "Player")
        {//Executes the attack
            collision.gameObject.GetComponent<HPManager>().TakeDamage(bashAttackDamage, true, stunDuration,false);
        }
    }
    /// <summary>
    /// On each collision check if collides with player
    /// if enemy is bashing stun him
    /// </summary>
    private void OnCollisionStay(Collision collision)
    {
        //If is bashing and collides with the player stun him
        if (isBashing && collision.gameObject.tag == "Player")
        {//Executes the attack
            collision.gameObject.GetComponent<HPManager>().TakeDamage(bashAttackDamage, true, stunDuration,false);
        }
    }

    /// <summary>
    /// Manages animations for the enemy
    /// </summary>
    private void ManageAnimation()
    {
        //SFX
        audioSource.clip = sfxAudioClips[0];
        audioSource.Play();
        //Ghost fx particles
        StartCoroutine(spriteGhostFX.SpawnGhostFx(dashGhostAmount));       
        //Attack
            enemyAnimationManager.SetAttackAnimation(false, animExclusivityDashTime);        
    }

    /// <summary>
    /// Sets the values for the data of this script
    /// </summary>
    /// <param name="pMovementCoolDownTime">cooldownTime for dash & attack</param>
    /// <param name="pPowerCoolDownTime">cooldowntime for tornado attack</param>
    /// <param name="pDashAccelerationFactor">Movement Speed of the avatar(acceleration)</param>    
    /// <param name="pJumpMovementCooldown">Movement cooldown after the jump</param>
    /// <param name="pJumpMovementPreparationTime">Movement cooldown before the jump</param>
    /// <param name="pJumpPreparationSlowFactor">The factor to slow the avatar before the Jump</param>
    /// <param name="pBashAttackDamage">Bash attack damage</param>
    /// <param name="pStunDuration"> The duration for the bash stun</param>
    /// <param name="pStunWindowDuration"> The duration for the window of bash stun</param>
    /// <param name="pVulnerableWindowDuration"> Vulnerable window duration</param>
    public void SetValues(float pMovementCoolDownTime, float pPowerCoolDownTime, float pDashAccelerationFactor, float pJumpMovementCooldown, float pJumpMovementPreparationTime, float pJumpPreparationSlowFactor, float pBashAttackDamage, float pStunDuration, float pStunWindowDuration, float pVulnerableWindowDuration)
    {
        vulnerableWindowDuration = pVulnerableWindowDuration;
        stunWindowDuration = pStunWindowDuration;
        bashAttackDamage = pBashAttackDamage;
        stunDuration = pStunDuration;
        movementCoolDownTime = pMovementCoolDownTime;
        powerCoolDownTime = pPowerCoolDownTime;
        dashAccelerationFactor = pDashAccelerationFactor;
        jumpMovementCooldown = pJumpMovementCooldown;
        jumpMovementPreparationTime = pJumpMovementPreparationTime;
        jumpPreparationSlowFactor = pJumpPreparationSlowFactor;
    }
}
