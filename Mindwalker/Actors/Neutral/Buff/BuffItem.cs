﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class is the base structure of items that give
/// buffs to an actor
/// </summary>
public abstract class BuffItem : MonoBehaviour
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //CONSTANTS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //The differente buff types that can be applied
    public enum BuffTypes{ HpPot }
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //The buff that this item will provide
    public BuffTypes buffType;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~ 
    //List of tags of actors that will benefit from the buff
    protected List<string> actorTagList;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //ABSTRACT METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    /// <summary>
    /// Applies the intended buff to the actor
    /// </summary>
    /// <param name="actor">The Game Object of the actor</param>
    protected abstract void ApplyBuff(GameObject actor);
    /// <summary>
    /// Fill the tag list of actors that will benefit from buffs
    /// </summary>
    protected abstract void SetActorTags();
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~ 
    private void Awake()
    {
        SetActorTags();
    }
    /// <summary>
    /// Applies the buff to the actor that interacts with it
    /// and then proceed to destroy itself
    /// </summary>
    /// <param name="other">The game object that entered the item trigger</param>
    private void OnTriggerEnter(Collider other)
    {
        //Check if on tag list
        if (actorTagList.Contains(other.tag))
        {
            //Apply buff
            ApplyBuff(other.gameObject);
            //Destroy gameobject
            GameObject.Destroy(this.gameObject);
        }
    }
}
