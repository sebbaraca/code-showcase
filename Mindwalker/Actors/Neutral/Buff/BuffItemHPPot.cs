﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class is in charge of replenishing the HP of the 
/// Avatar when he interacts with an HP pot by touchin it
/// </summary>
public class BuffItemHPPot : BuffItem
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //CONSTANTS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Different Sizes and recovery ammounts for HP Pots
    public enum HPPotTypes{ Small=5, Medium=15, Large=30}
    
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //The size of this HPPot
    public HPPotTypes hPPotType;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~ 
   

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    /// Applies the intended buff to the actor
    /// </summary>
    /// <param name="actor">The Game Object of the actor</param>
    protected override void ApplyBuff(GameObject actor)
    {
        actor.GetComponent<HPManager>().RecoverHP(((float)hPPotType /100f));
    }
    /// <summary>
    /// Fill the tag list of actors that will benefit from buffs
    /// </summary>
    protected override void SetActorTags()
    {
        actorTagList = new List<string>() { "Player" };
    }
}
