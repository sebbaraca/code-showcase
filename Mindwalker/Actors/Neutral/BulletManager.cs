﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class is in charge of the bullet fired by the neutral avatar state
/// </summary>
public class BulletManager : MonoBehaviour
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //CONSTANTS
    //~~~~~~~~~~~~~~~~~~~~~~~~

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC  VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Particle FX on collision
    public GameObject particleFx;
    //Duration before destruction on particles
    public float particleDuration=5f;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    //List of tags to be damaged
    private List<string> damageTags;
    //The damage base amount
    private float bulletDamageBase;
    //The damage step while the bullet increases
    private float bulletDamageStep;
    //Travel Speed
    private float bulletSpeed;
    //Increasing size for explosion
    //Explosion Time
    private float explosionTime;
    //Explosion size
    private float explosionSizeStep;
    //Rotation speed
    private float rotationSpeed;
    //Has started destruction?
    private bool isDestroyOnProcess;
    //Rigid body of the bullet
    private Rigidbody rigidBody;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    private void Awake()
    {
        damageTags = new List<string>();
        rigidBody = GetComponent<Rigidbody>();
    }
    private void Update()
    {
        //Moves forward if is not on destruction process
        transform.Rotate(Vector3.back, rotationSpeed);
        if (!isDestroyOnProcess)
        {
            // transform.Translate(Vector3.back * bulletSpeed);
            rigidBody.AddRelativeForce(Vector3.back * bulletSpeed);
            transform.localScale *= explosionSizeStep;
            bulletDamageBase += bulletDamageStep;
        }
    }
    /// <summary>
    ///Sets the values of the script
    /// </summary>
    /// <param name="pBulletDamageBase">The damage base amount</param>
    /// <param name="pBulletDamageStep"> The damage step while the bullet increases</param>
    /// <param name="pBulletSpeed">Travel Speed</param>
    /// <param name="pExplosionTime">Explosion Time of the bullet</param>
    /// <param name="pExplosionSizeStep">The value of the step of explosion size</param>
    /// <param name="pRotationSpeed">Rotation speed of the bullet</param>
    /// <param name="pDamageTags">The list of tags that the bullet can damage</param>
    /// <param name="pBulletTimeToExplode">The time for the bullet to self destruct</param>
    public void SetValues(float pBulletDamageBase, float pBulletDamageStep, float pBulletSpeed, float pExplosionTime, float pExplosionSizeStep, float pRotationSpeed,List<string> pDamageTags,float pBulletTimeToExplode)
    {
        isDestroyOnProcess = false;
        bulletDamageBase = pBulletDamageBase;
        bulletDamageStep = pBulletDamageStep;
        bulletSpeed = pBulletSpeed;
        explosionTime = pExplosionTime;
        explosionSizeStep = pExplosionSizeStep;
        rotationSpeed = pRotationSpeed;
        damageTags = pDamageTags;
        StartCoroutine(WaitForDestruction(pBulletTimeToExplode));
    }
    /// <summary>
    /// Waits a given time then destroys the bullet
    /// </summary>
    /// <param name="pBulletTimeToExplode">The time to explode</param>    
    private IEnumerator WaitForDestruction(float pBulletTimeToExplode)
    {
        yield return new WaitForSeconds(pBulletTimeToExplode);
        StartCoroutine(DestroyBullet());
    }
    /// <summary>
    /// Detects the Game object with the enemy or player tag that entered on the hit 
    /// collider zone of the hazard.Then he applies damage
    /// </summary>
    private void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag!="Ground")
        {
            GameObject.Destroy(GameObject.Instantiate(particleFx, transform.position, transform.rotation), particleDuration);
            StartCoroutine(DestroyBullet());
            if (damageTags.Contains(other.gameObject.tag))
            {
                other.gameObject.GetComponent<HPManager>().TakeDamage(bulletDamageBase, false, 0f, false);
            }
        }
    }

    /// <summary>
    /// Destroys the bullet on impact
    /// </summary>
    public IEnumerator DestroyBullet()
    {
        if (!isDestroyOnProcess)
        {
            isDestroyOnProcess = true;
            //GetComponent<Collider>().enabled = false;            
            yield return new WaitForSeconds(explosionTime);
            GameObject.Destroy(this.gameObject);
        }
    }


}
