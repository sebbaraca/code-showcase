﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class is in charge of managing the health of an individual
/// Used by either the avatar or the enemies
/// </summary>
public class HPManager : MonoBehaviour
{
    // This delegate allows to update the Avatar's HP on the UI
    public delegate void OnAvatarHealthChange(float hP, float maxHP,float threshold);
    // Event that notifies the change in the Avatar's HP
    public OnAvatarHealthChange OnAvatarHealthChangeEvent;  


    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    //Bool used to identify if it's an avatar or an enemy
    public bool isAvatar;
    //Is the manager ignoring the defense?
    public bool isIgnoringDefense { get; set; }
    //The current HP of the individual
    public float currentHP = 1;
    //List of sfx for health 0 Hurt neutral, 1 death neutral, 2 health recover, 3 hurt anger, 4  death anger
    public AudioClip[] sfxAudioClips;    
    //Audio Source for HP manager
    public AudioSource audioSource;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~ 
    //Avatar manager
    private AvatarManager avatarManager;
    //The Maximum HP of the individual
    private float maxHP=1;
    //The Warning threshold to warn the player on UI goes from 0 to 1
    private float warningThreshold;
    //Is the player dead?
    private bool isDead;
    //The defense of the individual on the given state 
    //(Goes from 0 to 1, being 1 invulnerability)
    private float defensivePower;
    //Cooldown to determine how often the individual can take damage
    private float hurtingCooldown;
    //Can take damage?
    private bool canTakeDamage;
    //Is vulnerable type?
    private bool isVulnerableType;
    //HealthBar Manager
    private HealthBarManager healthBarManager;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    private void Awake()
    {
        //Get Health Bar Manager
        if (!isAvatar) healthBarManager = GetComponentInChildren<HealthBarManager>();
        //Avatar
        else avatarManager = GetComponent<AvatarManager>();
    }
    //Initialize the variables
    private void Start()
    {
        isDead = false;
        isIgnoringDefense = false; 

    }
   

    /// <summary>
    /// Makes the actor vulnerable for some seconds 
    /// Then goes back to a invulnerable state
    /// </summary>
    /// <param name="cooldownTime">The vulnerability duration</param>
    public IEnumerator StartVulnerableCoolDown(float cooldownTime)
    {
        canTakeDamage = true;
        yield return new WaitForSeconds(cooldownTime);
        canTakeDamage = false;
    }

    /// <summary>
    /// Makes the actor invulnerable for some seconds 
    /// Then goes back to a vulnerable state
    /// </summary>
    /// <param name="cooldownTime">The invulnerability duration</param>
    public IEnumerator StartInvulnerableCoolDown(float cooldownTime)
    {
        canTakeDamage = false;
        yield return new WaitForSeconds(cooldownTime);
        canTakeDamage = true;
    }
   
    /// <summary>
    /// Stuns the actor for a given time, then goes back to normal
    /// </summary>
    /// <param name="stunDuration">Duration to be stuned</param>
    private void StunActor(float stunDuration)
    {
        //Stun for the given duration
        if (isAvatar) StartCoroutine(GetComponent<AvatarManager>().StunForDuration(stunDuration));
        else StartCoroutine(GetComponent<EnemyManager>().StunForDuration(stunDuration));
    }
    /// <summary>
    /// Takes Damage based on a damageAmount and the defense of the individual     
    /// </summary>
    /// <param name="damageAmount">The individual is going to recieve if it has no defense</param>
    /// <param name="isStunning">Is the damage going to stun</param>
    /// <param name="stunDuration">Duration of the stun to happen</param>
    /// <param name="useBypass">Bypass the take damage status</param>
    public void TakeDamage(float damageAmount,bool isStunning,float stunDuration,bool useBypass)
    {        
        if ((!canTakeDamage&&!useBypass) ||isDead) return; //If can't take damage returns        
        //Stun if must       
        if (isStunning) StunActor(stunDuration); 
        //Take Damage
        currentHP -= damageAmount * (isIgnoringDefense ? 1 : 1 - defensivePower);
        //SFX
        audioSource.clip = sfxAudioClips[(isAvatar&&avatarManager.avatarState==AvatarManager.AvatarStates.Anger)?3:0];
        audioSource.Play();
        if (currentHP <= 0)
        {
            //SFX
            audioSource.clip = sfxAudioClips[(isAvatar && avatarManager.avatarState == AvatarManager.AvatarStates.Anger) ? 4 : 1];
            audioSource.Play();
            // Kill the player 
            if (isAvatar)
            {
                StartCoroutine(KillAvatar());                
            }
            else //Kill Enemy
            {
               KillEnemy();
            }
        }
        //Refresh the UI for the Avatar and Healthbar for Enemy
        if (isAvatar && OnAvatarHealthChangeEvent != null) OnAvatarHealthChangeEvent(currentHP, maxHP,warningThreshold);
        if (!isAvatar) healthBarManager.RefreshHPBar(currentHP > 0 ? (currentHP / maxHP):0);
        //Start invulnerability cooldown if is vulnerable type
        if(currentHP>0&&isVulnerableType)StartCoroutine(StartInvulnerableCoolDown(hurtingCooldown));
    }
    /// <summary>
    /// Kills the enemy, destroying the object and removing hit lists
    /// </summary>
    private void KillEnemy()
    {
        //Unregister from hit list
        foreach (var item in GameObject.FindGameObjectWithTag("Player").GetComponents<AvatarActive>())
        {
            item.UnRegisterObject(gameObject, true);
            item.UnRegisterObject(gameObject, false);
        }
        gameObject.SetActive(false);
        //Unsubscribe from aim assist
        GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<AimAssistManager>().UnRegisterTarget(gameObject);
        GetComponent<EnemyManager>().DeathBehaviour();
        //Unsubscribe from enemy spawner
        GameObject.FindGameObjectWithTag("GM").GetComponent<EnemySpawner>().RemoveEnemy(this.gameObject);
        Destroy(this.gameObject, 5f);
    }
    /// <summary>
    /// Recover HP based on a recoverAmount      
    /// </summary>
    /// <param name="recoverPercentage">The HP Percentage to recover</param>
    public void RecoverHP(float recoverPercentage)
    {
        //SFX
        audioSource.clip = sfxAudioClips[2];
        audioSource.Play();
        //Recover Hp - If when recovered > Max then set max hp as new hp
        currentHP = ((currentHP + maxHP*recoverPercentage > maxHP) ? maxHP : currentHP + maxHP * recoverPercentage);
        //Refresh the UI for the Avatar and Healthbar for Enemy
        if (isAvatar && OnAvatarHealthChangeEvent != null) OnAvatarHealthChangeEvent(currentHP, maxHP,warningThreshold);
        if (!isAvatar) healthBarManager.RefreshHPBar(currentHP / maxHP);
    }

    /// <summary>
    /// Sets the values for the Hp Manager
    /// </summary>
    /// <param name="pMaxHP">The Maximum HP of the individual   </param>
    /// <param name="pDefensivePower">The defense of the individual on the given state -- (Goes from 0 to 1, being 1 invulnerability)</param>
    /// <param name="pHurtingCoolDown">Cooldown to determine how often the individual can take damage</param>
    /// <param name="pWarningThreshold">The Warning threshold to warn the player on UI goes from 0 to 1</param>
    /// <param name="isLoadingCheckpoint">Is the value from loading checkpoint?</param>
    /// <param name="isVulnerable">Is the user of this vulnerable?</param>
    public void SetValues(float pMaxHP, float pDefensivePower,float pHurtingCoolDown,float pWarningThreshold,bool isLoadingCheckpoint,bool isVulnerable)
    {        
        isIgnoringDefense = false;
        if(!isLoadingCheckpoint) //If the data is loading a checkpoint must not scale 
            currentHP = (currentHP * pMaxHP / maxHP); //Set current Hp on the new Hp scale    
        maxHP = pMaxHP;
        defensivePower = pDefensivePower;
        hurtingCooldown = pHurtingCoolDown;
        warningThreshold = pWarningThreshold;
        isVulnerableType = isVulnerable;
        canTakeDamage = isVulnerable;
        //Refresh the UI for the Avatar and Healthbar for Enemy
        if (isAvatar && OnAvatarHealthChangeEvent != null) OnAvatarHealthChangeEvent(currentHP, maxHP,warningThreshold);
        if (!isAvatar) healthBarManager.RefreshHPBar(currentHP / maxHP);
  
    }

    /// <summary>
    /// Starts the player cooldown and then loads last checkpoint
    /// </summary>
    private IEnumerator KillAvatar()
    {
        isDead = true;
        StunActor(2f);
        yield return new WaitForSeconds(2f);
        isDead = false;
        //Load last checkpoint
        //GameObject.FindGameObjectWithTag("GM").GetComponent<RespawnManager>().LoadCheckPoint();
        CheckpointManager.Instance.LoadCheckpointAndRestart();
    }
}
