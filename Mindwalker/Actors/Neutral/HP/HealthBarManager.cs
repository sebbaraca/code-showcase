﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Manager of the Health bar used on each individual
/// </summary>
public class HealthBarManager : MonoBehaviour
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Max Lenght when the bar is full
    public Vector3 MaxHealthBarLenght= new Vector3(0.15f,0.03f,1f);
    //Values for the bar to change color each one goes from 0 to 1 
    //X is high y medium
    public Vector2 colorThreshold = new Vector2(0.65f, 0.33f);
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~   
    //The transform of the HB
    private Transform healthBarTransform;
    //Current percentage of the HB
    private Vector3 curentHealthBarLenght= new Vector3(0.15f,0.03f,1f);
    //The HP bar Sprite
    private SpriteRenderer hPBarSprite;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    // Use this for initialization
    void Awake()
    {
        hPBarSprite = GetComponent<SpriteRenderer>();
        healthBarTransform = GetComponent<Transform>();
        RefreshHPBar(1f);
    }
    /// <summary>
    /// Refreshes the HP bar lenght
    /// </summary>
    /// <param name="percentage">The percentage of current health/ max health</param>
    public void RefreshHPBar(float percentage)
    {        
        //Set Color of the bar
        if (percentage > colorThreshold.x) hPBarSprite.color = Color.green;
        else if (percentage > colorThreshold.y) hPBarSprite.color = Color.yellow;
        else hPBarSprite.color = Color.red;
        curentHealthBarLenght.x = MaxHealthBarLenght.x * percentage;
        healthBarTransform.localScale = curentHealthBarLenght;
    }
}
