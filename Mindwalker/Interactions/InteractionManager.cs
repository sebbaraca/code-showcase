﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class is in charge of managing the player interactions with objects
/// To achieve that an Interactive Object informs to the player that he has
/// entered his interaction area
/// </summary>
public class InteractionManager : MonoBehaviour
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    //The current interactive object that is on the player's reach
    public List<InteractiveObject> interactiveObjects;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~

    // Use this for initialization
    void Start()
    {

        interactiveObjects = new List<InteractiveObject>(); //Set the current interactive object to none
        SubscribeInputs(); //Subscribe to inputs
    }

    /// <summary>
    /// Starts an interaction with the current interactive object
    /// </summary>
    void StartInteractionWithCurrentObjects()
    {
        foreach (var item in interactiveObjects)
        {
            item.StartObjectInteraction(gameObject);
        }
    }

    //[[[[[[[[[[[[[[[  DELEGATES  ]]]]]]]]]]]]]]]
    //Unsuscribes from delegates before being destroyed
    void OnDestroy()
    {
        // We unsubscribe to avoid memory leaks
        UnSubscribeInputs();
    }

    // Suscribes the Input Manager delegates
    public void SubscribeInputs()
    {
        InputManager.instance.OnYButtonEvent += StartInteractionWithCurrentObjects;
    }
    //Unsuscribes the Input Manager delegates
    public void UnSubscribeInputs()
    {
        InputManager.instance.OnYButtonEvent -= StartInteractionWithCurrentObjects;
    }
}
