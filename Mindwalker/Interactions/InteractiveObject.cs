﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Interface used for Interactive objects such as switches, buttons, NPC Etc..
/// All the classes that inhereit from this class start wit IO
/// </summary>
public abstract class InteractiveObject : MonoBehaviour
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    // If it is automatic only by getting inside the trigger zone it will activate the required effect. It it is manual, the player has to press the interaction button
    public bool IsAutomatic;

    public bool hasInteracted;

    public bool isReloadable;

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //ABSTRACT METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Method that is called when the player Interacts with this object    
    public abstract void StartObjectInteraction(GameObject player);
    //Method that is called when the player stops interacting with this object    
    public abstract void StopObjectInteraction();
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    /// Informs the player that he has entered an interactive space
    /// </summary>
    /// <param name="collision">the collision detected</param>
    private void OnTriggerEnter(Collider collision)
    {
        //Has the player entered the interactive space?
        if(collision.tag=="Player" && !hasInteracted)
        {
            //Set the current interactive object to this one
            AddGameObjectToInteractiveList(collision.gameObject, true);
            if (IsAutomatic == true)
            {
                StartObjectInteraction(collision.gameObject);
            }
        }
    }
    /// <summary>
    /// Informs the player that he has Exited an interactive space
    /// </summary>
    /// <param name="collision">the collision detected/// </summary></param>
    private void OnTriggerExit(Collider collision)
    {
        //Has the player entered the interactive space?
        if (collision.tag == "Player")
        {
            StopObjectInteraction();
            AddGameObjectToInteractiveList(collision.gameObject, false);
        }
    }

    public void AddGameObjectToInteractiveList(GameObject player, bool newValue)
    {
        List<InteractiveObject> interactiveObjects = player.GetComponent<InteractionManager>().interactiveObjects;
        if (newValue)
        {
            interactiveObjects.Add(this);
        }
        else
        {
            interactiveObjects.Remove(this);
        }
    }

}
