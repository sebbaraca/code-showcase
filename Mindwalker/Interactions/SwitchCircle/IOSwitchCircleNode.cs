﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class is an interactive object
/// Is the node interaction for the SwitchCirclePuzzle
/// </summary>
public class IOSwitchCircleNode : InteractiveObject
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //CONSTANTS
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    //The list of posible states for the node
    public enum NodeStates
    {
        black, white,red,blue
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~   
    //The list of destinies from this node
    public IOSwitchCircleNode[] nodeDestinies;
    //List of Possible States for this node
    public NodeStates[] possibleStates;    
    //The initial state of the node
    public int initialStateIndex;
    //The initial state of interaction of the node
    public bool canInteractAtStart;
    //The colors of the states
    public Color[] colors;
    //The winner state color
    public Color winnerColor;
    //Bool that determines if this node is interactuable right now
    public bool canInteract;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~ 
    //Has the puzzle started?
    private bool hasPuzzleStarted;
    //The light of the node
    private Light nodeLight;
    //The current state of the node
    private int nodeStateIndex;
    //Sprite Renderer of the node
    private SpriteRenderer spriteRenderer;
    //The Switch Circle Puzzle Master
    private SwitchCirclePuzzle switchCirclePuzzle;
    //State indicator sprite renderer
    private SpriteRenderer indicatorSpriteRenderer;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();        
        indicatorSpriteRenderer = GetComponentsInChildren<SpriteRenderer>()[1];        
        switchCirclePuzzle = GetComponentInParent<SwitchCirclePuzzle>();
        nodeLight = GetComponentInChildren<Light>();
        canInteract = canInteractAtStart;
        //Starter config
        hasPuzzleStarted = false;
        nodeLight.color = canInteractAtStart?Color.yellow:Color.black;
        spriteRenderer.color = canInteractAtStart ? Color.yellow : Color.black;
        //RestartNode();
    }
    /// <summary>
    /// Restarts the node value and aspect
    /// </summary>
    public void RestartNode()
    {
        hasPuzzleStarted = true;
        canInteract = true; //TODO Esto es para el demo only
        nodeStateIndex = initialStateIndex;
        RefreshNode(canInteractAtStart);
    }
    /// <summary>
    /// Refreshes the node aspect and values after switch
    /// </summary>
    /// <param name="canInteractInNextTurn">Can this node interact in the next turn?</param>
    private void RefreshNode(bool canInteractInNextTurn)
    {
        //TODO ajustado para el demo
        //canInteract = canInteractInNextTurn;
        //nodeLight.color = canInteract ? Color.green : Color.red;
        nodeLight.color = colors[nodeStateIndex];
        spriteRenderer.color = colors[nodeStateIndex];
        //indicatorSpriteRenderer.color = canInteract ? Color.green : Color.red;
    }
    /// <summary>
    /// Returns the current state of the node
    /// </summary>
    /// <returns>The current node state</returns>
    public NodeStates GetCurrentState()
    {
        return (NodeStates)nodeStateIndex;
    }
    /// <summary>
    /// Switches this node to the next possible state
    /// </summary>    
    public void SwitchSelfToNextState()
    {
        //Switches to the next possible state
        if ((nodeStateIndex + 1) < possibleStates.Length)
            nodeStateIndex++;
        //Back to the begining
        else nodeStateIndex = 0;       
    }
    /// <summary>
    /// Switches node destinies to the next state
    /// </summary>
    public void SwitchDestiniesToNextState()
    {
        foreach (var item in nodeDestinies)
        {            
            item.SwitchSelfToNextState();            
            item.RefreshNode((item.GetCurrentState() <= GetCurrentState()));
        }
    }
    /// <summary>
    /// Returns the current color of the node
    /// </summary>
    /// <returns>The current color of the node</returns>
    public Color GetColor()
    {
        return colors[nodeStateIndex];
    }
    /// <summary>
    /// Sets the winner indicator if puzzle is solved
    /// Sets previous values otherwise
    /// </summary>
    /// <param name="hasWon">Has won the game?</param>
    public void SetWinnerState(bool hasWon)
    {
        if (hasWon)
        {
            spriteRenderer.color = winnerColor;
            nodeLight.color = winnerColor;
            canInteract = false;
        }
        else
        {
            RefreshNode(canInteract);
        }

    }
    //----------------Interaction STUFF----------------
    /// <summary>
    /// Starts the interaction with this node
    /// Switches itself, then desnity nodes
    /// </summary>
    /// <param name="player"></param>
    public override void StartObjectInteraction(GameObject player)
    {        
        //Check if can interact
        if (!canInteract) return;
        //Start puzzle
        if(!hasPuzzleStarted)
        {
            hasPuzzleStarted = true;
            GetComponentInParent<SwitchCirclePuzzle>().StartPuzzle();
        }
        else //Continue playing
        {
            //Switch self state & deactivate for the next turn        
            SwitchSelfToNextState();
            RefreshNode(false);
            //Switch connected nodes & activate 4 next turn
            SwitchDestiniesToNextState();
            //Check if solved the puzzle
            switchCirclePuzzle.CheckForAnswer();
        }
        
    }
    /// <summary>
    /// Stops the interaction with the node
    /// </summary>
    public override void StopObjectInteraction()
    {
        //throw new System.NotImplementedException();
    }
}
