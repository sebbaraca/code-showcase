﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class is a minion of the Switch Circle Puzzle
/// It manages the Edges logic of the puzzle. 
/// (Turning on/off connection, placing walls, etc..)
/// </summary>
public class SCPEdgesMinion : MonoBehaviour
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~  
    //The pair of nodes that this edge connects
    public IOSwitchCircleNode originNode;
    public IOSwitchCircleNode destinyNode;

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~   
    //The sprite renderer of the edge
    private SpriteRenderer spriteRenderer;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~   
    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();        
    }

    private void Start()
    {
        //RefreshEdge(false);
        spriteRenderer.color = Color.black;
    }

    public void RefreshEdge(bool hasWon)
    {
        if(hasWon)
        {
            spriteRenderer.color = originNode.winnerColor;
        }
        //Nodes in Sync
        else if(originNode.GetCurrentState()==destinyNode.GetCurrentState())
        {
            spriteRenderer.color = originNode.GetColor();
        }
        else //Nodes ot of sync
        {
            spriteRenderer.color = Color.gray;
        }
    }
}
