﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class is in charge of the Puzzle of the Switch Circle
/// </summary>
public class SwitchCirclePuzzle : MonoBehaviour
{
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PUBLIC VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~   
    //The spawn point root
    public Transform enemySpawnPointRoot;
    //Reward of the puzzle
    public GameObject reward;
    //List of objects to turn off when game is won
    public GameObject[] toTurnOffOnWin;
    //List of Sfx for the puzzle 0 for switch, 1 for win,2 start
    public AudioClip[] sfxAudioClips;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //PRIVATE VARIABLES
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //AudioSource of the puzzle
    private AudioSource audioSource;
    //The node List of the puzzle
    private IOSwitchCircleNode[] iOSwitchCircleNodes;
    //The list of Edges 
    private SCPEdgesMinion[] sCPEdgesMinions; 
    //Is the puzzle solved?
    private bool isPuzzleSolved;
    //List of transforms that will spawn enemies
    private Transform[] enemiesSpawnPoints;
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    private void Awake()
    {
        //Get nodes from children
        iOSwitchCircleNodes = GetComponentsInChildren<IOSwitchCircleNode>();
        sCPEdgesMinions = GetComponentsInChildren<SCPEdgesMinion>();
        audioSource = GetComponent<AudioSource>();
        isPuzzleSolved = false;
        reward.SetActive(false);
        toTurnOffOnWin[0].SetActive(true); //Center Node
        toTurnOffOnWin[1].SetActive(false);//Minimap
        enemiesSpawnPoints = enemySpawnPointRoot.GetComponentsInChildren<Transform>();       

    }
    /// <summary>
    /// Checks that all nodes have the same value
    /// Then refresh edges
    /// </summary>
    public void CheckForAnswer()
    {       
        //Check for answer & check if can still play
        bool hasWon = true;
        IOSwitchCircleNode iOSwitchCircleNode = iOSwitchCircleNodes[0];
        bool canContinue = true;// iOSwitchCircleNode.canInteract;
        IOSwitchCircleNode.NodeStates state = iOSwitchCircleNode.GetCurrentState();
        for (int i = 0; i < iOSwitchCircleNodes.Length; i++)
        {
            iOSwitchCircleNode = iOSwitchCircleNodes[i];
            if (iOSwitchCircleNode.GetCurrentState()!=state)
            {
                //Has not arrived to answer
                hasWon = false;                
            }
            if (!canContinue && iOSwitchCircleNode.canInteract)
            {
                canContinue = true;
            }
        }
        //Has answer, Win game        
        WinPuzzle(state,iOSwitchCircleNodes[0].GetColor(),hasWon,canContinue);
    }
    /// <summary>
    /// Starts the puzzle and subscribes to checkpoint manager
    /// </summary>
    public void StartPuzzle()
    {
        CheckpointManager.Instance.OnCheckpointReloaded += RestartPuzzle;
        RestartPuzzle();
    }
    //TODO DOcument this when we know what it does exactly
    public void WinPuzzle(IOSwitchCircleNode.NodeStates winnerState,Color color,bool hasWon,bool canContinue)
    {
        isPuzzleSolved = hasWon;
        //Set win state if must
        foreach (var item in iOSwitchCircleNodes)
        {
            item.SetWinnerState(hasWon);
            //Reset puzzle if can't make another move
            if (!canContinue&&!hasWon) item.RestartNode();
        }        
        if (hasWon)
        {
            //SFX
            audioSource.clip = sfxAudioClips[1];
            audioSource.Play();
            //Unsubscribe from checkpoint
            CheckpointManager.Instance.OnCheckpointReloaded -= RestartPuzzle;
            //Disable objects and destroy enemies
            foreach (var item in toTurnOffOnWin)
            {
                item.SetActive(false);
            }
            EnemySpawner.Instance.DestroyAllEnemies(false);
            reward.SetActive(true);
            //TODO Cinamtica
            ObjectivesManager.Instance.CompleteDetailedObjective(false);
        }
        else
        {
            //SFX
            audioSource.clip = sfxAudioClips[0];
            audioSource.Play();
            //Spawn enemies
            SpawnEnemies();            
            reward.SetActive(false);
        }
        //Refresh Edges
        foreach (var item in sCPEdgesMinions)
        {
            item.RefreshEdge(hasWon);
        }
    }
    /// <summary>
    /// Spawns random enemies on the positions
    /// </summary>
    private void SpawnEnemies()
    {
        foreach (var item in enemiesSpawnPoints)
        {
            EnemySpawner.Instance.SpawnSpecificEnemy(item.position,item.rotation,EnemySpawner.EnemyTypes.Moira);
        }
        EnemySpawner.Instance.SetAIValue(true);
    }

    /// <summary>
    /// Restarts the puzzle
    /// </summary>
    public void RestartPuzzle()
    {
        if (isPuzzleSolved)
            return;
        //SFX
        audioSource.clip = sfxAudioClips[2];
        audioSource.Play();
        isPuzzleSolved = false;
        //Restart nodes
        foreach (var item in iOSwitchCircleNodes)
        {
            item.RestartNode();
        }
        //Refresh Edges
        foreach (var item in sCPEdgesMinions)
        {
            item.RefreshEdge(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //When player enters area turn on minimap
        if(other.tag=="Player"&&!isPuzzleSolved)
        {
            toTurnOffOnWin[1].SetActive(true);
        }
    }

    private void OnDestroy()
    {
        try { CheckpointManager.Instance.OnCheckpointReloaded -= RestartPuzzle; } catch (System.Exception) { }
    }
}
