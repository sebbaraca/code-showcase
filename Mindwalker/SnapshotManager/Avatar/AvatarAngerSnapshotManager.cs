﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
/// <summary>
/// This class stores and loads all the information relative to the 
/// anger avatar state for active and passive habilities
/// </summary>
[XmlRoot("AvatarAngerManager")]
public class AvatarAngerSnapshotManager
{
    /**Data Structure that stores the information of each avatar state*/
    public class AngerSnapshot
    {
        //************Active Ability************
        //Cooldown time for the active movement
        public float movementCoolDownTime;
        //Cooldown time for the active movement
        public float powerCoolDownTime;
        //Acceleration for the dash
        public float accelerationFactor;
        //Movement cooldown after the jump
        public float jumpMovementCooldown;
        //Movement cooldown before the jump
        public float jumpMovementPreparationTime;
        //The factor to slow the avatar before the Jump
        public float jumpPreparationSlowFactor;
        //The ammount of damage the slash attack does
        public float slashAttackDamage;
        //The ammount of damage the Tornado attack does
        public float tornadoAttackDamage;
        //Invulnerability for dash duration
        public float dashInvulnerabilityDuration;
        //Invulnerability for tornado duration
        public float tornadoInvulnerabilityDuration;
        //Force of tornado push back
        public float tornadoAttackForce;
        //Movement cooldown before the jump
        public float tornadoPreparationTime;
        //The factor to slow the avatar before the Jump
        public float tornadoSlowFactor;
        //Lift force for attacks
        public float upLiftForce;
        //Duration for the Stun on the attacks
        public float stunDuration;
        //************Pasive Ability************
        //Amount of max time  before frenzy resets
        public float maxFrenzyTime;
        //Amount of hits needed on each lvl (NOTE: This must be on same order as FrenzyLevels)
        [XmlArray("FrenzyHitsNeeded")]
        [XmlArrayItem("Value")]
        public int[] frenzyHitsNeeded ;
        //Damage amount multiplier of each frenzylvl
        [XmlArray("damageAmountMultipliers")]
        [XmlArrayItem("Value")]
        public float[] damageAmountMultipliers ;
        //Damage speed multiplier of each frenzylvl
        [XmlArray("damageSpeedMultipliers")]
        [XmlArrayItem("Value")]
        public float[] damageSpeedMultipliers;
        //Self damage per second of each frenzylvl
        [XmlArray("selfDamagesPerSecond")]
        [XmlArrayItem("Value")]
        public float[] selfDamagesPerSecond;
        //Life leech of each frenzy lvl
        [XmlArray("lifeLeechAmounts")]
        [XmlArrayItem("Value")]
        public float[] lifeLeechAmounts;
        //dash multiplier force of each frenzylvl
        [XmlArray("dashMultiplier")]
        [XmlArrayItem("Value")]
        public float[] dashMultiplier;
        //********************ANIMATION STUFF********
        //Number of ghosts for movement dash
        public int ghostsAmmountForDash;
        //Exclusivity time for tornado attack
        public float animExclusivityTornadoTime;
        //Exclusivity time for Dash attack
        public float animExclusivityDashTime;
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Variables
    //~~~~~~~~~~~~~~~~~~~~~~~~
    [XmlElement]
    //The anger snapshot
    public AngerSnapshot angerSnapshot;

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    /// Returns an avatar Snapshot stored on the XML specified
    /// </summary>
    /// <returns></returns>
    public static AvatarAngerSnapshotManager Load()
    {
        //Path to the XML file
        string path = "./Assets/Data/Avatar/AvatarAngerSnapshotManager.xml";
        var serializer = new XmlSerializer(typeof(AvatarAngerSnapshotManager));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as AvatarAngerSnapshotManager;
        }
    }
}

