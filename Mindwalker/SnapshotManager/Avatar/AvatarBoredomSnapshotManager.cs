﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
/// <summary>
/// This class stores and loads all the information relative to the 
/// anger avatar state for active and passive habilities
/// </summary>
[XmlRoot("AvatarBoredomSnapshotManager")]
public class AvatarBoredomSnapshotManager
{
    /**Data Structure that stores the information of each avatar state*/
    public class BoredomSnapshot
    {
        //************Active Ability************
        //Cooldown time for the active movement
        public float movementCoolDownTime;
        //Cooldown time for the active movement
        public float powerCoolDownTime;
        //Cooldown time for the flooratk
        public float floorCoolDownTime;
        //Acceleration for the dash
        public float accelerationFactor;       
        //The boost factor of the movement speed for the ball mode of the state
        public float boostFactor;
        //The boost factor of the movement speed for the floor attack
        public float floorBoostFactor;
        //Movement cooldown for transition
        public float movementCoolDown;
        //The ammount of damage the one punch attack does
        public float onePunchAttackDamage;
        //Invulnerability for one punch duration
        public float onePunchInvulnerabilityDuration;
        //The force of the one punch when pushing enemies
        public float onePunchForce;
        //The ammount of damage the rolling punch attack does
        public float rollingPunchAttackDamage;
        //Invulnerability for rolling punch duration
        public float rollingPunchInvulnerabilityDuration;
        //The force of the rolling punch when pushing enemies
        public float rollingPunchForce;
        //The ammount of damage the floor attack does
        public float floorAttackDamage;
        //Invulnerability for rolling punch duration
        public float floorInvulnerabilityDuration;
        //The force of the rolling punch when pushing enemies
        public float floorForce;
        //Lift force for attacks
        public float upLiftForce;
        //************Pasive Ability************
        //TODO
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Variables
    //~~~~~~~~~~~~~~~~~~~~~~~~
    [XmlElement]
    //The anger snapshot
    public BoredomSnapshot boredomSnapshot;

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    /// Returns an avatar Snapshot stored on the XML specified
    /// </summary>
    public static AvatarBoredomSnapshotManager Load()
    {
        //Path to the XML file
        string path = "./Assets/Data/Avatar/AvatarBoredomSnapshotManager.xml";
        var serializer = new XmlSerializer(typeof(AvatarBoredomSnapshotManager));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as AvatarBoredomSnapshotManager;
        }
    }
}

