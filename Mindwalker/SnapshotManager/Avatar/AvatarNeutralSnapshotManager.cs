﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
/// <summary>
/// This class stores and loads all the information relative to the 
/// anger avatar state for active and passive habilities
/// </summary>
[XmlRoot("AvatarNeutralSnapshotManager")]
public class AvatarNeutralSnapshotManager
{
    /**Data Structure that stores the information of each avatar state*/
    public class NeutralSnapshot
    {
        //************Active Ability************
        //Cooldown time for the active movement
        public float movementCoolDownTime;
        //Cooldown time for the active movement
        public float powerCoolDownTime;
        //Acceleration for the dash
        public float accelerationFactor;
        //¨¨¨¨¨¨¨¨¨¨¨¨¨¨BULLET¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
        //The damage base amount
        public float bulletDamageBase;
        //The damage step while the bullet increases
        public float bulletDamageStep;
        //Travel Speed
        public float bulletSpeed;
        //Increasing size for explosion
        //Explosion Time
        public float explosionTime;
        //Explosion size
        public float explosionSizeStep;
        //Rotation speed
        public float rotationSpeed;
        //Time to explode for the bullet
        public float bulletTimeToExplode;
        //Number of times to shoot on each action
        public int shootAmmo;
        //Time between shots
        public float shootCooldown;
        //************Pasive Ability************
        //TODO
        //***********Animation Stuff********
        //Number of ghosts for movement dash
        public int ghostsAmmountForDash ;
        //Exclusivity time for shoot animation
        public float animExclusivityDashTime;
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Variables
    //~~~~~~~~~~~~~~~~~~~~~~~~
    [XmlElement]
    //The anger snapshot
    public NeutralSnapshot neutralSnapshot;

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    /// Returns an avatar Snapshot stored on the XML specified
    /// </summary>
    public static AvatarNeutralSnapshotManager Load()
    {
        //Path to the XML file
        string path = "./Assets/Data/Avatar/AvatarNeutralSnapshotManager.xml";
        var serializer = new XmlSerializer(typeof(AvatarNeutralSnapshotManager));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as AvatarNeutralSnapshotManager;
        }
    }
}

