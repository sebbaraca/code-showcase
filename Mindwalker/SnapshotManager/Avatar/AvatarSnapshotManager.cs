﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
/// <summary>
/// This class stores and loads all the information relative to the 
/// avatar state variables, such as movement speed, damage, etc..
/// </summary>
[XmlRoot("AvatarSnapshotManager")]
public class AvatarSnapshotManager  
{
    /**Data Structure that stores the information of each avatar state*/ 
    public class AvatarSnapshot
    {
        [XmlAttribute("name")]
        //Name of the Avatar
        public string name;
        //Defensive Power of the avatar goes from 0 to 1
        public float defensivePower;
        //Max Health of the Avatar
        public float health;
        //Movement Speed of the player (acceleration)
        public float accelerationFactor;
        //Max speed that the player can achieve on x or y
        public float maxSpeed;
        //Slow factor when the player changes direction or stops accelerating
        public float slowFactor;
        //Mass of the player's rigid body
        public float mass ;
        //Linear drag of the rigid body
        public float linearDrag ;
        //The Warning threshold to warn the player on UI goes from 0 to 1
        public float warningThreshold ;
        //Cooldown to determine how often the individual can take damage
        public float hurtingCooldown;
        //Is this avatar mode melee?
        public bool isMelee;
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Variables
    //~~~~~~~~~~~~~~~~~~~~~~~~
    [XmlArray("AvatarSnapshots")]
    [XmlArrayItem("AvatarSnapshot")]
    //List of Avatar's values snapshots
    public List<AvatarSnapshot> AvatarSnapshots = new List<AvatarSnapshot>();

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    /// Returns an avatar Snapshot stored on the XML specified
    /// </summary>
    public static AvatarSnapshotManager Load()
    {
        //Path to the XML file
        string path = "./Assets/Data/Avatar/AvatarSnapshotManager.xml";
        var serializer = new XmlSerializer(typeof(AvatarSnapshotManager));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as AvatarSnapshotManager;
        }
    }
}
