﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
/// <summary>
/// This class stores and loads all the information relative to the 
/// enemy Moira state variables, such as movement speed, damage, etc..
/// </summary>
[XmlRoot("EMoiraSnapshotManager")]
public class EMoiraSnapshotManager
{
    //Data Structure that stores the information of each Enemy state
    public class EMoiraSnapshot
    {
        [XmlAttribute("name")]
        //Name of the Enemy
        public string name;
        //Defensive Power of the Enemy goes from 0 to 1
        public float defensivePower;
        //Max Health of the Enemy
        public float health;
        //Movement Speed of the Enemy (acceleration)
        public float accelerationFactor;
        //Max speed that the Enemy can achieve on x or y
        public float maxSpeed;
        //Slow factor when the Enemy changes direction or stops accelerating
        public float slowFactor;
        //Mass of the Enemy's rigid body
        public float mass;
        //Linear drag of the rigid body
        public float linearDrag;
        //Cooldown to determine how often the individual can take damage
        public float hurtingCooldown;
        //¨¨¨¨¨¨¨¨¨¨¨¨¨¨Äctive¨¨¨¨¨¨¨¨¨¨¨¨¨¨
        //Cooldown time for the active movement
        public float movementCoolDownTime;
        //Cooldown time for the active movement
        public float powerCoolDownTime;
        //Acceleration for the dash
        public float dashAccelerationFactor;
        //Movement cooldown after the jump
        public float jumpMovementCooldown;
        //Movement cooldown before the jump
        public float jumpMovementPreparationTime;
        //The factor to slow the avatar before the Jump
        public float jumpPreparationSlowFactor;
        //¨¨¨¨¨¨¨¨¨¨¨¨¨¨BULLET¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨    
        //The damage base amount
        public float bulletDamageBase;
        //The damage step while the bullet increases
        public float bulletDamageStep;
        //Travel Speed
        public float bulletSpeed;
        //Explosion Time
        public float explosionTime;
        //Increasing size step for explosion
        public float explosionSizeStep;
        //Rotation speed
        public float rotationSpeed;
        //Time to explode for the bullet
        public float bulletTimeToExplode;
        //¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨AI¨¨¨¨¨¨¨¨¨¨
        //Number of times to shoot on stay
        public int shootAmmo;
        //Time to change direction randomly
        public float changeDirectionTime;
        //Reload Time
        public float reloadTime;
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Variables
    //~~~~~~~~~~~~~~~~~~~~~~~~
    [XmlArray("EMoiraSnapshots")]
    [XmlArrayItem("EMoiraSnapshot")]
    //List of Avatar's values snapshots
    public List<EMoiraSnapshot> EMoiraSnapshots = new List<EMoiraSnapshot>();

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    /// Returns an Enemy Moira Snapshot stored on the XML specified
    /// </summary>
    public static EMoiraSnapshotManager Load()
    {
        //Path to the XML file
        string path = "./Assets/Data/Enemies/EMoiraSnapshotManager.xml";
        var serializer = new XmlSerializer(typeof(EMoiraSnapshotManager));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as EMoiraSnapshotManager;
        }
    }
}
