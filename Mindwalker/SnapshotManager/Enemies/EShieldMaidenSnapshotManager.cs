﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
/// <summary>
/// This class stores and loads all the information relative to the 
/// Shield Maiden enemy state variables, such as movement speed, damage, etc..
/// </summary>
[XmlRoot("EShieldMaidenSnapshotManager")]
public class EShieldMaidenSnapshotManager 
{
    //Data Structure that stores the information of each Enemy state
    public class EShieldMaidenSnapshot
    {
        [XmlAttribute("name")]
        //Name of the Enemy
        public string name;
        //Defensive Power of the Enemy goes from 0 to 1
        public float defensivePower;
        //Max Health of the Enemy
        public float health;
        //Movement Speed of the Enemy (acceleration)
        public float accelerationFactor;
        //Max speed that the Enemy can achieve on x or y
        public float maxSpeed;
        //Slow factor when the Enemy changes direction or stops accelerating
        public float slowFactor;
        //Mass of the Enemy's rigid body
        public float mass;
        //Linear drag of the rigid body
        public float linearDrag;
        //Cooldown to determine how often the individual can take damage
        public float hurtingCooldown;
        //¨¨¨¨¨¨¨¨¨¨¨¨¨¨Äctive¨¨¨¨¨¨¨¨¨¨¨¨¨¨
        //Cooldown time for the active movement
        public float movementCoolDownTime;
        //Cooldown time for the active movement
        public float powerCoolDownTime;
        //Acceleration for the dash
        public float dashAccelerationFactor;
        //Movement cooldown after the jump
        public float jumpMovementCooldown;
        //Movement cooldown before the jump
        public float jumpMovementPreparationTime;
        //The factor to slow the avatar before the Jump
        public float jumpPreparationSlowFactor;
        //Bash attack damage
        public float blashAttackDamage;
        //The duration for the bash stun
        public float stunDuration;
        //The duration of the stun Window
        public float stunWindowDuration;
        //Vulnerable window duration
        public float vulnerableWindowDuration;
        //¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨AI¨¨¨¨¨¨¨¨¨¨
        //Time to change direction randomly
        public float changeDirectionTime;

    }
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Variables
    //~~~~~~~~~~~~~~~~~~~~~~~~
    [XmlArray("EShieldMaidenSnapshots")]
    [XmlArrayItem("EShieldMaidenSnapshot")]
    //List of Avatar's values snapshots
    public List<EShieldMaidenSnapshot> EShieldMaidenSnapshots = new List<EShieldMaidenSnapshot>();

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    /// Returns an Enemy Shield Maiden Snapshot stored on the XML specified
    /// </summary>
    public static EShieldMaidenSnapshotManager Load()
    {
        //Path to the XML file
        string path = "./Assets/Data/Enemies/EShieldMaidenSnapshotManager.xml";
        var serializer = new XmlSerializer(typeof(EShieldMaidenSnapshotManager));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as EShieldMaidenSnapshotManager;
        }
    }
}
