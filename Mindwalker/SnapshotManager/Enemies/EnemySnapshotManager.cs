﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
/// <summary>
/// This class stores and loads all the information relative to the 
/// enemy state variables, such as movement speed, damage, etc..
/// </summary>
[XmlRoot("EnemySnapshotManager")]
public class EnemySnapshotManager
{
    //Data Structure that stores the information of each Enemy state
    public class EnemySnapshot
    {
        [XmlAttribute("name")]
        //Name of the Enemy
        public string name;
        //Defensive Power of the Enemy goes from 0 to 1
        public float defensivePower;
        //Max Health of the Enemy
        public float health;
        //Movement Speed of the Enemy (acceleration)
        public float accelerationFactor;
        //Max speed that the Enemy can achieve on x or y
        public float maxSpeed;
        //Slow factor when the Enemy changes direction or stops accelerating
        public float slowFactor;
        //Mass of the Enemy's rigid body
        public float mass;
        //Linear drag of the rigid body
        public float linearDrag;    
        //Cooldown to determine how often the individual can take damage
        public float hurtingCooldown;
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~
    //Variables
    //~~~~~~~~~~~~~~~~~~~~~~~~
    [XmlArray("EnemySnapshots")]
    [XmlArrayItem("EnemySnapshot")]
    //List of Avatar's values snapshots
    public List<EnemySnapshot> EnemySnapshots = new List<EnemySnapshot>();

    //~~~~~~~~~~~~~~~~~~~~~~~~
    //METHODS
    //~~~~~~~~~~~~~~~~~~~~~~~~
    /// <summary>
    /// Returns an avatar Snapshot stored on the XML specified
    /// </summary>
    public static EnemySnapshotManager Load()
    {
        //Path to the XML file
        string path = "./Assets/Data/Enemies/EnemySnapshotManager.xml";
        var serializer = new XmlSerializer(typeof(EnemySnapshotManager));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as EnemySnapshotManager;
        }
    }
}
